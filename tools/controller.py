from subprocess import call
import httplib, urllib, json, time

def getDeviceRoot(deviceAddress):
    conn = httplib.HTTPConnection(deviceAddress, 8080)
    conn.request('GET', 'commands/device/root');
    res = conn.getresponse()
    responseBody = res.read()
    conn.close()
    return json.loads(responseBody)

class Controller:

	def init(self, devicesFile):
		self.devices = [device.strip() for device in open(devicesFile)]
		
	def deploy(self):
		for i in range(0, 11):
			device = self.devices[i]
			address = device + ":5555"
			call(["adb", "kill-server"])
			call(["adb", "start-server"])
			
			call(["adb", "connect", device])
			call(["adb", "-s", address, "shell", "am", "force-stop", "-n" "pl.edu.agh.dkala_msc"]);
			call(["adb", "-s", address, "uninstall", "pl.edu.agh.dkala_msc"]);
			call(["adb", "-s", address, "install", "../dkala_msc/bin/dkala_msc.apk"]);
			call(["adb", "-s", address, "shell", "am", "start", "-n", "pl.edu.agh.dkala_msc/pl.edu.agh.dkala_msc.MainActivity"]);

	# makes sourceDevice connect to destinationDevice
	def connect(self, sourceDevice, destinationDevice):
		sourceIP = self.devices[sourceDevice];
		destinationIP = self.devices[destinationDevice];
		
		conn = httplib.HTTPConnection(sourceIP, 8080)
		conn.connect()
		
		body = '{ "DestinationIP" : "' + destinationIP + '" }';
		
		request = conn.putrequest('POST', '/commands/connect')
		headers = {}
		headers['Content-Type'] = 'application/json'
		headers['Content-Length'] = "%d"%(len(body));
		
		for k in headers:
			conn.putheader(k, headers[k])
		conn.endheaders()
		conn.send(body)

		resp = conn.getresponse()
		conn.close()
		
		return resp;

	def connectDevice(self, sourceDevice, destinationDevice):
		deviceAddress = self.devices[sourceDevice];
		rootAddress = self.devices[destinationDevice];
		
		conn = httplib.HTTPConnection(deviceAddress, 8080)
		conn.connect()
		
		body = '{ "DestinationIP" : "' + rootAddress + '" }';
		
		conn.putrequest('POST', '/commands/connect')
		headers = {}
		headers['Content-Type'] = 'application/json'
		headers['Content-Length'] = "%d"%(len(body));
		
		for k in headers:
			conn.putheader(k, headers[k])
		conn.endheaders()
		conn.send(body)
		
		resp = conn.getresponse()
		timeouts = 0
		while True:
			time.sleep(1)
			data = getDeviceRoot(deviceAddress)
			if "RootDevice" in data:
				break
			timeouts += 1
			if timeouts > 5:
				raise Exception("device cannot be connected to cloud")
		
	# makes sourceDevice execute javascript code located in file named taskFile
	def execute(self, sourceDevice, taskFile):
		print "Executing task from file '" + taskFile + "' on device " + self.devices[sourceDevice]
		
	def getConnectedDevices(self, sourceDevice):		
		sourceIP = self.devices[sourceDevice];
		
		conn = httplib.HTTPConnection(sourceIP, 8080)
		conn.request('GET', 'commands/device/children');
		res = conn.getresponse()
		responseBody = res.read()
		data = json.loads(responseBody)
		
		return data
		
	def getDeviceInformation(self, sourceDevice):		
		sourceIP = self.devices[sourceDevice];
		
		conn = httplib.HTTPConnection(sourceIP, 8080)
		conn.request('GET', 'commands/device/information');
		res = conn.getresponse()
		responseBody = res.read()
		data = json.loads(responseBody)
		
		return data
		
	def getDeviceRoot(self, sourceDevice):		
		sourceIP = self.devices[sourceDevice];
		
		conn = httplib.HTTPConnection(sourceIP, 8080)
		conn.request('GET', 'commands/device/root');
		res = conn.getresponse()
		responseBody = res.read()
		data = json.loads(responseBody)
		
		return data
		
	def getAllChildrenCount(self, sourceDevice):		
		sourceIP = self.devices[sourceDevice];
		
		conn = httplib.HTTPConnection(sourceIP, 8080)
		conn.request('GET', 'commands/device/allChildrenCount');
		res = conn.getresponse()
		responseBody = res.read()
		data = json.loads(responseBody)
		
		return data
		
	def getTaskResult(self, sourceDevice, taskId):
		sourceIP = self.devices[sourceDevice];
		
		conn = httplib.HTTPConnection(sourceIP, 8080)
		conn.request('GET', 'tasks/' + taskId);
		res = conn.getresponse()
		responseBody = res.read()
		
		data = json.loads(responseBody)
		
		return data
		
	def waitForTaskResult(self, sourceDevice, taskId):
		sourceIP = self.devices[sourceDevice];
		tryCount = 0
		while(True):
			try:
				conn = httplib.HTTPConnection(sourceIP, 8080)
				conn.request('GET', 'tasks/' + taskId);
				res = conn.getresponse()
				responseBody = res.read()
				try:
					data = json.loads(responseBody)
				except:
					print responseBody
					raise
				
				if data['Status'] == 'completed':
					break
				else:
					time.sleep(0.5)		
			except:
				if tryCount > 5:
					raise
				tryCount += 1
				time.sleep(0.5)
		
		try:
			result = json.loads(data['Result'])
		except:
			print responseBody
			raise
		return result
		
	def resetDevice(self, sourceDevice):
		sourceIP = self.devices[sourceDevice];
		
		conn = httplib.HTTPConnection(sourceIP, 8080)
		conn.connect()
		
		body = '';
		
		request = conn.putrequest('POST', '/commands/device/reset')
		headers = {}
		headers['Content-Type'] = 'application/json'
		headers['Content-Length'] = "%d"%(len(body));
		
		for k in headers:
			conn.putheader(k, headers[k])
		conn.endheaders()
		conn.send(body)

		resp = conn.getresponse()
		conn.close()
		
		return resp;
        
	def getDevices(self):
		return self.devices
		
	def resetAllDevices(self):
		for i in range(0, len(self.devices)):
			try:
				self.resetDevice(i)
			except:
				pass

	def scheduleTaskRequest(self, sourceDevice, taskRequest):
		sourceIP = self.devices[sourceDevice];
		
		conn = httplib.HTTPConnection(sourceIP, 8080)
		conn.connect()
		
		body = json.dumps(taskRequest);
		
		request = conn.putrequest('POST', '/commands/tasks')
		headers = {}
		headers['Content-Type'] = 'application/json'
		headers['Content-Length'] = "%d"%(len(body));
		
		for k in headers:
			conn.putheader(k, headers[k])
		conn.endheaders()
		conn.send(body)

		resp = conn.getresponse().read()
		conn.close()
		
		return json.loads(resp);