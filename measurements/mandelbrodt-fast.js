var task = (function() {
	var solver = (function() {
		function complexMultiply (first, second) {
			var complexResult = {
				r: first.r * second.r - first.i * second.i,
				i: first.i * second.r + first.r * second.i
			};

			return complexResult;
		}

		function complexAbs (complex) {
			var abs = Math.sqrt(complex.r * complex.r + complex.i * complex.i);
			return abs;
		}

		function evaluate (x, y, iterationCount) {
			var i, point, complex;

			point = {
				r: x,
				i: y
			};
			complex = {
				r: 0,
				i: 0
			};

			for (i = 0; i < iterationCount; i++) {
				if (complexAbs(complex) >= 2) {
					return i - 1;
				}
				complex = complexMultiply(complex, complex);
				complex.r += point.r;
				complex.i += point.i;
			}

			return i - 1;
		}

		function mandelbrot(xRange, yRange, stepsQuantity, iterationCount) {
			var x, y, result, xStepSize, yStepSize, evaluation;

			if (!iterationCount) {
				iterationCount = 255;
			}

			xStepSize = (xRange.to - xRange.from) / stepsQuantity;
			yStepSize = (yRange.to - yRange.from) / stepsQuantity;

			result = [];
			var xIndex = 0;
			for (x = xRange.from; x <= xRange.to; x += xStepSize) {
				result.push([]);
				for (y = yRange.from; y <= yRange.to; y += yStepSize) {
					evaluation = evaluate(x, y, iterationCount);
					result[xIndex].push(evaluation);
				}
				xIndex++;
			}

			return result;
		}

		return mandelbrot;
	})();
	
	return function(dataCenterIp) {	
		var deviceNumber = Platform.getDeviceNumber();
		var devicesCount = Platform.getDevicesCount();
		var taskId = Platform.getTaskId();
				
		if (deviceNumber === 0) {
            PlatformTools.sendDataToServer(dataCenterIp + "/tasks/", {
				taskId: taskId,
				devicesCount: devicesCount
			});
		}

		var step = 2.5 / devicesCount;
		var from = -1.85 + step * deviceNumber;
		var to = from + step;

	var start = new Date().getTime();
		var result = solver({from: from, to: to}, {from: -1.25, to: 1.25}, 900, 255);
	var end = new Date().getTime();

		PlatformTools.sendDataToServer(
			dataCenterIp + "/data/?taskId=" + taskId + "&deviceNumber=" + deviceNumber,
			result
		);
	
		PlatformTools.finalize({
			from: from,
			to: to,
			time: end - start
		});
	};
}());


task("http://guarded-beach-2118.herokuapp.com");