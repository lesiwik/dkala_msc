var Platform = {
	getDevicesCount: function() {
		return 1;
	},

	getDeviceNumber: function() {
		return 0;
	},
	
	getTaskId: function() {
		return "exampleTaskId";
	}
};

var PlatformTools = {
	sendDataToServer: function(url, data) {
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open('POST', url);
		xmlhttp.send(JSON.stringify(data));
	},
	
	finalize: function(data) {
		setTimeout(function() {
			var textField = document.getElementById('textField');
			textField.textContent = JSON.stringify(data);
		});
	}
}