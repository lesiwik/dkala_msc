var task = function(shots) {
	var deviceNumber = Platform.getDeviceNumber();
	var devicesCount = Platform.getDevicesCount();

	var start = new Date().getTime();
	
	var x = 0, y = 0, r = 0.5;
	var inside = 0;
	
	for (var i = 0 ; i < shots / devicesCount; i++) {
		x = Math.random() - 0.5;
		y = Math.random() - 0.5;
		if (Math.sqrt(x*x + y*y) <= r) {
			inside += 1;
		}
	}
	var isOdd = function(num) {
		return (num % 2) === 1;
	};
	
	var getRoot = function(currentDevice) {
		if (isOdd(currentDevice)) {
			return (currentDevice - 1) / 2;
		} else {
			return (currentDevice - 2) / 2;
		}
	};
	
	var getLeftChild = function(currentDevice) {
		return currentDevice * 2 + 1;
	};
	
	var getRightChild = function(currentDevice) {
		return currentDevice * 2 + 2;
	};
		
	var leftChild = getLeftChild(deviceNumber);
	var rightChild = getRightChild(deviceNumber);
	var leftChildShots = 0, rightChildShots = 0;
	var isLeftChildRequestPending = false, isRightChildRequestPending = false;
		
	var aggregate = function() {
		inside += leftChildShots;
		inside += rightChildShots;
		var end = new Date().getTime();
		var result = (inside / shots) * 4;
		
		if (deviceNumber === 0) {
			PlatformTools.finalize({
				time: end - start,
				result: result,
				inside: inside,
				shots: shots,
				left: leftChildShots,
				right: rightChildShots
			});
		} else {
			PlatformTools.sendTo(inside, getRoot(deviceNumber));
			PlatformTools.finalize({
				time: end - start,
				result: result,
				inside: inside,
				shots: shots,
				left: leftChildShots,
				right: rightChildShots
			});
		}
	};
	
	var leftChildAggregate = function(shots) {
		leftChildShots = shots;
		
		if (!isRightChildRequestPending) {
			aggregate();
		}
		isLeftChildRequestPending = false;
	};
	
	var rightChildAggregate = function(shots) {
		rightChildShots = shots;
		
		if (!isLeftChildRequestPending) {
			aggregate();
		}
		isRightChildRequestPending = false;
	};
	if (leftChild < devicesCount) {
		isLeftChildRequestPending = true;
		
		PlatformTools.onReceive(leftChild, leftChildAggregate);
	} else {
		isLeftChildRequestPending = false;
	}
	
	if  (rightChild < devicesCount) {
		isRightChildRequestPending = true;
		
		PlatformTools.onReceive(rightChild, rightChildAggregate);
	} else {
		isRightChildRequestPending = false;
	}
	
	if (!isLeftChildRequestPending && !isRightChildRequestPending) {
		aggregate();
	}
};


task(300000000);
