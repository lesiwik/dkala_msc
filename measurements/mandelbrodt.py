import sys, subprocess
sys.path.insert(0, '../tools')

import json, time, unittest, controller

controller = controller.Controller()
controller.init('../tools/devices.config')

dataSize = 300

results = []

while devicesCount <= len(controller.getDevices()):
    if devicesCount != 1:
        controller.connectDevice(devicesCount - 1, 0)
        
    composingCommand = "python compose_mandelbrodt-fast.py 10.0.0.118:5002"
    subprocess.call(composingCommand, shell=True)
    
    with open("mandelbrodt-fast.js", "r") as piSource:
        taskJsCode = piSource.read()
        
    taskName = "mandelbrodt-fast.js"
    taskRequest = { 'TaskJsCode': taskJsCode, 'TaskName': taskName }
        
    repetitionNumber = 0
    measurements = []
    while repetitionNumber < 10:
        scheduledTask = controller.scheduleTaskRequest(0, taskRequest)
        
        deviceTimes = []
        
        deviceToWaitOn = 0
        while deviceToWaitOn < currentDevicesCount:
            print "Waiting for task result on device number " + str(deviceToWaitOn) + " ..."
            taskResult = c.waitForTaskResult(deviceToWaitOn, scheduledTask['TaskId'])
            deviceTimes.append(taskResult["time"])
            deviceToWaitOn += 1
        
        measurements.append(max(deviceTimes))
        
        print "Devices count " + str(devicesCount) + " time " + str(result["time"])
    
        repetitionNumber += 1
    
    avg = sum(measurements) / float(len(measurements))
    results.append(avg)
    devicesCount += 1
    
print results