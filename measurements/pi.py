import sys, subprocess
sys.path.insert(0, '../tools')

import json, time, unittest, controller

controller = controller.Controller()
controller.init('../tools/devices.config')

dataSize = 300

results = []

for i in range(0, len(controller.getDevices())):
    results.append([])

while dataSize <= 30000000:
    controller.resetAllDevices()
    
    devicesCount = 1
    while devicesCount <= len(controller.getDevices()):
        if devicesCount != 1:
            controller.connectDevice(devicesCount - 1, 0)
            
        composingCommand = "python compose_pi-fast.py " + str(dataSize)
        subprocess.call(composingCommand, shell=True)
        
        with open("pi-fast.js", "r") as piSource:
            taskJsCode = piSource.read()
            
        taskName = "pi-fast_" + str(dataSize) + ".js"
        taskRequest = { 'TaskJsCode': taskJsCode, 'TaskName': taskName }
            
        repetitionNumber = 0
        measurements = []
        while repetitionNumber < 10:
            scheduledTask = controller.scheduleTaskRequest(0, taskRequest)
            result = controller.waitForTaskResult(0, scheduledTask['TaskId'])
            measurements.append(result["time"])
            
            print "Devices count " + str(devicesCount) + " data size " + str(dataSize) + " time " + str(result["time"])
        
            repetitionNumber += 1
        
        avg = sum(measurements) / float(len(measurements))
        results[devicesCount - 1].append(avg)
        devicesCount += 1
    
    dataSize *= 10
    
print results