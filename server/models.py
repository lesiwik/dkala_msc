import os, json

## DEVICE ##
def listDevices():
    return os.listdir("devices")

def loadDevices():
    devices = []
    for deviceAddress in listDevices():
        with open("devices/" + deviceAddress, "r") as myfile:
            deviceJson = myfile.read()
        device = json.loads(deviceJson)
        devices.append(device)
    devices = sorted(devices, key=lambda device: device["DeviceInformation"]["IPAddress"])
    return devices

def loadDevice(ipAddress):
    device = { 
        'DeviceInformation': {
            'IPAddress': ipAddress
        },

        'Status': 'unreachable'
    }

    if ipAddress in os.listdir("devices"):
        with open("devices/" + ipAddress, "r") as myfile:
            device = json.loads(myfile.read())
    
    for cloud in loadClouds():
        if device['DeviceInformation']['IPAddress'] in cloud["Devices"]:
            device["CloudName"] = cloud["Name"]
            break

    return device

def saveDevice(device):
    deviceAddress = device["DeviceInformation"]["IPAddress"]
    with open("devices/" + deviceAddress, "w") as myfile:
        myfile.write(json.dumps(device))

def deleteDevice(ipAddress):
    os.remove("devices/" + ipAddress);

## CLOUD ##
def listClouds():
    return os.listdir("clouds")

def loadClouds():
    clouds = []
    for cloudName in listClouds():
        with open("clouds/" + cloudName, "r") as myfile:
            cloudJson = myfile.read()
        cloud = json.loads(cloudJson)
        clouds.append(cloud)
    clouds = sorted(clouds, key=lambda cloud: cloud["Name"])
    return clouds

def loadCloud(cloudName):
    with open("clouds/" + cloudName, "r") as myfile:
        cloudJson = myfile.read()
    return json.loads(cloudJson)

def createCloud(cloudName):
    newCloud = { 'Name': cloudName, 'Devices': [] }
    return newCloud

def saveCloud(cloud):
    cloudName = cloud["Name"]
    with open("clouds/" + cloudName, "w") as myfile:
        myfile.write(json.dumps(cloud))

def removeCloud(cloudName):
    os.remove("clouds/" + cloudName)

## TASK ##
def listTasks():
    return os.listdir("tasks")

def loadTask(taskname):
    with open("tasks/" + taskname, "r") as myFile:
        taskJsCode = myFile.read()
    return taskJsCode

def deleteTask(taskname):
    os.remove("tasks/" + taskname)
    
## TASK EXECUTIONS ##
def listTaskExecutions():
    return os.listdir("taskExecutions")

def loadTaskExecutions():
    taskExecutions = []
    for taskId in listTaskExecutions():
        with open("taskExecutions/" + taskId, "r") as myfile:
            taskExecutionJson = myfile.read()
        taskExecution = json.loads(taskExecutionJson)
        taskExecutions.append(taskExecution)
    taskExecutions = sorted(
        taskExecutions, 
        key=lambda taskExecution: taskExecution["TaskId"], 
        reverse=True
    )
    return taskExecutions


def loadTaskExecution(taskId):
    with open("taskExecutions/" + taskId, "r") as myFile:
        taskExecution = json.loads(myFile.read())
    return taskExecution

def saveTaskExecution(taskExecution):
    with open("taskExecutions/" + taskExecution["TaskId"], "w") as myFile:
        myFile.write(json.dumps(taskExecution))

def deleteTaskExecution(taskId):
    os.remove("taskExecutions/" + taskId)

def createTaskExecution(taskId, taskName, cloudName):
    taskJsCode = loadTask(taskName)
    newTaskExecution = { 
        'TaskId': taskId, 
        'Result': '', 
        'Status': "created",
        'TaskJsCode': taskJsCode,
        'TaskName': taskName,
        'CloudName': cloudName,
        'RootDevice': loadCloud(cloudName)["Devices"][0]
    }
    return newTaskExecution
