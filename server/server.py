import sys, os, json, traceback, httplib, time, socket
from flask import Flask, Response, request, redirect, make_response
from werkzeug.utils import secure_filename
import controller, models

if not os.path.exists('tasks'):
    os.makedirs('tasks')
if not os.path.exists('clouds'):
    os.makedirs('clouds')
if not os.path.exists('devices'):
    os.makedirs('devices')
if not os.path.exists('taskExecutions'):
    os.makedirs('taskExecutions')

UPLOAD_FOLDER = 'tasks'
ALLOWED_EXTENSIONS = set(['js'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/')
def index():
    with open("server.html", "r") as myfile:
        data = myfile.read()
    return Response(data, mimetype="text/html")

@app.route('/<kind>/<filename>')
def resources(kind, filename):
    try:
      with open(kind + "/" + filename, "r") as myfile:
          data = myfile.read()
      if kind == "js":
          return Response(data, mimetype="text/javascript")
      elif kind == "css":
          return Response(data, mimetype="text/css")
      elif kind == "fonts":
          return Response(data, mimetype="font")
      return Response(data)
    except:
        return traceback.format_exc(), 404

@app.route('/tasks', methods=['GET'])
def tasksResource():
    try:
        resultType = request.args.get('return', '')

        if resultType == 'names-list':
            namesList = models.listTasks()
            return Response(json.dumps(namesList), mimetype="application/json")
      
        return "only /tasks?return=names-list is supported", 400
    except:
        return traceback.format_exc(), 500

@app.route('/tasks', methods=['POST'])
def uploadTask():
    try:
      file = request.files['file']
      if file and allowed_file(file.filename):
          filename = secure_filename(file.filename)
          file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
      return redirect("/")
    except:
        return traceback.format_exc(), 500

@app.route('/tasks/<taskname>', methods=['GET'])
def downloadTaskFile(taskname):
    try:
        taskCode = models.loadTask(taskname)
        response = make_response(taskCode)
        response.headers["Content-Disposition"] = "attachment; filename=" + taskname
        return response
        
    except:
        return traceback.format_exc(), 400

@app.route('/tasks/<taskname>', methods=['DELETE'])
def deleteTask(taskname):
    try:
        models.deleteTask(taskname)
        return "OK", 200
    except:
        return traceback.format_exc(), 400

@app.route('/clouds', methods=['GET'])
def getClouds():
    try:
        clouds = models.loadClouds()
        return Response(json.dumps(clouds), mimetype="application/json")
        
    except:
        return traceback.format_exc(), 400

@app.route('/devices', methods=['GET'])
def devicesResource():
    try:
        devices = models.loadDevices()
        return Response(json.dumps(devices), mimetype="application/json")
        
    except:
        return traceback.format_exc(), 400

@app.route('/devices', methods=['POST'])
def handleDevices():
    try:        
        resultType = request.args.get('action', '')
        if resultType == 'rebuildInfo':
            devices = []
            for deviceAddress in models.listDevices():           
                print >> sys.stderr, 'Rebuilding info for ' + deviceAddress
                device = controller.getDeviceInformation(deviceAddress)
                models.saveDevice(device)
            devices = models.loadDevices()
            return Response(json.dumps(devices), mimetype="application/json")
        if resultType == 'removeUnavailableDevices':
            devicesList = models.loadDevices()
            for device in devicesList:
                if device['Status'] == 'unreachable':
                    models.deleteDevice(device['DeviceInformation']['IPAddress'])
            devicesList = models.loadDevices()
            return Response(json.dumps(devicesList), mimetype="application/json")
        if resultType == 'discover':
            networkInformation = json.loads(request.data)
            networkAddress = networkInformation["networkAddress"]
            networkMask = networkInformation["networkMask"]
            discoveredDevices = controller.discoverDevices(networkAddress, networkMask)
            for dev in discoveredDevices:
                models.saveDevice(dev)
            devicesList = models.loadDevices()
            return Response(json.dumps(devicesList), mimetype="application/json")
        if resultType == 'reset':
            devicesReseted = []
            for deviceAddress in models.listDevices():
                controller.resetDevice(deviceAddress)
                devicesReseted.append(deviceAddress)
            return Response(json.dumps(devicesReseted), mimetype="application/json")
        if resultType == '':
            deviceAddress = json.loads(request.data)["DeviceInformation"]["IPAddress"]
            newDevice = controller.getDeviceInformation(deviceAddress)
            models.saveDevice(newDevice)
            return Response(json.dumps(newDevice), mimetype="application/json")
        else:
            return "This operation is not allowed", 404

    except:
        return traceback.format_exc(), 400

@app.route('/clouds', methods=['POST'])
def handleClouds():
    try:        
        resultType = request.args.get('action', '')
        if resultType == '':
            cloudName = json.loads(request.data)["Name"]
            newCloud = models.createCloud(cloudName)
            models.saveCloud(newCloud)
            return Response(json.dumps(newCloud), mimetype="application/json")
        else:
            return "This operation is not allowed, yet ;)", 404

    except:
        return traceback.format_exc(), 400

@app.route('/addToCloud', methods=['POST'])
def buildClouds():
    device_port = 8080
    try:
        cloudName = request.args.get('cloudName', '')
        devicesCount = int(request.args.get('count', '1') )
        cloud = models.loadCloud(cloudName)
        allDevices = models.loadDevices()

        alreadyConnected = 0
        for device in allDevices:
            if alreadyConnected == int(devicesCount):
                break;
            if not device["CloudName"]:
                IPAddress = device["DeviceInformation"]["IPAddress"]
                try:
                    sock = socket.create_connection((IPAddress, device_port), 0.05)
                    deviceInformation = controller.getDeviceInformation(IPAddress)
                    if deviceInformation['Status'] != 'unreachable':
                        print >> sys.stderr, 'Adding ' + IPAddress + ' to cloud ' + cloudName
                    sock.shutdown(socket.SHUT_RDWR)
                    sock.close()

                    cloud["Devices"].append(IPAddress)
                    device["CloudName"] = cloudName

                    if len(cloud["Devices"]) > 1:
                        controller.connectDevice(IPAddress, cloud["Devices"][0])
                    
                    models.saveDevice(device)
                    models.saveCloud(cloud)
                    alreadyConnected += 1
                except (socket.timeout, socket.error, socket.herror, socket.gaierror) as e:
                    print >> sys.stderr, 'Skipping ' + IPAddress + ' for being unreachable'


        # For some reasons valid json parsable object MUST be returned ...
        return Response("{}", mimetype="application/json")
    except:
        return traceback.format_exc(), 400
        
@app.route('/devices/<ipaddress>', methods=['DELETE'])
def removeDevice(ipaddress):
    try:    
        models.deleteDevice(ipaddress)
        return "", 200

    except:
        return traceback.format_exc(), 400

def buildCloudStructure(rootAddress, parent="null"):
    treeRoot = {
        "parent": parent
    }
    print >> sys.stderr, 'building cloud structure for ' + rootAddress
    for childDevice in controller.getConnectedDevices(rootAddress)["ChildrenDevices"]:
        if ("children" in treeRoot) == False:
            treeRoot["children"] = []
        treeRoot["children"].append(buildCloudStructure(childDevice["IPAddress"], rootAddress))

    return treeRoot

@app.route('/clouds/<name>/structure', methods=['GET'])
def getCloudStructure(name):
    try:
        cloud = models.loadCloud(name)
        treeData = buildCloudStructure(cloud["Devices"][0], "null")
        print >> sys.stderr, 'Done! - building cloud structure .'
        return Response(json.dumps([treeData]), mimetype="application/json")
    except:
        return traceback.format_exc(), 400
        
@app.route('/clouds/<name>', methods=['DELETE'])
def removeCloud(name):
    try:    
        cloud = models.loadCloud(name)
        for deviceAddress in cloud["Devices"]:
            try:
                device = models.loadDevice(deviceAddress)
                if 'CloudName' in device and device['CloudName'] == name:
                    device['CloudName'] = ''
                    models.saveDevice(device)
                    controller.resetDevice(device["DeviceInformation"]["IPAddress"])
            except:
                pass
        models.removeCloud(name)
        return "", 200

    except:
        return traceback.format_exc(), 400

@app.route('/devices/connect', methods=['POST'])
def connectDeviceToCloud():
    try:
        connection = json.loads(request.data)
        device = models.loadDevice(connection["IPAddress"])
        cloud = models.loadCloud(connection["CloudName"])
        
        device["CloudName"] = connection["CloudName"]
        cloud["Devices"].append(connection["IPAddress"])

        if len(cloud["Devices"]) > 1:
            controller.connectDevice(connection["IPAddress"], cloud["Devices"][0])
        
        models.saveDevice(device)
        models.saveCloud(cloud)

        # For some reasons valid json parsable object MUST be returned ...
        return Response("{}", mimetype="application/json")
    except:
        return traceback.format_exc(), 400

@app.route('/taskExecutions', methods=['GET'])
def taskExecutionsResource():
    try:
        taskExecutions = models.loadTaskExecutions()
        return Response(json.dumps(taskExecutions), mimetype="application/json")
        
    except:
        return traceback.format_exc(), 400

@app.route('/taskExecutions/<taskid>/result', methods=['GET'])
def downloadTaskResult(taskid):
    try:
        taskExecution = models.loadTaskExecution(taskid)
        filename = taskExecution["TaskId"] + "_" + taskExecution["TaskName"] + "_" + \
                   taskExecution["CloudName"] + ".json"
        response = make_response(taskExecution['Result'])
        response.headers["Content-Disposition"] = "attachment; filename=" + filename
        return response
        
    except:
        return traceback.format_exc(), 400

@app.route('/taskExecutions/<taskid>/taskCode', methods=['GET'])
def downloadTaskCode(taskid):
    try:
        taskExecution = models.loadTaskExecution(taskid)
        taskCode = models.loadTask(taskExecution["TaskName"])
        response = make_response(taskCode)
        response.headers["Content-Disposition"] = "attachment; filename=" + taskExecution["TaskName"]
        return response
        
    except:
        return traceback.format_exc(), 400

@app.route('/taskExecutions', methods=['POST'])
def scheduleTask():
    try:        
        resultType = request.args.get('action', '')
        if resultType == 'rebuildInfo':
            for taskExecution in models.loadTaskExecutions():
                try:
                    taskResult = controller.getTaskResult(
                        taskExecution["RootDevice"], taskExecution["TaskId"]
                    )
                    if "Result" in taskResult:
                        taskExecution["Result"] = taskResult["Result"]
                    if "Status" in taskResult:
                        taskExecution["Status"] = taskResult["Status"]
                    models.saveTaskExecution(taskExecution)
                except:
                    pass
            return Response("{}", mimetype="application/json")
        else:
            task = json.loads(request.data)
            taskJsCode = models.loadTask(task["taskName"])
            cloud = models.loadCloud(task["cloudName"])
            resp = controller.scheduleTask(
                cloud["Devices"][0], taskJsCode, task["taskName"]
            )
            
            taskExecution = models.createTaskExecution(
                resp["TaskId"], task["taskName"], task["cloudName"]
            )
            
            models.saveTaskExecution(taskExecution)
            return Response(json.dumps(taskExecution), mimetype="application/json")
    except:
        return traceback.format_exc(), 400

@app.route('/taskExecutions/<taskId>', methods=['DELETE'])
def removeTaskExecution(taskId):
    try:    
        models.deleteTaskExecution(taskId)
        return "", 200

    except:
        return traceback.format_exc(), 400
