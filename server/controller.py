from subprocess import call
import httplib, urllib, json, time, models, socket, netaddr

def getDeviceInformation(ipAddress):
    device = models.loadDevice(ipAddress)
    device["Status"] = "ready"
    try:
        conn = httplib.HTTPConnection(ipAddress, 8080)
        conn.request('GET', 'commands/device/information');
        res = conn.getresponse()
        responseBody = res.read()
        data = json.loads(responseBody)
        device['DeviceInformation'].update(data['CurrentDeviceInformation'])
    except:
        device['Status'] = 'unreachable'
    
    return device;

def discoverDevices(networkAddress, networkMask):
    device_port = 8080
    discoveredDevices = []
    devs = list(netaddr.IPNetwork(networkAddress + "/" + networkMask).iter_hosts())
    for dev in devs:
        try:
            sock = socket.create_connection((str(dev), device_port), 0.05)
            deviceInformation = getDeviceInformation(str(dev))
            if deviceInformation['Status'] != 'unreachable':
                discoveredDevices.append(deviceInformation)
            sock.shutdown(socket.SHUT_RDWR)
            sock.close()
        except (socket.timeout, socket.error, socket.herror, socket.gaierror) as e:
            pass

    return discoveredDevices
		
def getTaskResult(ipAddress, taskId):		
    conn = httplib.HTTPConnection(ipAddress, 8080)
    conn.request('GET', 'tasks/' + taskId);
    res = conn.getresponse()
    responseBody = res.read()

    data = json.loads(responseBody)
    conn.close()

    return data

def resetDevice(ipAddress):
    conn = httplib.HTTPConnection(ipAddress, 8080)
    conn.connect()

    body = '';

    request = conn.putrequest('POST', '/commands/device/reset')
    headers = {}
    headers['Content-Type'] = 'application/json'
    headers['Content-Length'] = "%d"%(len(body));

    for k in headers:
        conn.putheader(k, headers[k])
    conn.endheaders()
    conn.send(body)

    resp = conn.getresponse()
    responseBody = resp.read()
    conn.close()

def getDeviceRoot(deviceAddress):
    conn = httplib.HTTPConnection(deviceAddress, 8080)
    conn.request('GET', 'commands/device/root');
    res = conn.getresponse()
    responseBody = res.read()
    conn.close()
    return json.loads(responseBody)

def getConnectedDevices(deviceAddress):
    conn = httplib.HTTPConnection(deviceAddress, 8080)
    conn.request('GET', 'commands/device/children');
    res = conn.getresponse()
    responseBody = res.read()
    data = json.loads(responseBody)
    conn.close()
    return data

def connectDevice(deviceAddress, rootAddress):
    resetDevice(deviceAddress)
    time.sleep(1)

    conn = httplib.HTTPConnection(deviceAddress, 8080)
    conn.connect()
    
    body = '{ "DestinationIP" : "' + rootAddress + '" }';
    
    conn.putrequest('POST', '/commands/connect')
    headers = {}
    headers['Content-Type'] = 'application/json'
    headers['Content-Length'] = "%d"%(len(body));
    
    for k in headers:
        conn.putheader(k, headers[k])
    conn.endheaders()
    conn.send(body)
    
    resp = conn.getresponse()
    timeouts = 0
    while True:
        time.sleep(2)
        data = getDeviceRoot(deviceAddress)
        if "RootDevice" in data:
            break
        timeouts += 1
        if timeouts > 10:
            raise Exception("device cannot be connected to cloud")

def scheduleTask(deviceAddress, taskJsCode, taskName):
    conn = httplib.HTTPConnection(deviceAddress, 8080)
    conn.connect()

    taskRequest = { 'TaskJsCode': taskJsCode, 'TaskName': taskName }
    body = json.dumps(taskRequest);

    conn.putrequest('POST', '/commands/tasks')
    headers = {}
    headers['Content-Type'] = 'application/json'
    headers['Content-Length'] = "%d"%(len(body));

    for k in headers:
        conn.putheader(k, headers[k])
    conn.endheaders()
    conn.send(body)

    resp = conn.getresponse().read()
    conn.close()
    return json.loads(resp)
