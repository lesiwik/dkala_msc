function TaskExecutionsManagerModel(controlCenter) {
    var self = this,
        downloadTaskExecutionsListRequest,
        removeTaskExecutionRequest,
        refreshAllTaskExecutionsRequest;

    self.isDownloadingTaskExecutionsList = ko.observable(false);
    self.isRemovingTaskExecution = ko.observable(false);
    self.isRefreshingAllTaskExecutions = ko.observable(false);

    self.TaskNameFilter = ko.observable();
    self.CloudNameFilter = ko.observable();

    self.IsLoading = ko.computed(function() {
        return self.isDownloadingTaskExecutionsList() || self.isRemovingTaskExecution() ||
               self.isRefreshingAllTaskExecutions();
    }, this); 

    self.AvailableTaskExecutions = ko.observableArray();

    self.FilteredTaskExecutions = ko.computed(function() {
        var taskNameFilter = self.TaskNameFilter(),
            cloudNameFilter = self.CloudNameFilter(),

            afterTaskFilter, afterCloudFilter;

        if (taskNameFilter) {
            afterTaskFilter = _(self.AvailableTaskExecutions()).filter(function (taskExecution) {
                return taskExecution.TaskName.indexOf(taskNameFilter) > -1;
            });
        } else {
            afterTaskFilter = self.AvailableTaskExecutions();
        }
  
        if (cloudNameFilter) {
            afterCloudFilter = _(afterTaskFilter).filter(function (taskExecution) {
                return taskExecution.CloudName.indexOf(cloudNameFilter) > -1;
            });
        } else {
            afterCloudFilter = afterTaskFilter;
        }

        return afterCloudFilter;
    });

    self.downloadTaskExecutionsList = function() {
        if (downloadTaskExecutionsListRequest) {
            return downloadTaskExecutionsListRequest;
        }
        self.isDownloadingTaskExecutionsList(true);

        downloadTaskExecutionsListRequest = $.ajax({
            url: "/taskExecutions",
            dataType: "json"
        }).pipe(function(taskExecutions) {
            self.AvailableTaskExecutions(_(taskExecutions).map(function (taskExecution) {
                return new TaskExecutionModel(taskExecution);
            }));
        }).always(function() {
            self.isDownloadingTaskExecutionsList(false);
            downloadTaskExecutionsListRequest = null;
        });
        return downloadTaskExecutionsListRequest;
    };

    self.refreshAllTaskExecutions = function() {
        if (refreshAllTaskExecutionsRequest) {
            return refreshAllTaskExecutionsRequest;
        }
        self.isRefreshingAllTaskExecutions(true);

        refreshAllTaskExecutionsRequest = $.ajax({
            url: "/taskExecutions?action=rebuildInfo",
            dataType: "json",
            type: "POST"
        }).pipe(function() {
            return self.downloadTaskExecutionsList();
        }).always(function() {
            self.isRefreshingAllTaskExecutions(false);
            refreshAllTaskExecutionsRequest = null;
        });
        return refreshAllTaskExecutionsRequest;
    };

    self.downloadTaskResult = function(taskExecutionModel) {
        window.location.href = "/taskExecutions/" + taskExecutionModel.TaskId + "/result";
    };

    self.downloadTaskJsCode = function(taskExecutionModel) {
        window.location.href = "/taskExecutions/" + taskExecutionModel.TaskId + "/taskCode";
    };

    self.remove = function(taskExecutionModel) {
        if (removeTaskExecutionRequest) {
            return removeTaskExecutionRequest;
        }
        self.isRemovingTaskExecution(true);

        removeTaskExecutionRequest = $.ajax({
            url: "/taskExecutions/" + taskExecutionModel.TaskId,
            type: "DELETE"
        }).pipe(function() {
            return self.downloadTaskExecutionsList();
        }).always(function() {
            self.isRemovingTaskExecution(false);
            removeTaskExecutionRequest = null;
        });
        return removeTaskExecutionRequest;
    };

    self.downloadTaskExecutionsList();
};
