function CloudsManagerModel(controlCenter) {
    var self = this,
        refreshCloudsListRequest,
        addNewCloudRequest,
        removeCloudRequest,
        downloadCloudStructureRequest;

    self.isRefreshingCloudsList = ko.observable(false);
    self.isAddingNewCloud = ko.observable(false);
    self.isRemovingCloud = ko.observable(false);

    self.CloudNameFilter = ko.observable();
    self.SelectedCloud = ko.observable();
    self.AvailableClouds = ko.observableArray([]);
    self.NewCloudName = ko.observable();
    self.ActiveCloud = ko.observable();

    self.FilteredDevices = ko.computed(function() {
        var cloudNameFilter = self.CloudNameFilter(),
            beforeCloudFilter, afterCloudFilter;

        beforeCloudFilter = controlCenter.devicesManagerModel() &&
                            controlCenter.devicesManagerModel().AvailableDevices();
 
        if (!beforeCloudFilter) {
            return;
        }
  
        if (cloudNameFilter) {
            afterCloudFilter = _(beforeCloudFilter).filter(function (taskExecution) {
                var cloudName = taskExecution.CloudName();
                if (!cloudName) {
                    return false;
                }
                return taskExecution.CloudName().indexOf(cloudNameFilter) > -1;
            });
        } else {
            afterCloudFilter = beforeCloudFilter;
        }

        return afterCloudFilter;
    }, this);

    self.IsLoading = ko.computed(function() {
        return self.isRefreshingCloudsList() || self.isAddingNewCloud() || self.isRemovingCloud();
    }, true);

    self.CanAddCloud = ko.computed(function() {
        var cloudName;
        if (!self.NewCloudName()) {
            return false;
        }

        cloudName = self.NewCloudName().trim();
        if (!cloudName) {
            return false;
        }

        return !_(self.AvailableClouds()).find(function(cloud) {
            return cloud.Name === cloudName;
        });
    }, this);

    self.makeActive = function(cloud)  {
        if (self.ActiveCloud()) {
            self.ActiveCloud().IsActive(false);
        }
        self.ActiveCloud(cloud);
        cloud.IsActive(true);
    };

    self.listDevices = function(cloud) {
        self.CloudNameFilter(cloud.Name);
        $('#list-clouds-devices-modal').modal('show');
    };

    self.IsLoadingCloudStructure = ko.observable(false);

    self.showCloud = function(cloud) {
        if (downloadCloudStructureRequest) {
            return downloadCloudStructureRequest;
        }

        self.IsLoadingCloudStructure(true);

        downloadCloudStructureRequest = $.ajax({
            url: "/clouds/" + cloud.Name + "/structure",
            dataType: 'json'
        }).done(function(treeData) {
            var mydiv = document.getElementById('clouds-tree');
            while ( mydiv.firstChild ) mydiv.removeChild( mydiv.firstChild );

            // ************** Generate the tree diagram  *****************
            var margin = {top: 20, right: 80, bottom: 20, left: 80},
             width = $('.modal-dialog.cloud-structure').width() - margin.right - margin.left,
             height = $('.modal-dialog.cloud-structure').height() - margin.top - margin.bottom;
             
            var i = 0;

            var tree = d3.layout.tree().size([height, width]);

            var diagonal = d3.svg.diagonal().projection(function(d) { 
                return [d.y, d.x]; 
            });

            var svg = d3.select("#clouds-tree").append("svg")
             .attr("width", width + margin.right + margin.left)
             .attr("height", height + margin.top + margin.bottom)
              .append("g")
             .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            root = treeData[0];
              
            update(root);

            function update(source) {

              // Compute the new tree layout.
              var nodes = tree.nodes(root).reverse(),
               links = tree.links(nodes);

              // Normalize for fixed-depth.
              nodes.forEach(function(d) { d.y = d.depth * 80; });

              // Declare the nodesâ€¦
              var node = svg.selectAll("g.node")
               .data(nodes, function(d) { return d.id || (d.id = ++i); });

              // Enter the nodes.
              var nodeEnter = node.enter().append("g")
               .attr("class", "node")
               .attr("transform", function(d) { 
                return "translate(" + d.y + "," + d.x + ")"; });

              nodeEnter.append("circle")
               .attr("r", 10)
               .style("fill", "#fff");

              nodeEnter.append("text")
               .attr("x", function(d) { 
                return d.children || d._children ? -13 : 13; })
               .attr("dy", ".35em")
               .attr("text-anchor", function(d) { 
                return d.children || d._children ? "end" : "start"; })
               .text(function(d) { return d.name; })
               .style("fill-opacity", 1);

              // Declare the linksâ€¦
              var link = svg.selectAll("path.link")
               .data(links, function(d) { return d.target.id; });

              // Enter the links.
              link.enter().insert("path", "g")
               .attr("class", "link")
               .attr("d", diagonal);

            }
        }).fail(function(xhr) {

        }).always(function() {
            self.IsLoadingCloudStructure(false);
            downloadCloudStructureRequest = null;
        });
        $('#draw-clouds-structure-modal').modal('show');
    };

    self.listTasks = function(cloud) {
        controlCenter.taskExecutionsManagerModel().CloudNameFilter(cloud.Name);
        $('#task-executions-modal').modal('show');
    };

    self.addCloud = function() {
        if (addNewCloudRequest) {
            return addNewCloudRequest;
        }
  
        self.isAddingNewCloud(true);
        controlCenter.devicesManagerModel().isRefreshingDevicesList(true)

        addNewCloudRequest = $.ajax({
            url: "/clouds",
            type: "POST",
            data: JSON.stringify({
                Name: self.NewCloudName().trim()
            }),
            dataType: 'json',
            contentType:"application/json; charset=utf-8"
        }).pipe(function(newCloud) {
            self.AvailableClouds.push(new CloudModel(newCloud));
            return controlCenter.devicesManagerModel().downloadDevicesList();
        }).fail(function(xhr) {

        }).always(function() {
            self.isAddingNewCloud(false);
            addNewCloudRequest = null;
        });
        return addNewCloudRequest;
    };
    self.updateClouds = function() {
        $('#comming-soon-modal').modal('show');
    };
    self.removeCloud = function(cloud) {
        if (removeCloudRequest) {
            return removeCloudRequest;
        }
        self.isRemovingCloud(true);
        controlCenter.devicesManagerModel().isRefreshingDevicesList(true)

        removeCloudRequest = $.ajax({
            url: "/clouds/" + cloud.Name,
            type: "DELETE"
        }).pipe(function() {
            self.AvailableClouds.remove(cloud);
            if (self.ActiveCloud() === cloud) {
                self.ActiveCloud(null);
            }
            if (self.SelectedCloud() === cloud) {
                self.SelectedCloud(null);
            }
            return controlCenter.devicesManagerModel().downloadDevicesList();
        }).always(function() {
            self.isRemovingCloud(false);
            removeCloudRequest = null;
        });
        return removeCloudRequest;
    };
    self.invoke = function(cloud) {
        self.SelectedCloud(cloud);
        $('#run-task-modal').modal('show');
    };

    function refreshClouds(clouds) {
        var cloudModels, newActiveCloud;

        cloudModels = _(clouds).map(function (cloud) {
            return new CloudModel(cloud);
        });
        if (self.ActiveCloud()) {
            newActiveCloud = _(cloudModels).find(function(cloud) {
                return cloud.Name === self.ActiveCloud().Name;
            });
            if (newActiveCloud) {
                newActiveCloud.IsActive(true);
                self.ActiveCloud(newActiveCloud);
            } else {
                self.ActiveCloud(null);
            }
        }
        self.AvailableClouds(cloudModels);
    }

    self.downloadCloudsList = function () {
        if (refreshCloudsListRequest) {
            return refreshCloudsListRequest;
        }
        self.isRefreshingCloudsList(true);

        refreshCloudsListRequest = $.ajax({
            url: "/clouds"
        }).done(function(clouds) {
            refreshClouds(clouds);
        }).always(function() {
            self.isRefreshingCloudsList(false);
            refreshCloudsListRequest = null;
        });
        return refreshCloudsListRequest;
    }

    self.downloadCloudsList();
};
