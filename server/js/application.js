function ControlCenter (){
    var self = this,
        isRunningTask = ko.observable(false);    

    self.IsLoading = ko.computed(function() {
        return isRunningTask();
    }, this);

    self.tasksManagerModel = ko.observable();
    self.cloudsManagerModel = ko.observable();
    self.devicesManagerModel = ko.observable();
    self.taskExecutionsManagerModel = ko.observable();

    self.tasksManagerModel(new TasksManagerModel(this));
    self.cloudsManagerModel(new CloudsManagerModel(this));
    self.devicesManagerModel(new DevicesManagerModel(this));
    self.taskExecutionsManagerModel(new TaskExecutionsManagerModel(this));

    self.CanRunTask = ko.computed(function() {
        return self.cloudsManagerModel().SelectedCloud() &&
               self.tasksManagerModel().SelectedTask();
    }, this);

    self.runTask = function() {
        isRunningTask(true);
        self.taskExecutionsManagerModel().isDownloadingTaskExecutionsList(true);
 
        return $.ajax({
            url: "/taskExecutions",
            type: "POST",
            dataType: "json",
            contentType:"application/json; charset=utf-8",
            data: JSON.stringify({
                taskName: self.tasksManagerModel().SelectedTask().taskName,
                cloudName: self.cloudsManagerModel().SelectedCloud().Name
            })
        }).pipe(function(response){
            return self.taskExecutionsManagerModel().downloadTaskExecutionsList();
        }).done(function() {
            $('#task-executions-modal').modal('show');
        }).always(function() {
            isRunningTask(false);
        });
    };
};

ko.bindingHandlers.preloader = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        ko.bindingHandlers.preloader.update(element, valueAccessor, allBindings, viewModel, bindingContext);
    },

    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        if (valueAccessor()()) {
            if ($('#' + element.id + '-preloader').length !== 0) {
                return;
            }

            $('body').append(
'<div id="' + element.id + '-preloader" style="z-index: 100001; position: absolute; top: 0px; left: 0px; width:100%; height:100%">' +
'    <div id="floatingCirclesG" style="margin: auto auto;">' +
'      <div class="f_circleG" id="frotateG_01"></div> ' +
'      <div class="f_circleG" id="frotateG_02"></div> ' +
'      <div class="f_circleG" id="frotateG_03"></div> ' +
'      <div class="f_circleG" id="frotateG_04"></div> ' +
'      <div class="f_circleG" id="frotateG_05"></div> ' +
'      <div class="f_circleG" id="frotateG_06"></div> ' +
'      <div class="f_circleG" id="frotateG_07"></div> ' +
'      <div class="f_circleG" id="frotateG_08"></div> ' +
'    </div> ' +
'</div> ' +
'<div id="' + element.id + '-preloader-background" style="z-index: 100000; position: absolute; background: #ffffff; opacity:0.5"></div>'
);
            $('#' + element.id + '-preloader').offset($(element).offset());
            $('#' + element.id + '-preloader').css({
                height :($(element).height() + 'px')
            });
            $('#' + element.id + '-preloader').css({
                width :($(element).width() + 'px')
            });
            $('#' + element.id + '-preloader-background').offset($(element).offset());
            $('#' + element.id + '-preloader-background').css({
                height :($(element).height() + 'px')
            });
            $('#' + element.id + '-preloader-background').css({
                width :($(element).width() + 'px')
            });
        } else {
            $('#' + element.id + '-preloader').remove();
            $('#' + element.id + '-preloader-background').remove();
        }
    }
};

ko.applyBindings(new ControlCenter());
