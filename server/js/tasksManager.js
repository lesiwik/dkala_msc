function TasksManagerModel(controlCenter) {
    var self = this,
        refreshTasksListRequest,
        removeTaskRequest;

    self.isTaskListBeingRefreshed = ko.observable(false),
    self.isElementBeingRemoved = ko.observable(false);

    self.SelectedTask = ko.observable();
    self.AvailableTasks = ko.observableArray([]);
    self.IsLoading = ko.computed(function() {
        return self.isTaskListBeingRefreshed() || self.isElementBeingRemoved();
    }, this);
    self.SelectedFiles = ko.observable();

    self.onFilesSelected = function(files) {
        self.SelectedFiles(_(files).map(function(file) {
            return file.name;
        }).join(', '));
    };

    self.invoke = function(task) {
        self.SelectedTask(task);
        $('#run-task-modal').modal('show');
    };

    self.show = function(taskModel) {
        controlCenter.taskExecutionsManagerModel().TaskNameFilter(taskModel.taskName);
        $('#task-executions-modal').modal('show');
    };

    self.download = function(taskModel) {
        window.location.href = "/tasks/" + taskModel.taskName;
    };

    self.remove = function(taskModel) {
        if (removeTaskRequest) {
            return removeTaskRequest;
        }

        self.isElementBeingRemoved(true);
        removeTaskRequest = $.ajax({
            url: "/tasks/" + taskModel.taskName,
            type: "DELETE"
        }).pipe(function() {
            return self.refreshTaskList();
        }).always(function() {
            self.isElementBeingRemoved(false);
            removeTaskRequest = null;
        });
        return removeTaskRequest;
    };

    self.refreshTaskList = function() {
        if (refreshTasksListRequest) {
            return refreshTasksListRequest;
        }
        self.isTaskListBeingRefreshed(false);
        refreshTasksListRequest = $.ajax({
            url: "/tasks?return=names-list"
        }).done(function(taskNames) {
            var taskModels = _(taskNames).map(function (taskName) {
                return new TaskModel(taskName);
            });
            self.AvailableTasks(taskModels);
        }).always(function() {
            self.isTaskListBeingRefreshed(false);
            refreshTasksListRequest = null;
        });
        return refreshTasksListRequest;
    };

    self.refreshTaskList();
};
