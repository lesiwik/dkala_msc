function DevicesManagerModel(controlCenter) {
    var self = this,
        refreshDevicesListRequest,
        addNewDeviceRequest,
        removeNewDeviceRequest,
        pingDevicesRequest,
        discoverDevicesRequest,
        removeUnavailableDevicesRequest,
        connectDeviceToCloudRequest,
        resetAllDevicesRequest;

    self.isRefreshingDevicesList = ko.observable(false);
    self.isAddingNewDevice = ko.observable(false);
    self.isRemovingNewDevice = ko.observable(false);
    self.isPingingDevices = ko.observable(false);
    self.isDiscoveringDevices = ko.observable(false);
    self.isRemovingUnavailableDevices = ko.observable(false);
    self.isConnectingDeviceToCloud = ko.observable(false);
    self.isResetingAllDevices = ko.observable(false);

    self.NetworkAddress = ko.observable();
    self.NetworkMask = ko.observable();
    self.NewDeviceIp = ko.observable();
    self.AvailableDevices = ko.observableArray([]);
    self.IsLoading = ko.computed(function() {
        return self.isRefreshingDevicesList() || self.isPingingDevices() || 
               self.isAddingNewDevice() || self.isRemovingNewDevice() || 
               self.isRemovingUnavailableDevices() || self.isConnectingDeviceToCloud() ||
               self.isDiscoveringDevices() || self.isResetingAllDevices();
    }, true);

    self.CanAddDevice = ko.computed(function() {
        var deviceAddress;
        if (!self.NewDeviceIp()) {
            return false;
        }

        deviceAddress = self.NewDeviceIp().trim();
        if (!deviceAddress) {
            return false;
        }

        return !_(self.AvailableDevices()).find(function(device) {
            return device.DeviceInformation.IPAddress === deviceAddress;
        });
    }, this);

    self.CanConnectToCloud = ko.computed(function() {
        var activeCloud = controlCenter.cloudsManagerModel() &&
                          controlCenter.cloudsManagerModel().ActiveCloud();
        return !!activeCloud;
    });

    function refreshDevices(devices) {
        var deviceModels = _(devices).map(function (device) {
            return new DeviceModel(device, self);
        });
        self.AvailableDevices(deviceModels);
    }

    self.connect = function(device) {
        if (connectDeviceToCloudRequest) {
            return connectDeviceToCloudRequest;
        }
  
        self.isConnectingDeviceToCloud(true);
        controlCenter.cloudsManagerModel().isRefreshingCloudsList(true);

        connectDeviceToCloudRequest = $.ajax({
            url: "/devices/connect",
            type: "POST",
            data: JSON.stringify({
                IPAddress: device.DeviceInformation.IPAddress,
                CloudName: controlCenter.cloudsManagerModel().ActiveCloud().Name
            }),
            dataType: "json",
            contentType:"application/json; charset=utf-8"
        }).pipe(function() {
            return $.when(
                self.downloadDevicesList(),
                controlCenter.cloudsManagerModel().downloadCloudsList()
            );
        }).fail(function(xhr) {

        }).always(function() {
            self.isConnectingDeviceToCloud(false);
            connectDeviceToCloudRequest = null;
        });

        return connectDeviceToCloudRequest;
    };

    self.addDevice = function() {
        if (addNewDeviceRequest) {
            return addNewDeviceRequest;
        }
  
        self.isAddingNewDevice(true);

        addNewDeviceRequest = $.ajax({
            url: "/devices",
            type: "POST",
            data: JSON.stringify({
                DeviceInformation: {
                    IPAddress: self.NewDeviceIp().trim()
                }
            }),
            dataType: 'json',
            contentType:"application/json; charset=utf-8"
        }).done(function(newDevice) {
            self.AvailableDevices.push(
                new DeviceModel(newDevice, self)
            );
        }).fail(function(xhr) {

        }).always(function() {
            self.isAddingNewDevice(false);
            addNewDeviceRequest = null;
        });
        return addNewDeviceRequest;
    };

    self.removeDevice = function(device) {
        if (removeNewDeviceRequest) {
            return removeNewDeviceRequest;
        }
        self.isRemovingNewDevice(true);

        removeNewDeviceRequest = $.ajax({
            url: "/devices/" + device.DeviceInformation.IPAddress,
            type: "DELETE"
        }).done(function() {
            self.AvailableDevices.remove(device);
        }).fail(function(xhr) {

        }).always(function() {
            self.isRemovingNewDevice(false);
            removeNewDeviceRequest = null;
        });
        return removeNewDeviceRequest;
    };

    self.downloadDevicesList = function () {
        if (refreshDevicesListRequest) {
            return refreshDevicesListRequest;
        }
        self.isRefreshingDevicesList(true);

        refreshDevicesListRequest = $.ajax({
            url: "/devices"
        }).done(function(devices) {
            refreshDevices(devices);
        }).always(function() {
            self.isRefreshingDevicesList(false);
            refreshDevicesListRequest = null;
        });
        return refreshDevicesListRequest;
    }

    self.pingDevices = function() {
        if (pingDevicesRequest) {
            return pingDevicesRequest;
        }
        self.isPingingDevices(true);

        pingDevicesRequest = $.ajax({
            url: "/devices?action=rebuildInfo",
            type: "POST"
        }).done(function(devices) {
            refreshDevices(devices);
        }).always(function() {
            self.isPingingDevices(false);
            pingDevicesRequest = null;
        });
        return pingDevicesRequest;
    };

    self.removeUnavailableDevices = function() {
        if (removeUnavailableDevicesRequest) {
            return removeUnavailableDevicesRequest;
        }
        self.isRemovingUnavailableDevices(true);

        removeUnavailableDevicesRequest = $.ajax({
            url: "/devices?action=removeUnavailableDevices",
            type: "POST"
        }).done(function(devices) {
            refreshDevices(devices);
        }).always(function() {
            self.isRemovingUnavailableDevices(false);
            removeUnavailableDevicesRequest = null;
        });
        return removeUnavailableDevicesRequest;
    };

    self.resetAllDevices = function() {
        if (resetAllDevicesRequest) {
            return resetAllDevicesRequest;
        }
        self.isResetingAllDevices(true);

        resetAllDevicesRequest = $.ajax({
            url: "/devices?action=reset",
            type: "POST"
        }).always(function() {
            self.isResetingAllDevices(false);
            resetAllDevicesRequest = null;
        });
        return resetAllDevicesRequest;
    };

    self.discoverDevices = function() {
        if (discoverDevicesRequest) {
            return discoverDevicesRequest;
        }
        self.isDiscoveringDevices(true);

        discoverDevicesRequest = $.ajax({
            url: "/devices?action=discover",
            type: "POST",
            data: JSON.stringify({
                networkAddress: self.NetworkAddress(),
                networkMask: self.NetworkMask()
            }),
            dataType: "json",
            contentType:"application/json; charset=utf-8"
        }).done(function(devices) {
            refreshDevices(devices);
        }).always(function() {
            self.isDiscoveringDevices(false);
            discoverDevicesRequest = null;
        });

        return discoverDevicesRequest;
    };

    self.downloadDevicesList();
};
