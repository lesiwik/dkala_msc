function DeviceModel(deviceData, deviceManager) {
    var self = this;

    _(self).extend(deviceData);

    self.CloudName = ko.observable(deviceData.CloudName);    
    self.IsConnectedToCloud = ko.observable(!!deviceData.CloudName);
    self.CanConnectToCloud = ko.computed(function() {
        return !self.IsConnectedToCloud() && deviceManager.CanConnectToCloud() && 
                self.Status === "ready";
    }, this);
};
