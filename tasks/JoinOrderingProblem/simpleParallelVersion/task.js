function HashedList(values) {
    var self = this, first, last, nodes = [];

    function Node(value, prev, next) {
        var self = this;

        self.value = value;
        self.prev = prev;
        self.next = next;
    }

    self.getFirstValue = function () {
        return first.value;
    };

    self.appendValue = function (value) {
        var node;

        node = new Node(value, last);
        if (!first) {
            first = node;
        }
        if (last) {
            last.next = node;
        }
        last = node;
        nodes[value] = node;
    };

    self.removeValue = function(value) {
        var node = nodes[value];

        if (node.prev) {
            node.prev.next = node.next;
        }
        if (node.next) {
            node.next.prev = node.prev;
        }
        if (first === node) {
            first = node.next;
        }
        if (last === node) {
            last = node.prev;
        }
    };

    self.getNextValue = function(value) {
        var node = nodes[value];
        if (node.next) {
            return node.next.value;
        } else {
            return null;
        }
    };

    if (values) {
        values.forEach(function(value) {
            self.appendValue(value);
        });
    }
}

function array_contains(a, obj) {
    var i = a.length;
    while (i--) {
       if (a[i] === obj) {
           return true;
       }
    }
    return false;
}

function get_join_relations_cost(relation_1_name, relation_2_name){
   
    var idx1 = parseInt(relation_1_name.substr(1)) - 1; /* for R1 -> 0, R10 -> 9*/
    var idx2 = parseInt(relation_2_name.substr(1)) - 1;
    
    var costs = [
            [9999,10,20,5,25,30,35,2,50,50,20,30,35,30,60],
            [10,9999,15,35,30,30,15,10,10,30,35,45,25,35,55],
            [20,15,9999,45,15,15,20,15,20,25,40,35,40,45,35],
            [5,35,45,9999,45,45,45,45,40,30,45,15,40,35,80],
            [25,30,15,45,9999,20,20,10,60,15,10,30,55,55,55],
            [30,30,15,45,20,9999,25,20,35,25,20,30,65,45,10],
            [35,15,20,45,20,25,9999,20,40,30,25,20,70,40,30],
            [2,10,15,45,10,20,20,9999,25,20,30,25,10,35,40],
            [50,10,20,40,60,35,40,25,9999,25,20,55,60,25,45],
            [50,30,25,30,15,25,30,20,25,9999,55,65,55,20,100],
            [20,35,40,45,10,20,25,30,20,55,9999,60,50,30,55],
            [30,45,35,15,30,30,20,25,55,65,60,9999,15,15,60],
            [35,25,40,40,55,65,70,10,60,55,50,15,9999,45,55],
            [30,35,45,35,55,45,40,35,25,20,30,15,45,9999,50],
            [60,55,35,80,55,10,30,40,45,100,55,60,55,50,9999]
    ];
    return costs[idx1][idx2];
}

function get_all_join_cost(ordered_relations_names){
    var cost_sum = 0;
    for (var i = 0; i < ordered_relations_names.length - 1; ++i) {
        var r1 = ordered_relations_names[i];
        var r2 = ordered_relations_names[i+1];
        cost_sum += get_join_relations_cost(r1, r2);
    }
    return cost_sum;
}

function get_creature_fitness(creature){
    return 1.0 / get_all_join_cost(creature);
}

var my_seed = 1;

function rand_int(up_to, different_then_number){
    
    function __rand_int(up_to){
        
        return Math.floor(Math.random() * 1000.0 * my_seed) % up_to;
    }

    var rand = __rand_int(up_to);
    while(different_then_number == rand){
        rand = __rand_int(up_to);
    }
    return rand;
}

function get_random_of_two_elements(el_1, el_2){
    var a = [el_1, el_2];
    return a[rand_int(2)];
}

function permutate(unique_element_list){
    var permutation = [];
    var unique_element_list_cpy = unique_element_list.slice(0);
    while( unique_element_list_cpy.length > 0 ){
        var idx = rand_int(unique_element_list_cpy.length);    
        permutation.push(unique_element_list_cpy[idx]);
        unique_element_list_cpy.splice(idx, 1);
    }
    return permutation;
}

function leave_only_the_best_creatures(creatures, number_of_best_creature_to_survive){
    creatures.sort(function(creature_1, creature_2){
        return get_creature_fitness(creature_2) - get_creature_fitness(creature_1);
    });
    return creatures.slice(0, number_of_best_creature_to_survive);
}

function create_population(population_size, unique_relations_names){
    var population = [];
    var existance = {};
    for (var i = 0; i < population_size; i++) {
        var gen_permutation = permutate(unique_relations_names);
        while( existance[gen_permutation] !== undefined){
             gen_permutation = permutate(unique_relations_names);
        }
        existance[gen_permutation] = true;
        population.push(gen_permutation);
    }
    
    return population;
}

function mutate(creature){

    var mutation_count = creature.length * 0.05; 
    if (mutation_count === 0) {
        mutation_count = 1;
    }

    var mutated_creature = creature.slice(0);
    for (var i = 0; i < mutation_count; i++) {
        var idx_1 = rand_int(creature.length);
        var idx_2 = rand_int(creature.length, idx_1);
        mutated_creature[idx_2] = creature[idx_1];
        mutated_creature[idx_1] = creature[idx_2];
    }
    
    return mutated_creature;
}

function crossover_scx(creature_1_gens, creature_2_gens, ordered_relations_names){
    var initial_relation_name = get_random_of_two_elements(creature_1_gens[0], creature_2_gens[0]);

    var nextX, nextY, nextValue,
        new_creature_gens = [initial_relation_name],
        hashedListX = new HashedList(creature_1_gens),
        hashedListY = new HashedList(creature_2_gens),
        orderedHashedList = new HashedList();
        
    ordered_relations_names.forEach(function(element, index){
        orderedHashedList.appendValue(element);
    });

    function getNextFreeValue(currentValue) {
        var value;

        value = orderedHashedList.getFirstValue();
        if (value == currentValue) {
            value = orderedHashedList.getNextValue(value);
        }

        return value;
    }

    var currentValue = initial_relation_name;

    while (new_creature_gens.length < creature_1_gens.length) {
        nextX = hashedListX.getNextValue(currentValue);
        if (nextX === null) {
            nextX = getNextFreeValue(currentValue);
        }
        nextY = hashedListY.getNextValue(currentValue);
        if (nextY === null) {
            nextY = getNextFreeValue(currentValue);
        }

        if (nextX == nextY) {
            nextValue = nextX;
        } else {
            var costX = get_join_relations_cost(currentValue, nextX);
            var costY = get_join_relations_cost(currentValue, nextY);

            if (costX < costY) {
                nextValue = nextX;
            } else {
                nextValue = nextY;
            }
        }

        orderedHashedList.removeValue(currentValue);
        hashedListX.removeValue(currentValue);
        hashedListY.removeValue(currentValue);
        currentValue = nextValue;
        new_creature_gens.push(currentValue);
    }
    return new_creature_gens;
}

function generaten_new_offspring(best_creatures, ordered_relations_names){
    var new_population = [];
    for (var index = 0; index < best_creatures.length - 2; index++) {
        
        var idx_1 = rand_int(index+1);
        var idx_2 = rand_int(index+1, idx_2);

        var creature_1_gens = best_creatures[index];
        var creature_2_gens = best_creatures[idx_1];
        var creature_3_gens = best_creatures[idx_2];

        var new_x = crossover_scx(creature_1_gens, creature_2_gens, ordered_relations_names);
        var new_y = crossover_scx(creature_1_gens, creature_3_gens, ordered_relations_names);

        new_x = mutate(new_x);
        new_y = mutate(new_y);

        new_population.push(new_x);
        new_population.push(new_y);
    }   
    return new_population;
}

function find_best_solution(ordered_relations_names, population_size, number_of_best_creature_to_survive, iteration_number){
    var initial_population = create_population(population_size, ordered_relations_names);
    var best_solution;
    var current_population = initial_population;
        
    for (var i = 0; i < iteration_number; i++) {
        var best_creatures = leave_only_the_best_creatures(current_population, number_of_best_creature_to_survive);
        var new_offspring = generaten_new_offspring(best_creatures, ordered_relations_names);
        current_population = current_population.concat(new_offspring);
    }

    return leave_only_the_best_creatures(current_population, 1)[0];
}


var devicesCount = Platform.getDevicesCount();
var deviceNumber = Platform.getDeviceNumber();

my_seed = (deviceNumber +1 )* 163*13*11;

var ITERATION_NUMBER = 10;
var POPULATION_SIZE = 200;

var NUMBER_OF_BEST_CREATURE_TO_SURVIVE = POPULATION_SIZE / 2;

var DATA_SERVER_IP = "192.168.31.5";
var DATA_SERVER_PORT = 5002;

var data = ["R1","R2","R3","R4","R5","R6","R7","R8","R9","R10","R11","R12","R13"];



if (deviceNumber === 0) {
    PlatformTools.sendDataToServer(
        "http://"+ DATA_SERVER_IP+ ":" + DATA_SERVER_PORT + "/measurements", { taskId: Platform.getTaskId(),devicesCount: devicesCount }   
    );
}
var rootNumber = (function getRootNumber() {
    if (deviceNumber === 0) return;
    return Math.floor((deviceNumber - 1) / 2);
})();

var leftChildNumber = (function getLeftChildNumber() {
    var number = deviceNumber * 2 + 1;
    if (number < devicesCount) {
        return number;
    }
})();

var rightChildNumber = (function getRightChildNumber() {
    var number = deviceNumber * 2 + 2;
    if (number < devicesCount) {
        return number;
    }
})();

var best_solution = {
    time: 0.0,
    result: [],
    result_cost: 1000000000000,
    node_id: -1
};


function run(){
    console.log("Start ... dev_id =" + deviceNumber );
    
    var start_time = new Date().getTime();
    var solution = find_best_solution(data, POPULATION_SIZE, NUMBER_OF_BEST_CREATURE_TO_SURVIVE, ITERATION_NUMBER);
    var solution_cost = get_all_join_cost(solution);
    var execution_time = (new Date().getTime()) - start_time;
    
    best_solution = {
        time: execution_time,
        result: solution,
        result_cost: solution_cost,
        node_id: deviceNumber
    };
     
   console.log("my_id=" +deviceNumber+ " left_id=" + leftChildNumber + " right_id="+ rightChildNumber + " root=" +  rootNumber + "end");    
    
    var remote_result_1, remote_result_2; 
    if( leftChildNumber !== undefined ){
        while( remote_result_1 === undefined ){
            remote_result_1 = Platform.receiveFromDevice(leftChildNumber);
        }
	remote_result_1 = JSON.parse(remote_result_1)
    }
    if( rightChildNumber !== undefined ){
        while(remote_result_2 === undefined ){
            remote_result_2 = Platform.receiveFromDevice(rightChildNumber);
        }
	remote_result_2 = JSON.parse(remote_result_2)
    }
    

    if(remote_result_1 !== undefined && remote_result_1.result_cost < best_solution.result_cost ){
        best_solution = remote_result_1;
    }
    if( remote_result_2 !== undefined && remote_result_2.result_cost < best_solution.result_cost ){
        best_solution = remote_result_2;
    }

        
    if( rootNumber !== undefined ){
        PlatformTools.sendTo(best_solution, rootNumber);
    }
    
    
    if( deviceNumber === 0 ){
         PlatformTools.sendDataToServer(
            "http://"+ DATA_SERVER_IP+ ":" + DATA_SERVER_PORT + "/measurements/" + Platform.getTaskId() + "/devices/" + deviceNumber, best_solution
        );
    }

    PlatformTools.finalize(deviceNumber);
}

run();