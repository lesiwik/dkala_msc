function get_join_relations_cost(relation_1_name, relation_2_name){
   
    var idx1 = parseInt(relation_1_name.substr(1)) - 1; /* for R1 -> 0, R10 -> 9*/
    var idx2 = parseInt(relation_2_name.substr(1)) - 1;
    
    var costs = [
            [9999,10,20,5,25,30,35,2,50,50,20,30,35,30,60],
            [10,9999,15,35,30,30,15,10,10,30,35,45,25,35,55],
            [20,15,9999,45,15,15,20,15,20,25,40,35,40,45,35],
            [5,35,45,9999,45,45,45,45,40,30,45,15,40,35,80],
            [25,30,15,45,9999,20,20,10,60,15,10,30,55,55,55],
            [30,30,15,45,20,9999,25,20,35,25,20,30,65,45,10],
            [35,15,20,45,20,25,9999,20,40,30,25,20,70,40,30],
            [2,10,15,45,10,20,20,9999,25,20,30,25,10,35,40],
            [50,10,20,40,60,35,40,25,9999,25,20,55,60,25,45],
            [50,30,25,30,15,25,30,20,25,9999,55,65,55,20,100],
            [20,35,40,45,10,20,25,30,20,55,9999,60,50,30,55],
            [30,45,35,15,30,30,20,25,55,65,60,9999,15,15,60],
            [35,25,40,40,55,65,70,10,60,55,50,15,9999,45,55],
            [30,35,45,35,55,45,40,35,25,20,30,15,45,9999,50],
            [60,55,35,80,55,10,30,40,45,100,55,60,55,50,9999]
    ];
    return costs[idx1][idx2];
}
function get_all_join_cost(ordered_relations_names){
    var cost_sum = 0;
    for (var i = 0; i < ordered_relations_names.length - 1; ++i) {
        var r1 = ordered_relations_names[i];
        var r2 = ordered_relations_names[i+1];
        cost_sum += get_join_relations_cost(r1, r2);
    }
    return cost_sum;
}

var relaction_names = ["R1","R2","R3","R4","R5","R6","R7","R8","R9", "R10", "R11", "R12", "R13"];

best_solution = {
    time: 0.0,
    result: [],
    result_cost: 100000000
};

function array_contains(a, obj) {
    var i = a.length;
    while (i--) {
       if (a[i] === obj) {
           return true;
       }
    }
    return false;
}

function brute(result){
    
    if( result.length == relaction_names.length ){
        var current_cost = get_all_join_cost(result)
        if ( best_solution.result_cost > current_cost ){
            best_solution.result = result;
            best_solution.result_cost = current_cost;
        }
        return;
    }

    
    for( var i = 0; i < relaction_names.length; i++ ){
        var local_result = result.slice(0);
        var r = relaction_names[i]; 
        if( !array_contains(local_result, r) ){
            local_result.push(r);
            brute( local_result );
        }
    }
    
}

function main(){
    var start_time = new Date().getTime();
    brute([]);
    best_solution.time = (new Date().getTime()) - start_time;
    console.log(best_solution);
}

main();