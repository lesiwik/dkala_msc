function HashedList(values) {
    var self = this, first, last, nodes = [];

    function Node(value, prev, next) {
        var self = this;

        self.value = value;
        self.prev = prev;
        self.next = next;
    }

    self.getFirstValue = function () {
        return first.value;
    };

    self.appendValue = function(value) {
        var node;

        node = new Node(value, last);
        if (!first) {
            first = node;
        }
        if (last) {
            last.next = node;
        }
        last = node;
        nodes[value] = node;
    };

    self.removeValue = function(value) {
        var node = nodes[value];

        if (node.prev) {
            node.prev.next = node.next;
        }
        if (node.next) {
            node.next.prev = node.prev;
        }
        if (first === node) {
            first = node.next;
        }
        if (last === node) {
            last = node.prev;
        }
    };

    self.getNextValue = function(value) {
        var node = nodes[value];
        if (node.next) {
            return node.next.value;
        } else {
            return null;
        }
    };

    if (values) {
        values.forEach(function(value) {
            self.appendValue(value);
        });
    }
}


function get_join_relations_cost(relation_1_name, relation_2_name){
    //in test version we have only 4 relation
    return { 
        "R1":{"R1":9999, "R2":10, "R3":20, "R4":5},
        "R2":{"R1":10, "R2":9999, "R3":15, "R4":35},
        "R3":{"R1":20, "R2":15, "R3":9999, "R4":45},  
        "R4":{"R1":5, "R2":35, "R3":45, "R4":9999}
    }[relation_1_name][relation_2_name];
}

function get_all_join_cost(ordered_relations_names){
    var cost_sum = 0;
    for (var i = 0; i < ordered_relations_names.length - 1; ++i) {
        var r1 = ordered_relations_names[i];
        var r2 = ordered_relations_names[i+1];
        cost_sum += get_join_relations_cost(r1, r2);
    }
    return cost_sum;
}

function get_creature_fitness(creature){
    return 1.0 / get_all_join_cost(creature);
}

function rand_int(up_to, different_then_number){
    
    function __rand_int(up_to){
        return Math.floor(Math.random()*up_to);
    }

    var rand = __rand_int(up_to);
    while(different_then_number == rand){
        rand = __rand_int(up_to);
    }
    return rand;
}

function get_random_of_two_elements(el_1, el_2){
    var a = [el_1, el_2];
    return a[rand_int(2)];
}

function permutate(unique_element_list){
    var permutation = [];
    var unique_element_list_cpy = unique_element_list.slice(0);
    while( unique_element_list_cpy.length > 0 ){
        var idx = rand_int(unique_element_list_cpy.length);    
        permutation.push(unique_element_list_cpy[idx]);
        unique_element_list_cpy.splice(idx, 1);
    }
    return permutation;
}

function leave_only_the_best_creatures(creatures, number_of_best_creature_to_survive){
    creatures.sort(function(creature_1, creature_2){
        return get_creature_fitness(creature_2) - get_creature_fitness(creature_1);
    });
    return creatures.slice(0, number_of_best_creature_to_survive);
}

function create_population(population_size, unique_relations_names){
    var population = []; // array of unique creatures
    var existance = {};
    for (var i = 0; i < population_size; i++) {
        var gen_permutation = permutate(unique_relations_names);
        while( existance[gen_permutation] !== undefined){
             gen_permutation = permutate(unique_relations_names);
        }
        existance[gen_permutation] = true;
        population.push(gen_permutation);
    }
    
    return population;
}

/**
 * mutates creature. Changes 5% gens places
 * @param  {[type]} creature - array of gens
 * @return {[type]}
 */
function mutate(creature){

    var mutation_count = creature.length * 0.05; // 5% changes
    if (mutation_count === 0) {
        mutation_count = 1;
    }

    var mutated_creature = creature.slice(0);
    for (var i = 0; i < mutation_count; i++) {
        var idx_1 = rand_int(creature.length);
        var idx_2 = rand_int(creature.length, idx_1);
        mutated_creature[idx_2] = creature[idx_1];
        mutated_creature[idx_1] = creature[idx_2];
    }
    
    return mutated_creature;
}

function crossover_scx(creature_1_gens, creature_2_gens, ordered_relations_names){
    var initial_relation_name = get_random_of_two_elements(creature_1_gens[0], creature_2_gens[0]);

    var nextX, nextY, nextValue,
        new_creature_gens = [initial_relation_name],
        hashedListX = new HashedList(creature_1_gens),
        hashedListY = new HashedList(creature_2_gens),
        orderedHashedList = new HashedList();
        
    ordered_relations_names.forEach(function(element, index){
        orderedHashedList.appendValue(element);
    });

    function getNextFreeValue(currentValue) {
        var value;

        value = orderedHashedList.getFirstValue();
        if (value == currentValue) {
            value = orderedHashedList.getNextValue(value);
        }

        return value;
    }

    var currentValue = initial_relation_name;

    while (new_creature_gens.length < creature_1_gens.length) {
        nextX = hashedListX.getNextValue(currentValue);
        if (nextX === null) {
            nextX = getNextFreeValue(currentValue);
        }
        nextY = hashedListY.getNextValue(currentValue);
        if (nextY === null) {
            nextY = getNextFreeValue(currentValue);
        }

        if (nextX == nextY) {
            nextValue = nextX;
        } else {
            var costX = get_join_relations_cost(currentValue, nextX);
            var costY = get_join_relations_cost(currentValue, nextY);

            if (costX < costY) {
                nextValue = nextX;
            } else {
                nextValue = nextY;
            }
        }

        orderedHashedList.removeValue(currentValue);
        hashedListX.removeValue(currentValue);
        hashedListY.removeValue(currentValue);
        currentValue = nextValue;
        new_creature_gens.push(currentValue);
    }
    return new_creature_gens;
}

function generaten_new_offspring(best_creatures, ordered_relations_names){
    var new_population = [];
    for (var index = 0; index < best_creatures.length - 2; index++) {
        
        var idx_1 = rand_int(index+1);
        var idx_2 = rand_int(index+1, idx_2);

        var creature_1_gens = best_creatures[index];
        var creature_2_gens = best_creatures[idx_1];
        var creature_3_gens = best_creatures[idx_2];

        var new_x = crossover_scx(creature_1_gens, creature_2_gens, ordered_relations_names);
        var new_y = crossover_scx(creature_1_gens, creature_3_gens, ordered_relations_names);

        new_x = mutate(new_x);
        new_y = mutate(new_y);

        new_population.push(new_x);
        new_population.push(new_y);
    }   
    return new_population;
}

function find_best_solution(ordered_relations_names, population_size, number_of_best_creature_to_survive, iteration_number){
    var initial_population = create_population(population_size, ordered_relations_names);
    var best_solution;
    var current_population = initial_population;
        
    for (var i = 0; i < iteration_number; i++) {
        var best_creatures = leave_only_the_best_creatures(current_population, number_of_best_creature_to_survive);
        var new_offspring = generaten_new_offspring(best_creatures, ordered_relations_names);
        current_population = current_population.concat(new_offspring);
    }

    return leave_only_the_best_creatures(current_population, 1)[0];
}

