var task = function() {
	var deviceNumber = Platform.getDeviceNumber();
	var devicesCount = Platform.getDevicesCount();

	var start = new Date().getTime();
	
	var x = 0, y = 0, r = 0.5;
	var results = [], inside = 0;
	var shots = 80000000; /* 200 mln */
	
	for (var i = 0 ; i < shots / devicesCount; i++) {
		x = Math.random() - 0.5;
		y = Math.random() - 0.5;
		if (Math.sqrt(x*x + y*y) <= r) {
			inside += 1;
		}
	}
	
	var onResultCompleted = function() {
		var end = new Date().getTime();
		
		results[0] = inside;
		var parts = 0;
		for (var i = 0; i < results.length; i++) {
			parts += results[i] / (shots / devicesCount);
		}
		pi = 4 * parts / results.length;
		
		
		PlatformTools.finalize({
			time: end - start,
			result: pi,
			debug: results,
		});
	};
	
	var createOnReceiveEvent = function (sender) {
		return function(hits) {				
			results[sender] = hits;
			var nextSender = sender + 1;
			if (nextSender < devicesCount) {
				PlatformTools.onReceive(nextSender, createOnReceiveEvent(nextSender));
			} else {
				onResultCompleted();
			}
		}
	};
	
	if (deviceNumber === 0) {
		if (devicesCount > 1) {
			PlatformTools.onReceive(1, createOnReceiveEvent(1));
		} else {
			onResultCompleted();
		}
	} else {
		PlatformTools.sendTo(inside, 0);
		PlatformTools.finalize("results are in task root device");
	}
};