import sys

with open("pi-fast-engine.js", "r") as engineFile:
	engine = engineFile.read()

with open("pi-fast.js", "w") as taskFile:
	taskFile.write(engine)
	taskFile.write("\n\ntask(")
	taskFile.write(sys.argv[1])
	taskFile.write(");\n")
