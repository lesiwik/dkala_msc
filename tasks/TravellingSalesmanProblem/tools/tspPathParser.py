import sys, json

source = sys.argv[1]
destination = sys.argv[2]

if len(sys.argv) == 4:
	variable = sys.argv[3]
else:
	variable = "data"

with open(source, "r") as sourceFile:
	sourceContent = sourceFile.readlines()
	
path = []
for line in sourceContent:
	elements = line.split()
	path.append(int(elements[0]) - 1)

with open(destination, "w") as destinationFile:
	destinationFile.write("var ")
	destinationFile.write(variable)
	destinationFile.write(" = ")
	destinationFile.write(json.dumps(path, indent = 4))
	destinationFile.write("; ")