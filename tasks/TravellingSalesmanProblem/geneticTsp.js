function GeneticTsp(engine, options, statistics) {
    var self = this,
        indexedArray = [];

    function getRandomNumber(range) {
        var randomNumber = Math.round(Math.random() * (range));
        return randomNumber;
    }
    
    function buildIndexedArray(length) {
        for (var index = 0; index < length; index++) {
            indexedArray[index] = index;
        }
    }

    function selectRandomly(population, count) {
        var randomlySelected = [], randomIndex, indexedArrayLength;

        buildIndexedArray(population.length);
        indexedArrayLength = population.length - 1;

        for (var index = 0; index < count; index++) {
            randomIndex = getRandomNumber(indexedArrayLength);
            randomlySelected.push(population[indexedArray[randomIndex]]);

            indexedArray[randomIndex] = indexedArray[indexedArrayLength];
            indexedArrayLength -= 1;
        }

        return randomlySelected;
    }

    function ensureFitness(chromosome, coordinates) {
        if (!chromosome.fitness) {
            chromosome.fitness = engine.calculateFitness(chromosome, coordinates);
        }
    }

    function sortByFitness(chromosomes, coordinates) {
        chromosomes.sort(function (X, Y) {
            ensureFitness(X, coordinates);
            ensureFitness(Y, coordinates);

            return X.fitness - Y.fitness;
        })
    };

    /* getTwoRandomNumbers(3,5) returns [3, 4] */
    function getTwoRandomNumbers(index, size) {
        var range, firstRandom, secondRandom;

        range = size - index;
        if (range == 2) {
            return [index, index + 1];
        }

        firstRandom = Math.floor(Math.random() * range) + index;
        secondRandom = firstRandom;
        while (secondRandom === firstRandom) {
            secondRandom = Math.floor(Math.random() * range) + index;
        }

        return [firstRandom, secondRandom];
    }

    function makeHashTable(X) {
        var hashTable = {};

        for (var index = 0; index < X.length - 1; index++) {
            hashTable[X[index] + '-' + X[index + 1]] = true;
        }

        return hashTable;
    }

    function countNonExisting(specimenChromosomes, allChromosomes) {
        var nonExistingCount = 0;

        Object.keys(specimenChromosomes).forEach(function (chromosome) {
            if (!allChromosomes[chromosome]) {
                nonExistingCount += 1;
            }
        });

        return nonExistingCount;
    }

    function extendChromosomes(allChromosomes, specimenChromosomes) {
        Object.keys(specimenChromosomes).forEach(function (chromosome) {
            allChromosomes[chromosome] = true;
        });
    }

    function makeCrossover(population, coordinates) {
        var index, newPopulation, afterCrossover, newX, newY, crossoverIndices,
            populationIndex, afterCrossoverIndex, randomPopulation,
            availableChromosomesCount, requiredNewChromosomes,
            populationChromosomes, specimenChromosomes;

        newPopulation = [];

        for (index = 0; index < population.length - 2; index++) {
            crossoverIndices = getTwoRandomNumbers(index + 1, population.length);
            newX = engine.makeSCX(population[index], population[crossoverIndices[0]], coordinates);
            newY = engine.makeSCX(population[index], population[crossoverIndices[1]], coordinates);

            newPopulation.push(newX);
            newPopulation.push(newY);
        }


        afterCrossover = [];
        afterCrossoverIndex = population.length - 1;
        Array.prototype.push.apply(population, newPopulation);
        sortByFitness(population, coordinates);
        populationIndex = population.length - 1;

        availableChromosomesCount = coordinates.length + coordinates.length * (coordinates.length - 3) / 2;
        requiredNewChromosomes = Math.ceil(availableChromosomesCount/ population.length);

        populationChromosomes = {};
        /* Fill population with new specimen, skipping repetitions. */
        while (populationIndex >= 0 && afterCrossoverIndex >= 0) {
            specimenChromosomes = makeHashTable(population[populationIndex]);
            if (countNonExisting(specimenChromosomes, populationChromosomes) < requiredNewChromosomes) {
                populationIndex -= 1;
                continue;
            }
            
            afterCrossover[afterCrossoverIndex] = population[populationIndex];

            extendChromosomes(populationChromosomes, specimenChromosomes);

            populationIndex -= 1;
            afterCrossoverIndex -= 1;
        }

        /* Fill population with new specimen when there were too many repetitions. */
        if (afterCrossoverIndex >= 0) {
            randomPopulation = engine.createPopulation(afterCrossoverIndex + 1, coordinates.length);

            while (afterCrossoverIndex >= 0) {
                afterCrossover[afterCrossoverIndex] = randomPopulation[afterCrossoverIndex];
                afterCrossoverIndex -= 1;
            }
        }

        return afterCrossover;
    }

    function makeMutation(population, coordinates, mutationElementsCount) {
        var chromosomesToMutate;

        chromosomesToMutate = selectRandomly(population, mutationElementsCount);
        chromosomesToMutate.forEach(function (chromosome) {
            engine.mutate(chromosome, coordinates);
            chromosome.fitness = engine.calculateFitness(chromosome, coordinates);
        });
    }

    self.setMaxIterationCount = function (maxIterationCount) {
        options.maxIterationCount = maxIterationCount;
    };

    self.getCurrentIterationNumber = function() {
        return self.iterationNumber;
    };

    self.solve = function(coordinates, satisfyingPathLength, onCalculationsCompleted) {
        var solution, population,
            bestPath, bestPathLength, nextBestPath, nextBestPathLength, mutationElementsCount;
        
        self.iterationNumber = 0;
        population = engine.createPopulation(options.populationSize, coordinates.length);
        mutationElementsCount = Math.floor(options.mutationChance * population.length);

        statistics.saveTaskInformation(coordinates, options.maxIterationCount);

        sortByFitness(population, coordinates);

        function iterationLogic() {
            population = makeCrossover(population, coordinates);
            makeMutation(population, coordinates, mutationElementsCount);
            sortByFitness(population, coordinates);

            nextBestPath = population[population.length - 1];
            nextBestPathLength = Math.round(1 / nextBestPath.fitness);
            self.iterationNumber += 1;

            statistics.setBestPath(self.iterationNumber, nextBestPath);
            statistics.setBestPathLength(self.iterationNumber, nextBestPathLength);
            statistics.saveIterationDetails(self.iterationNumber, population);

            if (!bestPathLength || nextBestPathLength < bestPathLength) {
                bestPathLength = nextBestPathLength;
                bestPath = nextBestPath;
            }
            
            console.log("Currently best found path equals " + bestPathLength + " iteration number equals " + self.iterationNumber + " max iteration count equals " + options.maxIterationCount + " !");
            if (!(bestPath && bestPathLength <= satisfyingPathLength) && self.iterationNumber < options.maxIterationCount) {
                setTimeout(iterationLogic);
            } else {
                solution = {
                    path: bestPath,
                    pathLength: bestPathLength,
                    iterationNumber: self.iterationNumber
                };
                console.log("Attempting to finalize calculation with " + JSON.stringify(solution));
                onCalculationsCompleted(solution);
            }
        }

        if (!(bestPath && bestPathLength <= satisfyingPathLength) && self.iterationNumber < options.maxIterationCount) {
            setTimeout(iterationLogic);
        };
    };
}