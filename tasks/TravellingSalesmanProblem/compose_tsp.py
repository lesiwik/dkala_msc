import sys, json

if len(sys.argv) != 7:
    print "python compose_tsp.py [dataCenterIp] [dataCenterPort] [populationSize] [maxIterationCount] [dataSource.tsp] [satisfyingPathLength]"
    raise SystemExit

with open("Statistics.js", "r") as statisticsFile:
	statistics = statisticsFile.read()

with open("tools.js", "r") as toolsFile:
	tools = toolsFile.read()

with open("engine.js", "r") as engineFile:
	engine = engineFile.read()

with open("geneticTsp.js", "r") as geneticTspFile:
	geneticTsp = geneticTspFile.read()

logic =  'var statistics = new Statistics("' + sys.argv[1] + '", "' + sys.argv[2] + '");\n'
logic += 'var tools = new Tools();\n'
logic += 'var engine = new Engine(tools);\n'
logic += 'var geneticTsp = new GeneticTsp(engine, {\n'
logic += '    populationSize: ' + sys.argv[3] + ',\n'
logic += '    mutationChance: 0.2,\n'
logic += '    maxIterationCount: ' + sys.argv[4] + '\n'
logic += '}, statistics);\n\n'

dataSource = sys.argv[5]

with open(dataSource, "r") as sourceFile:
	sourceContent = sourceFile.readlines()
	
path = []
for line in sourceContent:
	elements = line.split()
	path.append([float(elements[1]), float(elements[2])])

data = "var data = " + json.dumps(path, indent = 4) + ";\n"

execution =  'var devicesCount = Platform.getDevicesCount();\n'
execution += 'var deviceNumber = Platform.getDeviceNumber();\n'
execution += 'if (deviceNumber === 0) {\n'
execution += '    var taskInitialization = {\n'
execution += '        taskId: Platform.getTaskId(),\n'
execution += '        devicesCount: devicesCount\n'
execution += '    };\n'
execution += '    PlatformTools.sendDataToServer(\n'
execution += '        "http://' + sys.argv[1] + ':' + sys.argv[2] + '/measurements", taskInitialization\n'
execution += '    );\n'
execution += '}\n'
execution += 'var rootNumber = (function getRootNumber() {\n'
execution += '    if (deviceNumber === 0) return;\n'
execution += '    return Math.floor((deviceNumber - 1) / 2);\n'
execution += '})();\n'
execution += 'var leftChildNumber = (function getLeftChildNumber() {\n'
execution += '    var number = deviceNumber * 2 + 1;\n'
execution += '    if (number < devicesCount) {\n'
execution += '        return number;\n'
execution += '    }\n'
execution += '})();\n'
execution += 'var rightChildNumber = (function getRightChildNumber() {\n'
execution += '    var number = deviceNumber * 2 + 2;\n'
execution += '    if (number < devicesCount) {\n'
execution += '        return number;\n'
execution += '    }\n'
execution += '})();\n'
execution += 'var bestSolution = {}, taskUpdate;\n'
execution += 'var bestKnownExecution = {\n'
execution += '    executionsCount: 0,\n'
execution += '    iterationNumber: 0\n'
execution += '};\n'
execution += 'var executionsCountHolder = {\n'
execution += '    executionsCount: 0,\n'
execution += '};\n'
execution += 'var bestLocalExecution;\n'
execution += 'var startTime = new Date().getTime();\n'
execution += '\n'
execution += 'function onReceiveCallback(remoteFinalExecution) {\n'
execution += '    if ((!bestKnownExecution.executionsCount) || remoteFinalExecution.executionsCount < bestKnownExecution.executionsCount || \n'
execution += '        (remoteFinalExecution.executionsCount === bestKnownExecution.executionsCount && remoteFinalExecution.iterationNumber < bestKnownExecution.iterationNumber)) {\n'
execution += '        if (executionsCountHolder.executionsCount === remoteFinalExecution.executionsCount) { \n'
execution += '            geneticTsp.setMaxIterationCount(remoteFinalExecution.iterationNumber);\n'
execution += '        }\n'
execution += '        bestKnownExecution.executionsCount = remoteFinalExecution.executionsCount;\n'
execution += '        bestKnownExecution.iterationNumber = remoteFinalExecution.iterationNumber;\n'
execution += '        bestKnownExecution.timeTaken = remoteFinalExecution.timeTaken;\n'
execution += '        bestKnownExecution.deviceNumber = remoteFinalExecution.deviceNumber;\n'
execution += '    }\n'
execution += '}\n'
execution += 'if (rootNumber || rootNumber === 0) {\n'
execution += '    PlatformTools.onReceive(rootNumber, onReceiveCallback);\n'
execution += '}\n'
execution += 'if (leftChildNumber) {\n'
execution += '    PlatformTools.onReceive(leftChildNumber, onReceiveCallback);\n'
execution += '}\n'
execution += 'if (rightChildNumber) {\n'
execution += '    PlatformTools.onReceive(rightChildNumber, onReceiveCallback);\n'
execution += '}\n'
execution += 'function finalize() {\n'
execution += '    var execution;\n'
execution += '    if ((!bestLocalExecution) || (bestKnownExecution.executionsCount &&\n'
execution += '        (bestKnownExecution.executionsCount < bestLocalExecution.executionsCount ||\n'
execution += '        (bestKnownExecution.executionsCount == bestLocalExecution.executionsCount && \n'
execution += '         bestKnownExecution.iterationNumber <= bestLocalExecution.iterationNumber)))) {\n'
execution += '        console.log("Found better or equal remote execution! " + JSON.stringify(bestKnownExecution));\n'
execution += '        execution = bestKnownExecution;\n'
execution += '    } else {\n'
execution += '        execution = bestLocalExecution;\n'
execution += '    }\n'
execution += '    if (rootNumber || rootNumber === 0) {\n'
execution += '        PlatformTools.sendTo(execution, rootNumber);\n'
execution += '    }\n'
execution += '    if (leftChildNumber) {\n'
execution += '        PlatformTools.sendTo(execution, leftChildNumber);\n'
execution += '    }\n'
execution += '    if (rightChildNumber) {\n'
execution += '        PlatformTools.sendTo(execution, rightChildNumber);\n'
execution += '    }\n'
execution += '    execution.bestSolution = bestSolution;\n'
execution += '    PlatformTools.finalize(execution);\n'
execution += '}\n'
execution += '\n'
execution += 'function onCalculationsCompleted(solution) {\n'
execution += '    bestSolution = solution;\n'
execution += '    if (solution.pathLength <= ' + sys.argv[6] + ') {\n'
execution += '        taskUpdate = {\n'
execution += '            executionsCount: executionsCountHolder.executionsCount,\n'
execution += '            iterationNumber: solution.iterationNumber\n'
execution += '        };\n'
execution += '        PlatformTools.sendDataToServer(\n'
execution += '            "http://' + sys.argv[1] + ':' + sys.argv[2] + '/measurements/" + Platform.getTaskId() + "/devices/" + deviceNumber, taskUpdate\n'
execution += '        );\n'
execution += '        bestLocalExecution = {\n'
execution += '            executionsCount: executionsCountHolder.executionsCount,\n'
execution += '            iterationNumber: bestSolution.iterationNumber,\n'
execution += '            timeTaken: (new Date().getTime()) - startTime,\n'
execution += '            deviceNumber: deviceNumber\n'
execution += '        };\n'
execution += '        console.log("Found satisfying solution - " + JSON.stringify(bestLocalExecution));\n'
execution += '        finalize();\n'
execution += '        return;\n'
execution += '    }\n'
execution += '    setTimeout(loopEntry);\n'
execution += '}\n'
execution += '\n'
execution += 'function loopEntry () {\n'
execution += '    executionsCountHolder.executionsCount += 1;\n'
execution += '    console.log("Starting new loopEntry " + JSON.stringify(bestKnownExecution));\n'
execution += '    if (bestKnownExecution.executionsCount && bestKnownExecution.executionsCount < executionsCountHolder.executionsCount) {\n'
execution += '        finalize();\n'
execution += '        return;\n'
execution += '    }\n'
execution += '    if (bestKnownExecution.executionsCount && bestKnownExecution.executionsCount === executionsCountHolder.executionsCount) {\n'
execution += '        geneticTsp.setMaxIterationCount(bestKnownExecution.iterationNumber);\n'
execution += '    }\n'
execution += '    geneticTsp.solve(data, ' + sys.argv[6] + ', onCalculationsCompleted);\n'
execution += '}\n'
execution += 'setTimeout(loopEntry);\n'

with open("tsp.js", "w") as taskFile:
    taskFile.write(statistics)
    taskFile.write(tools)
    taskFile.write(engine)
    taskFile.write(geneticTsp)
    taskFile.write(logic)
    taskFile.write(data)
    taskFile.write(execution)
