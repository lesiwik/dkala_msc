function Statistics(serverAddress) {
    var self = this;

    function getServerAddress() {
        return serverAddress;
    }

    self.setBestPath = function (iteration, bestPath) {
        
    };

    self.setBestPathLength = function (iteration, bestPathLength) {
        
    };

    self.saveIterationDetails = function (iteration, population) {
        var details, serverAddress = getServerAddress();

        details = {
            iteration: iteration,
            population: population,
            distances: []
        };

        population.forEach(function (chromosome, index) {
            if (chromosome.fitness) {
                details.distances[index] = 1 / chromosome.fitness;
            }
        });

        PlatformTools.sendDataToServer(
            serverAddress + "/calculation/" + Platform.getDeviceNumber() + "/values",
            details
        );
    };

    self.saveTaskInformation = function (coordinates, iterationCount) {
        PlatformTools.sendDataToServer(
            getServerAddress() + "/calculation/" + Platform.getDeviceNumber(), {
                coordinates: coordinates,
                iterationCount: iterationCount
            }
        );
    };
}
function Tools() {
    var self = this;


    function deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    function getDistance(a, b) {
        var x, y, distance;
        x = a[0] - b[0];
        y = a[1] - b[1];
        distance = Math.round(Math.sqrt(x * x + y * y));

        return distance;
    }

    self.getDistance = function (a, b) {
        var distance = getDistance(a, b);
        return distance;
    };

    self.getPathLength = function (path, coordinates) {
        var distance, step, fromNode, toNode;

        distance = 0;
        for (var nodeIndex = 0; nodeIndex < path.length - 1; nodeIndex++) {
            fromNode = path[nodeIndex];
            toNode = path[nodeIndex + 1];
            step = getDistance(coordinates[fromNode], coordinates[toNode]);

            distance += step
        }
        fromNode = path[path.length - 1];
        toNode = path[0];
        step = getDistance(coordinates[fromNode], coordinates[toNode]);

        distance += step;
        return distance;
    };
}function Engine(tools) {
    var self = this;

    function HashedList(values) {
        var self = this, first, last, nodes = [];

        function Node(value, prev, next) {
            var self = this;

            self.value = value;
            self.prev = prev;
            self.next = next;
        }

        self.getFirstValue = function () {
            return first.value;
        };

        self.appendValue = function(value) {
            var node;

            node = new Node(value, last);
            if (!first) {
                first = node;
            }
            if (last) {
                last.next = node;
            }
            last = node;
            nodes[value] = node;
        };

        self.removeValue = function(value) {
            var node = nodes[value];

            if (node.prev) {
                node.prev.next = node.next;
            }
            if (node.next) {
                node.next.prev = node.prev;
            }
            if (first === node) {
                first = node.next;
            }
            if (last === node) {
                last = node.prev;
            }
        };

        self.getNextValue = function(value) {
            var node = nodes[value];
            if (node.next) {
                return node.next.value;
            } else {
                return null;
            }
        };

        if (values) {
            values.forEach(function(value) {
                self.appendValue(value);
            });
        }
    }

    function getRandomNumber(range) {
        var randomNumber = Math.round(Math.random() * (range));
        return randomNumber;
    }

    self.mutate = function (X, coordinates) {
        var a, b, c, d, 
            ab, bc, cd,
            ac, cb, bd,
            temp;

        for (var i = 0; i < X.length - 3; i++) {
            a = X[i];
            b = X[i + 1];
            c = X[i + 2];
            d = X[i + 3];

            ab = tools.getDistance(coordinates[a], coordinates[b]);
            bc = tools.getDistance(coordinates[b], coordinates[c]);
            cd = tools.getDistance(coordinates[c], coordinates[d]);

            ac = tools.getDistance(coordinates[a], coordinates[c]);
            cb = tools.getDistance(coordinates[c], coordinates[b]);
            bd = tools.getDistance(coordinates[b], coordinates[d]);

            if (ab + bc + cd > ac + cb + bd) {
                temp = X[i + 1];
                X[i + 1] = X[i + 2];
                X[i + 2] = temp;
            }
        }
    };

    self.makeSCX = function (X, Y, coordinates) {
        var nextX, nextY, nextValue,
            costX, costY, Z = [0],
            hashedListX = new HashedList(X),
            hashedListY = new HashedList(Y),
            orderedHashedList = new HashedList();
            
        for (var value = 0; value < X.length; value++) {
            orderedHashedList.appendValue(value);
        }

        function getNextFreeValue(currentValue) {
            var value;

            value = orderedHashedList.getFirstValue();
            if (value == currentValue) {
                value = orderedHashedList.getNextValue(value);
            }

            return value;
        }

        currentValue = 0;
        while (Z.length < X.length) {
            nextX = hashedListX.getNextValue(currentValue);
            if (nextX === null) {
                nextX = getNextFreeValue(currentValue);
            }
            nextY = hashedListY.getNextValue(currentValue);
            if (nextY === null) {
                nextY = getNextFreeValue(currentValue);
            }

            if (nextX == nextY) {
                nextValue = nextX;
            } else {
                costX = tools.getDistance(coordinates[currentValue], coordinates[nextX]);
                costY = tools.getDistance(coordinates[currentValue], coordinates[nextY]);

                if (costX < costY) {
                    nextValue = nextX;
                } else {
                    nextValue = nextY;
                }
            }

            orderedHashedList.removeValue(currentValue);
            hashedListX.removeValue(currentValue);
            hashedListY.removeValue(currentValue);
            currentValue = nextValue;
            Z.push(currentValue);
        }
        return Z;
    };

    self.createPopulation = function (populationSize, chromosomeLength) {
        var population = [], chromosome, availableNodes = [],
            chromosomeIndex, pathIndex, randomIndex, randomElement;


        for (chromosomeIndex = 0; chromosomeIndex < populationSize; chromosomeIndex++) {
            for (var index = 0; index < chromosomeLength - 1; index++) {
                availableNodes[index] = index + 1;
            }
            chromosome = [0];

            for (pathIndex = 1; pathIndex < chromosomeLength; pathIndex++) {
                randomIndex = getRandomNumber(chromosomeLength - pathIndex - 1);
                randomElement = availableNodes[randomIndex];
                availableNodes[randomIndex] = availableNodes[chromosomeLength - pathIndex - 1];

                chromosome.push(randomElement);
            }
            population.push(chromosome);
        }

        return population;
    };

    self.calculateFitness = function (chromosome, coordinates) {
        var fitness, distance;

        distance = tools.getPathLength(chromosome, coordinates);
        fitness = 1 / distance;

        return fitness;
    };
}function GeneticTsp(engine, options, statistics) {
    var self = this,
        indexedArray = [];

    function getRandomNumber(range) {
        var randomNumber = Math.round(Math.random() * (range));
        return randomNumber;
    }
    
    function buildIndexedArray(length) {
        for (var index = 0; index < length; index++) {
            indexedArray[index] = index;
        }
    }

    function selectRandomly(population, count) {
        var randomlySelected = [], randomIndex, indexedArrayLength;

        buildIndexedArray(population.length);
        indexedArrayLength = population.length - 1;

        for (var index = 0; index < count; index++) {
            randomIndex = getRandomNumber(indexedArrayLength);
            randomlySelected.push(population[indexedArray[randomIndex]]);

            indexedArray[randomIndex] = indexedArray[indexedArrayLength];
            indexedArrayLength -= 1;
        }

        return randomlySelected;
    }

    function ensureFitness(chromosome, coordinates) {
        if (!chromosome.fitness) {
            chromosome.fitness = engine.calculateFitness(chromosome, coordinates);
        }
    }

    function sortByFitness(chromosomes, coordinates) {
        chromosomes.sort(function (X, Y) {
            ensureFitness(X, coordinates);
            ensureFitness(Y, coordinates);

            return X.fitness - Y.fitness;
        })
    };

    /* getTwoRandomNumbers(3,5) returns [3, 4] */
    function getTwoRandomNumbers(index, size) {
        var range, firstRandom, secondRandom;

        range = size - index;
        if (range == 2) {
            return [index, index + 1];
        }

        firstRandom = Math.floor(Math.random() * range) + index;
        secondRandom = firstRandom;
        while (secondRandom === firstRandom) {
            secondRandom = Math.floor(Math.random() * range) + index;
        }

        return [firstRandom, secondRandom];
    }

    function makeHashTable(X) {
        var hashTable = {};

        for (var index = 0; index < X.length - 1; index++) {
            hashTable[X[index] + '-' + X[index + 1]] = true;
        }

        return hashTable;
    }

    function countNonExisting(specimenChromosomes, allChromosomes) {
        var nonExistingCount = 0;

        Object.keys(specimenChromosomes).forEach(function (chromosome) {
            if (!allChromosomes[chromosome]) {
                nonExistingCount += 1;
            }
        });

        return nonExistingCount;
    }

    function extendChromosomes(allChromosomes, specimenChromosomes) {
        Object.keys(specimenChromosomes).forEach(function (chromosome) {
            allChromosomes[chromosome] = true;
        });
    }

    function makeCrossover(population, coordinates) {
        var index, newPopulation, afterCrossover, newX, newY, crossoverIndices,
            populationIndex, afterCrossoverIndex, randomPopulation,
            availableChromosomesCount, requiredNewChromosomes,
            populationChromosomes, specimenChromosomes;

        newPopulation = [];

        for (index = 0; index < population.length - 2; index++) {
            crossoverIndices = getTwoRandomNumbers(index + 1, population.length);
            newX = engine.makeSCX(population[index], population[crossoverIndices[0]], coordinates);
            newY = engine.makeSCX(population[index], population[crossoverIndices[1]], coordinates);

            newPopulation.push(newX);
            newPopulation.push(newY);
        }


        afterCrossover = [];
        afterCrossoverIndex = population.length - 1;
        Array.prototype.push.apply(population, newPopulation);
        sortByFitness(population, coordinates);
        populationIndex = population.length - 1;

        availableChromosomesCount = coordinates.length + coordinates.length * (coordinates.length - 3) / 2;
        requiredNewChromosomes = Math.ceil(availableChromosomesCount/ population.length);

        populationChromosomes = {};
        /* Fill population with new specimen, skipping repetitions. */
        while (populationIndex >= 0 && afterCrossoverIndex >= 0) {
            specimenChromosomes = makeHashTable(population[populationIndex]);
            if (countNonExisting(specimenChromosomes, populationChromosomes) < requiredNewChromosomes) {
                populationIndex -= 1;
                continue;
            }
            
            afterCrossover[afterCrossoverIndex] = population[populationIndex];

            extendChromosomes(populationChromosomes, specimenChromosomes);

            populationIndex -= 1;
            afterCrossoverIndex -= 1;
        }

        /* Fill population with new specimen when there were too many repetitions. */
        if (afterCrossoverIndex >= 0) {
            randomPopulation = engine.createPopulation(afterCrossoverIndex + 1, coordinates.length);

            while (afterCrossoverIndex >= 0) {
                afterCrossover[afterCrossoverIndex] = randomPopulation[afterCrossoverIndex];
                afterCrossoverIndex -= 1;
            }
        }

        return afterCrossover;
    }

    function makeMutation(population, coordinates, mutationElementsCount) {
        var chromosomesToMutate;

        chromosomesToMutate = selectRandomly(population, mutationElementsCount);
        chromosomesToMutate.forEach(function (chromosome) {
            engine.mutate(chromosome, coordinates);
            chromosome.fitness = engine.calculateFitness(chromosome, coordinates);
        });
    }

    self.setMaxIterationCount = function (maxIterationCount) {
        options.maxIterationCount = maxIterationCount;
    };

    self.getCurrentIterationNumber = function() {
        return self.iterationNumber;
    };

    self.solve = function(coordinates, satisfyingPathLength, onCalculationsCompleted) {
        var solution, population,
            bestPath, bestPathLength, nextBestPath, nextBestPathLength, mutationElementsCount;
        
        self.iterationNumber = 0;
        population = engine.createPopulation(options.populationSize, coordinates.length);
        mutationElementsCount = Math.floor(options.mutationChance * population.length);

        statistics.saveTaskInformation(coordinates, options.maxIterationCount);

        sortByFitness(population, coordinates);

        function iterationLogic() {
            population = makeCrossover(population, coordinates);
            makeMutation(population, coordinates, mutationElementsCount);
            sortByFitness(population, coordinates);

            nextBestPath = population[population.length - 1];
            nextBestPathLength = Math.round(1 / nextBestPath.fitness);
            self.iterationNumber += 1;

            statistics.setBestPath(self.iterationNumber, nextBestPath);
            statistics.setBestPathLength(self.iterationNumber, nextBestPathLength);
            statistics.saveIterationDetails(self.iterationNumber, population);

            if (!bestPathLength || nextBestPathLength < bestPathLength) {
                bestPathLength = nextBestPathLength;
                bestPath = nextBestPath;
            }
            
            console.log("Currently best found path equals " + bestPathLength + " iteration number equals " + self.iterationNumber + " max iteration count equals " + options.maxIterationCount + " !");
            if (!(bestPath && bestPathLength <= satisfyingPathLength) && self.iterationNumber < options.maxIterationCount) {
                setTimeout(iterationLogic);
            } else {
                solution = {
                    path: bestPath,
                    pathLength: bestPathLength,
                    iterationNumber: self.iterationNumber
                };
                console.log("Attempting to finalize calculation with " + JSON.stringify(solution));
                onCalculationsCompleted(solution);
            }
        }

        if (!(bestPath && bestPathLength <= satisfyingPathLength) && self.iterationNumber < options.maxIterationCount) {
            setTimeout(iterationLogic);
        };
    };
}
var statistics = new Statistics('https://damp-crag-45464.herokuapp.com');
var tools = new Tools();
var engine = new Engine(tools);
var geneticTsp = new GeneticTsp(engine, {
    populationSize: 500,
    mutationChance: 0.2,
    maxIterationCount: 1000
}, statistics);

var data = [
    [
        0.0, 
        13.0
    ], 
    [
        0.0, 
        26.0
    ], 
    [
        0.0, 
        27.0
    ], 
    [
        0.0, 
        39.0
    ], 
    [
        2.0, 
        0.0
    ], 
    [
        5.0, 
        13.0
    ], 
    [
        5.0, 
        19.0
    ], 
    [
        5.0, 
        25.0
    ], 
    [
        5.0, 
        31.0
    ], 
    [
        5.0, 
        37.0
    ], 
    [
        5.0, 
        43.0
    ], 
    [
        5.0, 
        8.0
    ], 
    [
        8.0, 
        0.0
    ], 
    [
        9.0, 
        10.0
    ], 
    [
        10.0, 
        10.0
    ], 
    [
        11.0, 
        10.0
    ], 
    [
        12.0, 
        10.0
    ], 
    [
        12.0, 
        5.0
    ], 
    [
        15.0, 
        13.0
    ], 
    [
        15.0, 
        19.0
    ], 
    [
        15.0, 
        25.0
    ], 
    [
        15.0, 
        31.0
    ], 
    [
        15.0, 
        37.0
    ], 
    [
        15.0, 
        43.0
    ], 
    [
        15.0, 
        8.0
    ], 
    [
        18.0, 
        11.0
    ], 
    [
        18.0, 
        13.0
    ], 
    [
        18.0, 
        15.0
    ], 
    [
        18.0, 
        17.0
    ], 
    [
        18.0, 
        19.0
    ], 
    [
        18.0, 
        21.0
    ], 
    [
        18.0, 
        23.0
    ], 
    [
        18.0, 
        25.0
    ], 
    [
        18.0, 
        27.0
    ], 
    [
        18.0, 
        29.0
    ], 
    [
        18.0, 
        31.0
    ], 
    [
        18.0, 
        33.0
    ], 
    [
        18.0, 
        35.0
    ], 
    [
        18.0, 
        37.0
    ], 
    [
        18.0, 
        39.0
    ], 
    [
        18.0, 
        41.0
    ], 
    [
        18.0, 
        42.0
    ], 
    [
        18.0, 
        44.0
    ], 
    [
        18.0, 
        45.0
    ], 
    [
        25.0, 
        11.0
    ], 
    [
        25.0, 
        15.0
    ], 
    [
        25.0, 
        22.0
    ], 
    [
        25.0, 
        23.0
    ], 
    [
        25.0, 
        24.0
    ], 
    [
        25.0, 
        26.0
    ], 
    [
        25.0, 
        28.0
    ], 
    [
        25.0, 
        29.0
    ], 
    [
        25.0, 
        9.0
    ], 
    [
        28.0, 
        16.0
    ], 
    [
        28.0, 
        20.0
    ], 
    [
        28.0, 
        28.0
    ], 
    [
        28.0, 
        30.0
    ], 
    [
        28.0, 
        34.0
    ], 
    [
        28.0, 
        40.0
    ], 
    [
        28.0, 
        43.0
    ], 
    [
        28.0, 
        47.0
    ], 
    [
        32.0, 
        26.0
    ], 
    [
        32.0, 
        31.0
    ], 
    [
        33.0, 
        15.0
    ], 
    [
        33.0, 
        26.0
    ], 
    [
        33.0, 
        29.0
    ], 
    [
        33.0, 
        31.0
    ], 
    [
        34.0, 
        15.0
    ], 
    [
        34.0, 
        26.0
    ], 
    [
        34.0, 
        29.0
    ], 
    [
        34.0, 
        31.0
    ], 
    [
        34.0, 
        38.0
    ], 
    [
        34.0, 
        41.0
    ], 
    [
        34.0, 
        5.0
    ], 
    [
        35.0, 
        17.0
    ], 
    [
        35.0, 
        31.0
    ], 
    [
        38.0, 
        16.0
    ], 
    [
        38.0, 
        20.0
    ], 
    [
        38.0, 
        30.0
    ], 
    [
        38.0, 
        34.0
    ], 
    [
        40.0, 
        22.0
    ], 
    [
        41.0, 
        23.0
    ], 
    [
        41.0, 
        32.0
    ], 
    [
        41.0, 
        34.0
    ], 
    [
        41.0, 
        35.0
    ], 
    [
        41.0, 
        36.0
    ], 
    [
        48.0, 
        22.0
    ], 
    [
        48.0, 
        27.0
    ], 
    [
        48.0, 
        6.0
    ], 
    [
        51.0, 
        45.0
    ], 
    [
        51.0, 
        47.0
    ], 
    [
        56.0, 
        25.0
    ], 
    [
        57.0, 
        12.0
    ], 
    [
        57.0, 
        25.0
    ], 
    [
        57.0, 
        44.0
    ], 
    [
        61.0, 
        45.0
    ], 
    [
        61.0, 
        47.0
    ], 
    [
        63.0, 
        6.0
    ], 
    [
        64.0, 
        22.0
    ], 
    [
        71.0, 
        11.0
    ], 
    [
        71.0, 
        13.0
    ], 
    [
        71.0, 
        16.0
    ], 
    [
        71.0, 
        45.0
    ], 
    [
        71.0, 
        47.0
    ], 
    [
        74.0, 
        12.0
    ], 
    [
        74.0, 
        16.0
    ], 
    [
        74.0, 
        20.0
    ], 
    [
        74.0, 
        24.0
    ], 
    [
        74.0, 
        29.0
    ], 
    [
        74.0, 
        35.0
    ], 
    [
        74.0, 
        39.0
    ], 
    [
        74.0, 
        6.0
    ], 
    [
        77.0, 
        21.0
    ], 
    [
        78.0, 
        10.0
    ], 
    [
        78.0, 
        32.0
    ], 
    [
        78.0, 
        35.0
    ], 
    [
        78.0, 
        39.0
    ], 
    [
        79.0, 
        10.0
    ], 
    [
        79.0, 
        33.0
    ], 
    [
        79.0, 
        37.0
    ], 
    [
        80.0, 
        10.0
    ], 
    [
        80.0, 
        41.0
    ], 
    [
        80.0, 
        5.0
    ], 
    [
        81.0, 
        17.0
    ], 
    [
        84.0, 
        20.0
    ], 
    [
        84.0, 
        24.0
    ], 
    [
        84.0, 
        29.0
    ], 
    [
        84.0, 
        34.0
    ], 
    [
        84.0, 
        38.0
    ], 
    [
        84.0, 
        6.0
    ], 
    [
        107.0, 
        27.0
    ]
];
var devicesCount = Platform.getDevicesCount();
var deviceNumber = Platform.getDeviceNumber();
if (deviceNumber === 0) {
    var taskInitialization = {
        taskId: Platform.getTaskId(),
        devicesCount: devicesCount
    };
}
var rootNumber = (function getRootNumber() {
    if (deviceNumber === 0) return;
    return Math.floor((deviceNumber - 1) / 2);
})();
var leftChildNumber = (function getLeftChildNumber() {
    var number = deviceNumber * 2 + 1;
    if (number < devicesCount) {
        return number;
    }
})();
var rightChildNumber = (function getRightChildNumber() {
    var number = deviceNumber * 2 + 2;
    if (number < devicesCount) {
        return number;
    }
})();
var bestSolution = {}, taskUpdate;
var bestKnownExecution = {
    executionsCount: 0,
    iterationNumber: 0
};
var executionsCountHolder = {
    executionsCount: 0,
};
var bestLocalExecution;
var startTime = new Date().getTime();

function onReceiveCallback(remoteFinalExecution) {
    if ((!bestKnownExecution.executionsCount) || remoteFinalExecution.executionsCount < bestKnownExecution.executionsCount || 
        (remoteFinalExecution.executionsCount === bestKnownExecution.executionsCount && remoteFinalExecution.iterationNumber < bestKnownExecution.iterationNumber)) {
        if (executionsCountHolder.executionsCount === remoteFinalExecution.executionsCount) { 
            geneticTsp.setMaxIterationCount(remoteFinalExecution.iterationNumber);
        }
        bestKnownExecution.executionsCount = remoteFinalExecution.executionsCount;
        bestKnownExecution.iterationNumber = remoteFinalExecution.iterationNumber;
        bestKnownExecution.timeTaken = remoteFinalExecution.timeTaken;
        bestKnownExecution.deviceNumber = remoteFinalExecution.deviceNumber;
    }
}
if (rootNumber || rootNumber === 0) {
    PlatformTools.onReceive(rootNumber, onReceiveCallback);
}
if (leftChildNumber) {
    PlatformTools.onReceive(leftChildNumber, onReceiveCallback);
}
if (rightChildNumber) {
    PlatformTools.onReceive(rightChildNumber, onReceiveCallback);
}
function finalize() {
    var execution;
    if ((!bestLocalExecution) || (bestKnownExecution.executionsCount &&
        (bestKnownExecution.executionsCount < bestLocalExecution.executionsCount ||
        (bestKnownExecution.executionsCount == bestLocalExecution.executionsCount && 
         bestKnownExecution.iterationNumber <= bestLocalExecution.iterationNumber)))) {
        console.log("Found better or equal remote execution! " + JSON.stringify(bestKnownExecution));
        execution = bestKnownExecution;
    } else {
        execution = bestLocalExecution;
    }
    if (rootNumber || rootNumber === 0) {
        PlatformTools.sendTo(execution, rootNumber);
    }
    if (leftChildNumber) {
        PlatformTools.sendTo(execution, leftChildNumber);
    }
    if (rightChildNumber) {
        PlatformTools.sendTo(execution, rightChildNumber);
    }
    execution.bestSolution = bestSolution;
    PlatformTools.finalize(execution);
}

function onCalculationsCompleted(solution) {
    bestSolution = solution;
    if (solution.pathLength <= 620) {
        taskUpdate = {
            executionsCount: executionsCountHolder.executionsCount,
            iterationNumber: solution.iterationNumber
        };
        bestLocalExecution = {
            executionsCount: executionsCountHolder.executionsCount,
            iterationNumber: bestSolution.iterationNumber,
            timeTaken: (new Date().getTime()) - startTime,
            deviceNumber: deviceNumber
        };
        console.log("Found satisfying solution - " + JSON.stringify(bestLocalExecution));
        finalize();
        return;
    }
    setTimeout(loopEntry);
}

function loopEntry () {
    executionsCountHolder.executionsCount += 1;
    console.log("Starting new loopEntry " + JSON.stringify(bestKnownExecution));
    if (bestKnownExecution.executionsCount && bestKnownExecution.executionsCount < executionsCountHolder.executionsCount) {
        finalize();
        return;
    }
    if (bestKnownExecution.executionsCount && bestKnownExecution.executionsCount === executionsCountHolder.executionsCount) {
        geneticTsp.setMaxIterationCount(bestKnownExecution.iterationNumber);
    }
    geneticTsp.solve(data, 620, onCalculationsCompleted);
}
setTimeout(loopEntry);
