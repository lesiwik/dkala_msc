function Engine(tools) {
    var self = this;

    function HashedList(values) {
        var self = this, first, last, nodes = [];

        function Node(value, prev, next) {
            var self = this;

            self.value = value;
            self.prev = prev;
            self.next = next;
        }

        self.getFirstValue = function () {
            return first.value;
        };

        self.appendValue = function(value) {
            var node;

            node = new Node(value, last);
            if (!first) {
                first = node;
            }
            if (last) {
                last.next = node;
            }
            last = node;
            nodes[value] = node;
        };

        self.removeValue = function(value) {
            var node = nodes[value];

            if (node.prev) {
                node.prev.next = node.next;
            }
            if (node.next) {
                node.next.prev = node.prev;
            }
            if (first === node) {
                first = node.next;
            }
            if (last === node) {
                last = node.prev;
            }
        };

        self.getNextValue = function(value) {
            var node = nodes[value];
            if (node.next) {
                return node.next.value;
            } else {
                return null;
            }
        };

        if (values) {
            values.forEach(function(value) {
                self.appendValue(value);
            });
        }
    }

    function getRandomNumber(range) {
        var randomNumber = Math.round(Math.random() * (range));
        return randomNumber;
    }

    self.mutate = function (X, coordinates) {
        var a, b, c, d, 
            ab, bc, cd,
            ac, cb, bd,
            temp;

        for (var i = 0; i < X.length - 3; i++) {
            a = X[i];
            b = X[i + 1];
            c = X[i + 2];
            d = X[i + 3];

            ab = tools.getDistance(coordinates[a], coordinates[b]);
            bc = tools.getDistance(coordinates[b], coordinates[c]);
            cd = tools.getDistance(coordinates[c], coordinates[d]);

            ac = tools.getDistance(coordinates[a], coordinates[c]);
            cb = tools.getDistance(coordinates[c], coordinates[b]);
            bd = tools.getDistance(coordinates[b], coordinates[d]);

            if (ab + bc + cd > ac + cb + bd) {
                temp = X[i + 1];
                X[i + 1] = X[i + 2];
                X[i + 2] = temp;
            }
        }
    };

    self.makeSCX = function (X, Y, coordinates) {
        var nextX, nextY, nextValue,
            costX, costY, Z = [0],
            hashedListX = new HashedList(X),
            hashedListY = new HashedList(Y),
            orderedHashedList = new HashedList();
            
        for (var value = 0; value < X.length; value++) {
            orderedHashedList.appendValue(value);
        }

        function getNextFreeValue(currentValue) {
            var value;

            value = orderedHashedList.getFirstValue();
            if (value == currentValue) {
                value = orderedHashedList.getNextValue(value);
            }

            return value;
        }

        currentValue = 0;
        while (Z.length < X.length) {
            nextX = hashedListX.getNextValue(currentValue);
            if (nextX === null) {
                nextX = getNextFreeValue(currentValue);
            }
            nextY = hashedListY.getNextValue(currentValue);
            if (nextY === null) {
                nextY = getNextFreeValue(currentValue);
            }

            if (nextX == nextY) {
                nextValue = nextX;
            } else {
                costX = tools.getDistance(coordinates[currentValue], coordinates[nextX]);
                costY = tools.getDistance(coordinates[currentValue], coordinates[nextY]);

                if (costX < costY) {
                    nextValue = nextX;
                } else {
                    nextValue = nextY;
                }
            }

            orderedHashedList.removeValue(currentValue);
            hashedListX.removeValue(currentValue);
            hashedListY.removeValue(currentValue);
            currentValue = nextValue;
            Z.push(currentValue);
        }
        return Z;
    };

    self.createPopulation = function (populationSize, chromosomeLength) {
        var population = [], chromosome, availableNodes = [],
            chromosomeIndex, pathIndex, randomIndex, randomElement;


        for (chromosomeIndex = 0; chromosomeIndex < populationSize; chromosomeIndex++) {
            for (var index = 0; index < chromosomeLength - 1; index++) {
                availableNodes[index] = index + 1;
            }
            chromosome = [0];

            for (pathIndex = 1; pathIndex < chromosomeLength; pathIndex++) {
                randomIndex = getRandomNumber(chromosomeLength - pathIndex - 1);
                randomElement = availableNodes[randomIndex];
                availableNodes[randomIndex] = availableNodes[chromosomeLength - pathIndex - 1];

                chromosome.push(randomElement);
            }
            population.push(chromosome);
        }

        return population;
    };

    self.calculateFitness = function (chromosome, coordinates) {
        var fitness, distance;

        distance = tools.getPathLength(chromosome, coordinates);
        fitness = 1 / distance;

        return fitness;
    };
}