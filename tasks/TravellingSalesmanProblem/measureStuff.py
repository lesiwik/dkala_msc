import sys, json, subprocess, time
sys.path.append("../../tools")
import controller

if len(sys.argv) != 9:
    print "python compose_tsp.py [dataCenterIp] [dataCenterPort] [populationSize] [maxIterationCount] [dataSource.tsp] [satisfyingPathLength] [devicesCount] [repetitionsCount]"
    raise SystemExit
    
dataCenterIp = sys.argv[1]
dataCenterPort = sys.argv[2]
populationSize = sys.argv[3]
maxIterationCount = sys.argv[4]
dataSourceFile = sys.argv[5]
satisfyingPathLength = sys.argv[6]
devicesCount = int(sys.argv[7])
repetitionsCount = int(sys.argv[8])

c = controller.Controller()
c.init('../../tools/devices.config')
print "Reseting all devices started ..."
c.resetAllDevices();
print "Reseting all devices completed!\n"

previousDevicesCount = 1
currentDevicesCount = 1
continuation = True
initialRepetitionNumber = 0

measurements = []

while currentDevicesCount <= devicesCount:
    deviceToConnect = previousDevicesCount
    while deviceToConnect < currentDevicesCount:
        print "Attempting to connect device number " + str(deviceToConnect) + " to root ..."
        c.connectDevice(deviceToConnect, 0)
        time.sleep(1)
        print "Connecting device number + " + str(deviceToConnect) + " to root completed!\n"
        deviceToConnect += 1
    
    if continuation:
        repetitionNumber = initialRepetitionNumber
        continuation = False
    else:
        repetitionNumber = 0
    print "Starting execution on " + str(currentDevicesCount) + " devices!\n"
    
    print "Composing tsp.js file ..."
    composingCommand = "python compose_tsp.py " + dataCenterIp + " " + dataCenterPort + " " + \
                        populationSize + " " + maxIterationCount + " " + dataSourceFile + " " + \
                        satisfyingPathLength
    subprocess.call(composingCommand, shell=True)   
    print "Composing tsp.js file completed!\n"
    
    with open("tsp.js", "r") as tspSource:
        taskJsCode = tspSource.read()
    taskName = "tsp_" + dataSourceFile + ".js"
    taskRequest = { 'TaskJsCode': taskJsCode, 'TaskName': taskName }
    
    repetitionMeasurements = []
    while repetitionNumber < repetitionsCount:
        print "\nScheduling task request on " + str(currentDevicesCount) + \
              " devices for the " + str(repetitionNumber) + "th time\n"
        time.sleep(1)
        scheduledTask = c.scheduleTaskRequest(0, taskRequest)
        time.sleep(1)
        
        deviceToWaitOn = 0
        timeTaken = 0
        while deviceToWaitOn < currentDevicesCount:
            print "Waiting for task result on device number " + str(deviceToWaitOn) + " ..."
            bestSolution = c.waitForTaskResult(deviceToWaitOn, scheduledTask['TaskId'])
            if timeTaken == 0:
                timeTaken = bestSolution["timeTaken"]
            elif bestSolution["timeTaken"] < timeTaken:
                timeTaken = bestSolution["timeTaken"]
            deviceToWaitOn += 1
        repetitionMeasurements.append(timeTaken)
        repetitionNumber += 1
    measurements.append(repetitionMeasurements)
    previousDevicesCount = currentDevicesCount
    currentDevicesCount += 1
    
    with open("results.json", "w") as myfile:
        myfile.write(json.dumps(measurements))

print json.dumps(measurements)    
    