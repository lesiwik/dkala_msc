function Tools() {
    var self = this;


    function deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    function getDistance(a, b) {
        var x, y, distance;
        x = a[0] - b[0];
        y = a[1] - b[1];
        distance = Math.round(Math.sqrt(x * x + y * y));

        return distance;
    }

    self.getDistance = function (a, b) {
        var distance = getDistance(a, b);
        return distance;
    };

    self.getPathLength = function (path, coordinates) {
        var distance, step, fromNode, toNode;

        distance = 0;
        for (var nodeIndex = 0; nodeIndex < path.length - 1; nodeIndex++) {
            fromNode = path[nodeIndex];
            toNode = path[nodeIndex + 1];
            step = getDistance(coordinates[fromNode], coordinates[toNode]);

            distance += step
        }
        fromNode = path[path.length - 1];
        toNode = path[0];
        step = getDistance(coordinates[fromNode], coordinates[toNode]);

        distance += step;
        return distance;
    };
}