import glob, json

measurements = []
parsed = []

for i in range(0, 14):
    parsed.append([])

for filename in glob.glob('measurements/*.json'):
    with open (filename, 'r') as measurementSource:
        measurements.append(json.loads(measurementSource.read()))

for measurement in measurements:
    iteration = -1
    for calculation in measurement['devicesCalculations']:
        if calculation == None:
            continue

        newIteration = (calculation['execution'] - 1) * 1000 + calculation['iterationNumber']

        if iteration == -1 or newIteration < iteration:
            iteration = newIteration
    
    if iteration != -1:
        index = measurement['devicesCount']
        
        parsed[index].append(iteration)

with open('measurements.csv', 'w') as chartSource:
    for el in parsed:
        for its in el:
            chartSource.write(str(its))
            chartSource.write(',')
        chartSource.write('\n')

