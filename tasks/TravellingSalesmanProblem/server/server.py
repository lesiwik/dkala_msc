import os, json, traceback, shutil

from flask import Flask, Response, request

if not os.path.exists('calculations'):
    os.makedirs('calculations')

if not os.path.exists('measurements'):
    os.makedirs('measurements')

app = Flask(__name__)

@app.route('/')
def hello():
    return "It works!"

@app.route('/debug/chart')
def chart():
    try:
        with open("chart.html", "r") as myfile:
            data = myfile.read()
        return Response(data, mimetype="text/html")
    except:
        return traceback.format_exc(), 500

@app.route('/measurements', methods=['POST'])
def startMeasurement():
    try:
        measurementInfo = json.loads(request.data)

        measurementData = {
            'taskId': measurementInfo['taskId'],
            'devicesCount': measurementInfo['devicesCount'],
            'devicesCalculations': []
        }

        for i in range(0, int(measurementData['devicesCount'])):
            measurementData['devicesCalculations'].append(None)
        
        with open('measurements/' + measurementData['taskId'] + '.json', 'w') as measurementFile:
            measurementFile.write(json.dumps(measurementData, indent=4))

        return "", 200
    except:
        return traceback.format_exc(), 500

@app.route('/measurements/<taskId>', methods=['GET'])
def getMeasurement(taskId):
    try:
        with open('measurements/' + taskId + '.json', 'r') as measurementFile:
            measurement = measurementFile.read()

        return Response(measurement, mimetype="application/json")
    except:
        return traceback.format_exc(), 500

@app.route('/measurements/<taskId>/devices/<deviceNumber>', methods=['POST'])
def updateMeasurement(taskId, deviceNumber):
    try:
        measurementData = json.loads(request.data)

        with open('measurements/' + taskId + '.json', 'r') as measurementFile:
            measurement = json.loads(measurementFile.read())
        
        measurement['devicesCalculations'][int(deviceNumber)] = measurementData
        
        with open('measurements/' + taskId + '.json', 'w') as measurementFile:
            measurementFile.write(json.dumps(measurement, indent=4))

        return "", 200
    except:
        return traceback.format_exc(), 500

@app.route('/debug/chart/<calculation>')
def getChartData(calculation):
    try:
        chartFilename = 'calculations/' + calculation + '/' + 'chart.js'
        with open(chartFilename, 'r') as chartStream:
            chart = json.loads(chartStream.read())

        data = []
        for i in range(0, len(chart)):
            data.append(0)

        for key in chart:
            data[int(key) - 1] = chart[key]
        return Response(json.dumps(data), mimetype="appliation/json")
    except:
        return traceback.format_exc(), 500

@app.route('/debug/population')
def population():
    with open("population.html", "r") as myfile:
        data = myfile.read()
    return Response(data, mimetype="text/html")

@app.route('/debug/population/<calculation>')
def getPopulationData(calculation):
    try:
        iteration = request.args.get('iteration', '0')
        chromosome = request.args.get('chromosome', '0')
        filename = 'calculations/' + calculation + '/' + iteration + '.js'
        with open(filename, 'r') as dataSource:
            data = json.loads(dataSource.read())

        X = data['population'][int(chromosome)]

        retVal = {}
        retVal['specimen'] = data['population'][int(chromosome)]
        retVal['pathLength'] = data['distances'][int(chromosome)]

        return Response(json.dumps(retVal), mimetype="application/json")
    except:
        return traceback.format_exc(), 500

@app.route('/<kind>/<filename>')
def resources(kind, filename):
    try:
      with open(kind + "/" + filename, "r") as myfile:
          data = myfile.read()
      if kind == "js":
          return Response(data, mimetype="text/javascript")
      elif kind == "css":
          return Response(data, mimetype="text/css")
      elif kind == "fonts":
          return Response(data, mimetype="font")
      return Response(data)
    except:
        return traceback.format_exc(), 404

@app.route('/calculation/<calculation>', methods=['POST'])
def prepareServerForNewData(calculation):
    try:
        shutil.rmtree('calculations/' + calculation);
    except:
        pass

    os.mkdir('calculations/' + calculation);

    with open('calculations/details.js', 'w') as detailsStream:
        detailsStream.write(request.data)
    
    return "", 200

@app.route('/calculation/details', methods=['GET'])
def getCoordinates():
    with open('calculations/details.js', 'r') as detailsStream:
        details = detailsStream.read()
    
    return Response(details, mimetype="application/json")
    

@app.route('/calculation/<calculation>/values', methods=['POST'])
def saveNewValue(calculation):
    try:
        chartFilename = 'calculations/' + calculation + '/' + 'chart.js'
        if os.path.isfile(chartFilename):
            with open(chartFilename, 'r') as chartStream:
                chart = json.loads(chartStream.read())
        else:
            chart = {}

        details = json.loads(request.data)
        bestPath = min(details['distances'])
        chart[str(details['iteration'])] = bestPath;


        with open(chartFilename, 'w') as chartStream:
            chartStream.write(json.dumps(chart))

        fileName = 'calculations/' + calculation + '/' + str(details['iteration']) + '.js'
        with open (fileName, 'w') as populationStream:
            populationStream.write(request.data)

        return ""
    except:
        return traceback.format_exc(), 500
    

