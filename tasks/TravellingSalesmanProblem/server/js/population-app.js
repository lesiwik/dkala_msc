function PopulationApp() {
    var self = this, 
        ctx, bounds, coordinatesRequest, coordinates,
        populationSize, iterationCount;

    ctx = document.getElementById("myPopulation").getContext("2d");

    coordinatesRequest = $.ajax({
        url: "/calculation/details",
        type: "GET",
        dataType: "json"
    }).done(function(details) {
        var coords = details.coordinates,
            xs, ys;

        bounds = {
            x: {},
            y: {}
        };

        xs = _(coords).map(function(coord) {
            return coord[0];
        });
        ys = _(coords).map(function(coord) {
            return coord[1];
        });

        bounds.x.lower = _.min(xs);
        bounds.x.upper = _.max(xs);
        bounds.y.lower = _.min(ys);
        bounds.y.upper = _.max(ys);

        coordinates = coords;
        populationSize = details.populationSize;
        iterationCount = details.iterationCount;

        self.drawPopulation();
    });

    function mapCoordinate(coordinate) {
        var newCoordinate = [];

        newCoordinate[0] = 
            (coordinate[0] - bounds.x.lower) / (bounds.x.upper - bounds.x.lower) * 760 + 20;
        newCoordinate[1] =
            (coordinate[1] - bounds.y.lower) / (bounds.y.upper - bounds.y.lower) * 560 + 20;

        newCoordinate[1] = 600 - newCoordinate[1];

        return newCoordinate;
    }

    self.PathLength = ko.observable();
    self.CalculationNumber = ko.observable(0);
    self.IterationNumber = ko.observable(1);
    self.ChromosomeNumber = ko.observable(0);
    self.Specimen = ko.observableArray();
    self.SpecimenDescription = ko.computed(function() {
        return self.Specimen().join(', ');
    });

    self.CalculationNumber.subscribe(function() {
        self.drawPopulation();
    });

    self.IterationNumber.subscribe(function() {
        self.drawPopulation();
    });

    self.ChromosomeNumber.subscribe(function() {
        self.drawPopulation();
    });

    self.drawPopulation = function() {
        var calculationNumber, iterationNumber, chromosomeNumber;

        calculationNumber = parseInt(self.CalculationNumber(), 10);
        iterationNumber = parseInt(self.IterationNumber(), 10);
        chromosomeNumber = parseInt(self.ChromosomeNumber(), 10);

        return $.when(coordinatesRequest).pipe(function() {
            $.ajax({
                url: "/debug/population/" + calculationNumber + 
                     "?iteration=" + iterationNumber + 
                     "&chromosome=" + chromosomeNumber,
                type: "GET",
                dataType: "json"
            }).done(function(response) {
                self.Specimen(response.specimen);
                self.PathLength(response.pathLength);
                self.draw(response.specimen);
            });
        });
    };

    self.draw = function(path) {

        ctx.clearRect(0, 0, 800, 600);

        for (var i = 0; i < path.length - 1; i++) {
            var point;

            point = mapCoordinate(coordinates[path[i]]);
            nextPoint = mapCoordinate(coordinates[path[i + 1]]);
            

            ctx.fillRect(point[0] - 1, point[1] - 1, 3, 3);
            ctx.fillRect(nextPoint[0] - 1, nextPoint[1] - 1, 3, 3);

            ctx.beginPath();
            ctx.moveTo(point[0], point[1]);
            ctx.lineTo(nextPoint[0], nextPoint[1]);
            ctx.stroke();
        }

    };
}

ko.applyBindings(new PopulationApp());
