function ChartApp() {
    var self = this, options;

    function createChartData(values) {
        var data, labels, selectedValues, skip, index, valuesIndex;
        if (values.length > 100) {
            selectedValues = [];
            labels = [];

            skip = values.length / 100;
            index = 0;
            valuesIndex = 0;
            while(index < 100) {
                selectedValues[index] = values[Math.round(valuesIndex)];
                labels[index] = Math.round(valuesIndex) + 1;

                index += 1;
                valuesIndex += skip;
            }
            

            labels[99] = values.length;
            values = selectedValues;
        } else {
            labels = _.range(1, values.length + 1);
        }

        data = {
            labels: labels,
            datasets: [
                {
                    label: "Best path per iteration",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: values
                }
            ]
        };

        return data;
    }

    self.CalculationNumber = ko.observable(0);
    self.drawChart = function() {
        var calculationNumber = parseInt(self.CalculationNumber(), 10);

        return $.ajax({
            url: "/debug/chart/" + calculationNumber,
            type: "GET",
            dataType: "json"
        }).done(function(response) {
            var ctx, myLineChart, data = createChartData(response);
            $("#myChart").remove();
            $("#canvas-container").append('<canvas id="myChart" width="900" height="850"></canvas>');
            ctx = document.getElementById("myChart").getContext("2d");
            myLineChart = new Chart(ctx);
            myLineChart.Line(data, options);
        });
    };

    setInterval(function() {
        self.drawChart();
    }, 10000);

    options = {

        ///Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines : true,

        //String - Colour of the grid lines
        scaleGridLineColor : "rgba(0,0,0,.05)",

        //Number - Width of the grid lines
        scaleGridLineWidth : 1,

        //Boolean - Whether the line is curved between points
        bezierCurve : true,

        //Number - Tension of the bezier curve between points
        bezierCurveTension : 0.4,

        //Boolean - Whether to show a dot for each point
        pointDot : true,

        //Number - Radius of each point dot in pixels
        pointDotRadius : 4,

        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth : 1,

        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 0,

        //Boolean - Whether to show a stroke for datasets
        datasetStroke : true,

        //Number - Pixel width of dataset stroke
        datasetStrokeWidth : 2,

        //Boolean - Whether to fill the dataset with a colour
        datasetFill : true,

        //String - A legend template
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

    };
}

ko.applyBindings(new ChartApp());
