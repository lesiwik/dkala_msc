function Statistics(serverIp, serverPort) {
    var self = this;

    if (!serverPort) {
        serverPort = "5000";
    }

    function getServerAddress() {
        return "http://" + serverIp + ":" + serverPort;
    }

    self.setBestPath = function (iteration, bestPath) {
        
    };

    self.setBestPathLength = function (iteration, bestPathLength) {
        
    };

    self.saveIterationDetails = function (iteration, population) {
        /*
        var details, serverAddress = getServerAddress();

        details = {
            iteration: iteration,
            population: population,
            distances: []
        };

        population.forEach(function (chromosome, index) {
            if (chromosome.fitness) {
                details.distances[index] = 1 / chromosome.fitness;
            }
        });

        PlatformTools.sendDataToServer(
            serverAddress + "/calculation/" + Platform.getDeviceNumber() + "/values",
            details
        );
        */
    };

    self.saveTaskInformation = function (coordinates, iterationCount) {
        /*
        PlatformTools.sendDataToServer(
            getServerAddress() + "/calculation/" + Platform.getDeviceNumber(), {
                coordinates: coordinates,
                iterationCount: iterationCount
            }
        );
        */
    };
}
