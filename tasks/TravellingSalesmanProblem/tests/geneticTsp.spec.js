/// <reference path="lib/sinon-1.10.1.js" />
/// <reference path="jasmine.js" />

/// <reference path="../data/xqf131Coordinates.js" />

/// <reference path="../geneticTsp.js" />
/// <reference path="../engine.js" />
/// <reference path="../tools.js" />

(function (undefined) {

    var engineProxy, engine, tools, statistics;

    function Statistics() {
        var self = this,
            bestPaths = [], bestPathLengths = [];

        self.setBestPath = function (iteration, bestPath) {
            bestPaths[iteration] = bestPath;
        };

        self.setBestPathLength = function (iteration, bestPathLength) {
            bestPathLengths[iteration] = bestPathLength;
        };

        self.saveTaskInformation = function (coordinates) {

        };

        self.saveIterationDetails = function (iteration, population) {

        };

        self.getBestPath = function (iteration) {
            return bestPaths[iteration];
        };

        self.getBestPathLength = function (iteration) {
            return bestPathLengths[iteration];
        };

        self.getIterationsCount = function () {
            return bestPaths.length;
        };
    }

    describe("TSP PGA genetic algorithm", function () {
        var tools, engine, engineProxy, engineProxyStubs, geneticTsp, testStartDate;

        function createGeneticTsp(options) {
            var geneticTsp;

            if (options == undefined) {
                options = {};
            }
            if (options.populationSize == undefined) {
                options.populationSize = 100;
            }
            if (options.mutationChance == undefined) {
                options.mutationChance = 0.1;
            }
            if (options.maxIterationCount == undefined) {
                options.maxIterationCount = 100;
            }

            geneticTsp = new GeneticTsp(engineProxy, options, statistics);
            return geneticTsp;
        }

        beforeEach(function () {
            tools = new Tools();
            engine = new Engine(tools);
            statistics = new Statistics();

            engineProxy = {
                mutate: function () {

                },

                makeSCX: function () {

                },

                createPopulation: function () {

                },

                calculateFitness: function () {

                }
            };

            engineProxyStubs = {
                mutate: sinon.stub(engineProxy, "mutate", function () {
                    return engine.mutate.apply(engine, arguments);
                }),

                makeSCX: sinon.stub(engineProxy, "makeSCX", function () {
                    return engine.makeSCX.apply(engine, arguments);
                }),

                createPopulation: sinon.stub(engineProxy, "createPopulation", function () {
                    return engine.createPopulation.apply(engine, arguments);
                }),

                calculateFitness: sinon.stub(engineProxy, "calculateFitness", function (c) {
                    return engine.calculateFitness.apply(engine, arguments);
                })
            };
        });

        describe("behavioral tests", function () {
            var currentStub, coordinates, population, stubs, options, tooStrictPathRequirement = 0;

            beforeEach(function () {
                options = {};
            });
            
            it("should create population with proper chromosome count and length", function () {
                // arrange
                options.populationSize = 100;
                options.maxIterationCount = 1;

                function onCalculationsCompleted() {
                    // assert
                    expect(
                        engineProxyStubs.createPopulation.calledWith(options.populationSize, xqf131Coordinates.length)
                    ).toBeTruthy();
                }

                // act
                geneticTsp = createGeneticTsp(options);
                geneticTsp.solve(xqf131Coordinates, tooStrictPathRequirement, onCalculationsCompleted);
            });

            it("should make certain population part undergo a mutation in each iteration", function () {
                var expectedCallsCount, mutatedChromosomes;

                // arrange
                options.populationSize = 300;
                options.nutationChance = 0.1;
                options.maxIterationCount = 3;

                mutatedChromosomes = Math.floor(options.nutationChance * options.populationSize);
                expectedCallsCount = mutatedChromosomes * options.maxIterationCount;

                function onCalculationsCompleted() {
                    // assert
                    expect(engineProxyStubs.mutate.callCount).toEqual(expectedCallsCount);
                }

                // act
                geneticTsp = createGeneticTsp(options);
                geneticTsp.solve(xqf131Coordinates, tooStrictPathRequirement);
            });

            it("should return best path as calculation result", function () {
                var options, solution, bestPathLength;

                // arrange
                options = {
                    maxIterationCount: 100
                };

                // act
                geneticTsp = createGeneticTsp(options);
                geneticTsp.solve(xqf131Coordinates, 564);

                function onCalculationsCompleted(solution) {
                    // assert
                    for (var iteration = 0; iteration < statistics.getIterationsCount() ; iteration++) {
                        if (!bestPathLength) {
                            bestPathLength = statistics.getBestPathLength(iteration);
                            continue;
                        }
                        if (bestPathLength > statistics.getBestPathLength(iteration)) {
                            bestPathLength = statistics.getBestPathLength(iteration);
                        }
                    }
                    expect(solution.pathLength).toEqual(bestPathLength);
                }
            });

        });
    });

}());