/// <reference path="jasmine.js" />
/// <reference path="../data/finlandCoordinates.js" />
/// <reference path="../data/finlandPath.js" />
/// <reference path="../tools.js" />

(function () {

    describe("TSP PGA tools", function () {
        var tools;

        beforeEach(function () {
            tools = new Tools();
        });

        describe("get distance between locations", function () {
            var testCases = [{
                a: [0.0, 0.0],
                b: [0.0, 0.0],
                expectedLowerBound: 0,
                expectedUpperBound: 0
            }, {
                a: [10.3, -20.2],
                b: [10.3, -20.2],
                expectedLowerBound: 0,
                expectedUpperBound: 0
            }, {
                a: [-1, 2],
                b: [5, -3],
                expectedLowerBound: 8,
                expectedUpperBound: 9
            }];

            function createDescription(testCase) {
                var a = testCase.a, b = testCase.b, description;

                description = "expect (" + a[0] + ", " + a[1] + ") & (" + b[0] + ", " + b[1] + ") " +
                              "to be between [" + testCase.expectedLowerBound + ", " + testCase.expectedUpperBound + "]";

                return description;
            }

            testCases.forEach(function (testCase) {
                var description = createDescription(testCase);


                it(description, function () {
                    var distance,
                        lowerBound = testCase.expectedLowerBound,
                        upperBound = testCase.expectedUpperBound;

                    // act
                    distance = tools.getDistance(testCase.a, testCase.b);

                    // assert
                    expect(distance).not.toBeLessThan(lowerBound);
                    expect(distance).not.toBeGreaterThan(upperBound);
                });
            })

        });

        describe("get total length for path", function () {

            it("over finland", function () {
                var actualDistance, expectedDistance = 520527;

                // act
                actualDistance = tools.getPathLength(finlandPath, finlandCoordinates);

                // assert
                expect(expectedDistance).toEqual(actualDistance);
            });

        });
    });

}());
