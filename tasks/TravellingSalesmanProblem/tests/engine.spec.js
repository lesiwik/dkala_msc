/// <reference path="lib/sinon-1.10.1.js" />
/// <reference path="jasmine.js" />

/// <reference path="../engine.js" />

(function () {

    var costMatrix,dependencies;
    
    costMatrix = [
            /* 0,   1,   2,   3,   4,   5,   6  */
    /* 0 */ [999,  75,  99,   9,  35,  63,   8],
    /* 1 */ [ 51, 999,  86,  46,  88,  29,  20],
    /* 2 */ [100,   5, 999,  16,  28,  35,  28],
    /* 3 */ [ 20,  45,  11, 999,  59,  53,  49],
    /* 4 */ [ 86,  63,  33,  65, 999,  76,  72],
    /* 5 */ [ 36,  53,  89,  31,  21, 999,  52],
    /* 6 */ [ 58,  31,  43,  67,  52,  60, 999]
    ];

    dependencies = {
        tools: {
            getDistance: function (a, b) {

            },

            getPathLength: function (a, b) {

            }
        }
    };

    describe("TSP PGA engine", function () {
        var engine, coordinates, getDistanceStub, testCases;

        beforeEach(function () {
            engine = new Engine(dependencies.tools);
            coordinates = [0, 1, 2, 3, 4, 5, 6];
            getDistanceStub = sinon.stub(dependencies.tools, "getDistance", function (a, b) {
                return costMatrix[a][b];
            });
        });

        afterEach(function () {
            getDistanceStub.restore();
        });

        describe("should make Sequential Constructive Crossover properly", function () {
            var testCases;

            testCases = [{
                X: [0, 1, 2], Y: [0, 2, 1], Z: [0, 1, 2]
            }, {
                X: [0, 4, 6, 2, 5, 3, 1], Y: [0, 5, 1, 3, 2, 4, 6], Z: [0, 4, 6, 1, 3, 2, 5]
            }];

            function createDescription(testCase) {
                var description = "(" + testCase.X.join(", ") + ") and (" + testCase.Y.join(", ") + ") " +
                                  "should cross as (" + testCase.Z.join(", ") + ")";

                return description;
            }

            testCases.forEach(function (testCase) {
                var description = createDescription(testCase);

                it("description", function () {
                    var actualCrossover;

                    // act
                    actualCrossover = engine.makeSCX(testCase.X, testCase.Y, coordinates);

                    // assert
                    expect(actualCrossover).toEqual(testCase.Z);
                });
            });
        });

        describe("mutation", function () {
            var testCases;

            testCases = [
                [0, 4, 6, 2, 5, 3, 1],
                [0, 5, 1, 3, 2, 4, 6],
                [0, 4, 6, 1, 3, 2, 5]
            ];

            function createDescription(testCase) {
                var description = "(" + testCase.join(", ") + ")";

                return description;
            }

            describe("should midly adjust chromosome", function () {

                testCases.forEach(function (testCase) {
                    var description = createDescription(testCase);

                    it(description, function () {
                        var initialValue = testCase.slice(0);

                        // act
                        engine.mutate(testCase, coordinates);

                        // assert
                        expect(testCase).not.toEqual(initialValue);
                    });
                });

            });

            describe("should leave 0 as first chromosome", function () {

                testCases.forEach(function (testCase) {
                    var description = createDescription(testCase);

                    it(description, function () {
                        // act
                        engine.mutate(testCase, coordinates);

                        // assert
                        expect(testCase[0]).toEqual(0);
                    });
                });

            });

        });

        describe("should create random population", function () {

            function assertPopulationRandomness(population) {
                var differentChromosomes = [];

                if (population.length < 2) {
                    return;
                }

                differentChromosomes.push(population[0]);
                for (var outterIndex = 1; outterIndex < population.length; outterIndex++) {
                    var elementToVerify = population[outterIndex];
                    for (var innerIndex = 0; innerIndex < differentChromosomes.length; innerIndex++) {
                        expect(elementToVerify).not.toEqual(differentChromosomes[innerIndex]);
                    }
                    differentChromosomes.push(elementToVerify);
                }
            }

            it("within single creation", function () {
                var population;

                // act
                population = engine.createPopulation(10, 7);

                // assert
                assertPopulationRandomness(population);
            });

            it("accross multiple invocations", function () {
                var combinedPopulations = [];

                // act
                combinedPopulations.push.apply(combinedPopulations, engine.createPopulation(5, 10));

                // assert
                assertPopulationRandomness(combinedPopulations);
            });

            it("with exact chromosome count", function () {
                var population;

                // act
                population = engine.createPopulation(52, 2);

                // assert
                expect(population.length).toEqual(52);
            });

            it("with exact chromosome length", function () {
                var population;

                // act
                population = engine.createPopulation(1, 82);

                // assert
                expect(population[0].length).toEqual(82);
            });

            function assertChromosomeValidity(chromosome) {
                expect(chromosome[0]).toEqual(0);
                chromosome.sort(function (a, b) {
                    return a - b;
                });
                for (var index = 0; index < chromosome.length; index++) {
                    expect(chromosome[index]).toEqual(index);
                }
            }

            it("with valid TSP path in each chromosome", function () {
                var population;

                // act
                population = engine.createPopulation(300, 1000);

                // assert
                population.forEach(function (chromosome) {
                    assertChromosomeValidity(chromosome);
                });
            });

        });

        describe("should properly calculate chromosome fitness", function () {
            var testCases, getPathLengthStub;

            testCases = [{
                chromosome: [0, 1, 2, 3, 4, 5, 6], fitness: 1/422, pathLength: 422,
            }, {
                chromosome: [0, 3, 5, 6, 2, 1, 4], fitness: 1/336, pathLength: 336
            }];

            function createDescription(testCase) {
                var description = "chromosome - (" + testCase.chromosome.join(", ") + ") " +
                                  "should have fitness set to " + testCase.fitness;

                return description;
            }

            afterEach(function () {
                getPathLengthStub.restore();
            });

            testCases.forEach(function (testCase) {
                var description = createDescription(testCase);

                it(description, function () {
                    var actualFitness;

                    // arrange
                    getPathLengthStub = sinon.stub(dependencies.tools, "getPathLength", function () {
                        return testCase.pathLength
                    });

                    // act
                    actualFitness = engine.calculateFitness(testCase.chromosome, coordinates);

                    // assert
                    expect(actualFitness).toEqual(testCase.fitness);
                });
            });
        });
    });

}());