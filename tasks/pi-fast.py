import sys, time, json
sys.path.insert(0, '../tools')

import controller
controller = controller.Controller()
controller.init('devices.config')
with open('devices.config') as configFile:
	devicesCount = sum(1 for line in configFile)
	
controller.resetAllDevices()


with open("pi-fast.js", "r") as myFile:
	task = myFile.read()

for d in range(1, devicesCount + 1):
	shots = 2000000
	
	if d == 1:
		print "Measurements for 1 device:"
	else:
		print "Measurements for " + str(d) + " devices:"
		
	for i in range(1, 11):
		specificTask = task + 'task(' + str(shots) + ');'
		taskRequest = { 'TaskJsCode': specificTask }
		taskId = controller.scheduleTaskRequest(0, taskRequest)['TaskId']
		taskResult = controller.waitForTaskResult(0, taskId);
		
		if i <= 3:
			print "\t" + str(shots) + "\t\t" + str(taskResult['time'])
		else:
			print "\t" + str(shots) + "\t" + str(taskResult['time'])
		
		shots *= 2
	
	if d == 4:
		break
	controller.connect(d, d - 1)
	time.sleep(0.3)
