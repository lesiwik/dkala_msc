import os, json, traceback
from flask import Flask, Response, request

app = Flask(__name__)

@app.route('/')
def hello():
    with open("canvas_fast.html", "r") as myfile:
        data = myfile.read()
    return Response(data, mimetype="text/html")

@app.route('/tasks/', methods=['POST'])
def saveTask():
    try:
        data = request.data
        newTask = json.loads(data)
        with open('tasks/' + newTask['taskId'], "w") as myfile:
            myfile.write(data)

        return "", 200
    except:
        return traceback.fromat_exc(), 500

@app.route('/tasks/', methods=['GET'])
def getTask():
    path = request.args.get('taskId', '')
    if os.path.isfile('tasks/' + path) is False:
        return "Task hasn't started yet", 404
    
    with open('tasks/' + path, "r") as myfile:
        taskData = myfile.read()

    return Response(taskData, mimetype="application/json")

@app.route('/data/', methods=['POST'])
def saveTaskResult():
    try:
        taskId = request.args.get('taskId', '')
        if taskId == '':
            return 'task id must be provided', 400

        deviceNumber = request.args.get('deviceNumber', '')
        if deviceNumber == '':
            return 'device number must be provided', 400

        taskDataFile = 'tasks/' + taskId + '__' + deviceNumber
        with open(taskDataFile, "w") as myfile:
            myfile.write(request.data)

        return "", 200
    except:
        return traceback.fromat_exc(), 500

@app.route('/data/', methods=['GET'])
def getTaskResult():
    try:
        taskId = request.args.get('taskId', '')
        if taskId == '':
            return 'task id must be provided', 400

        deviceNumber = request.args.get('deviceNumber', '')
        if deviceNumber == '':
            return 'device number must be provided', 400

        taskDataFile = 'tasks/' + taskId + '__' + deviceNumber
        with open(taskDataFile, "r") as myfile:
            data = myfile.read()

        return Response(data, mimetype="application/json")
    except:
        return traceback.fromat_exc(), 500
