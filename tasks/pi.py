import sys, time
sys.path.insert(0, '../tools')

import controller
controller = controller.Controller()
controller.init('devices.config')
with open('devices.config') as configFile:
	devicesCount = sum(1 for line in configFile)
	
controller.resetAllDevices()

for i in range(1, devicesCount):
	controller.connect(i, i - 1)
	time.sleep(0.3)
	
time.sleep(2)

with open("pi.js", "r") as myFile:
	task = myFile.read()
	
task += 'task(200000000);';

taskRequest = { 'TaskJsCode': task }
taskId = controller.scheduleTaskRequest(0, taskRequest)['TaskId']

print taskId