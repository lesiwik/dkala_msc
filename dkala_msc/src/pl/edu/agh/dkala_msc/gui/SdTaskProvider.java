package pl.edu.agh.dkala_msc.gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Environment;

public class SdTaskProvider {

	private Map<String, String> _tasks = new HashMap<String, String>();
	
	public List<String> GetAvailableTasks() {
		ArrayList<String> tasks = new ArrayList<String>();

		for (String taskName : _tasks.keySet()) {
			tasks.add(taskName);
		}
		
		return tasks;
	}
	
	public String GetTaskJsCode(String taskName) {
		return _tasks.get(taskName);
	}
	
	public void Initialize() {
		fillTasksList(Environment.getExternalStorageDirectory());
		_tasks.put("empty.js", "PlatformTools.finalize(\"Done!\");");
	}
	
	private void fillTasksList (File dir) {
		String pdfPattern = ".js";

		File[] listFile = dir.listFiles();

		if (listFile == null) {
			return;
		}

		for (int i = 0; i < listFile.length; i++) {

			if (listFile[i].isDirectory()) {
				fillTasksList(listFile[i]);
			} else if (listFile[i].getName().endsWith(pdfPattern)) {
				String taskName = listFile[i].getName();
				String taskJsCode;
				try {
					taskJsCode = getStringFromFile(listFile[i].getAbsolutePath());					
					_tasks.put(taskName, taskJsCode);
				} catch (Exception e) {	}
			}
			
		}
	}
	
	private static String convertStreamToString(InputStream is) throws Exception {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	      sb.append(line).append("\n");
	    }
	    reader.close();
	    return sb.toString();
	}

	private static String getStringFromFile (String filePath) throws Exception {
	    File fl = new File(filePath);
	    FileInputStream fin = new FileInputStream(fl);
	    String ret = convertStreamToString(fin);
	    //Make sure you close all streams.
	    fin.close();        
	    return ret;
	}

}
