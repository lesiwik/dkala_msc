package pl.edu.agh.dkala_msc.gui.fragments;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.app.AlertDialog;
import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.R;
import pl.edu.agh.dkala_msc.infrastructure.LocalTaskInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class TaskHandlingFragment extends Fragment {

	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";
	private View rootView;

	private Map<String, LocalTaskInfo> _completedLocalTasks;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static TaskHandlingFragment newInstance(int sectionNumber) {
		TaskHandlingFragment fragment = new TaskHandlingFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public void updateCompletedLocalTasksList(
			Map<String, LocalTaskInfo> completedLocalTasks) {
		_completedLocalTasks = new HashMap<String, LocalTaskInfo>();
		Iterator<Entry<String, LocalTaskInfo>> it = completedLocalTasks
				.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, LocalTaskInfo> pairs = it.next();
			LocalTaskInfo task = pairs.getValue();
			String entry = task.getTaskName() + " - " +pairs.getKey();

			_completedLocalTasks.put(entry, task);
		}

		Spinner completedTasksDropdown = (Spinner) rootView
				.findViewById(R.id.selectCompletedTask);

		String[] list = _completedLocalTasks.keySet().toArray(
				new String[_completedLocalTasks.size()]);

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
				this.getActivity(), android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		completedTasksDropdown.setAdapter(dataAdapter);

		rootView.invalidate();
	}
	
	public void setCurrentRunningTask(String taskName, String taskRootIp, String taskId) {
		Button startTask = (Button) rootView.findViewById(R.id.startTask);
		Spinner taskDropdown = (Spinner) rootView.findViewById(R.id.taskSelector);
		if (taskName == null) {
			startTask.setEnabled(true);
			taskDropdown.setEnabled(true);
		} else {
			startTask.setEnabled(false);
			taskDropdown.setEnabled(false);
		}

		rootView.invalidate();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_task_handling, container,
				false);

		setTasksList();
		setStartTaskListener();
		setOpenTaskResultListener();
		MainActivity.TaskHandler.updateCompletedLocalTasksList();
		MainActivity.TaskHandler.setCurrentRunningTask();
		
		return rootView;
	}

	private void setTasksList() {
		Spinner taskDropdown = (Spinner) rootView
				.findViewById(R.id.taskSelector);
		List<String> list = MainActivity.SdTaskProvider.GetAvailableTasks();
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
				this.getActivity(), android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		taskDropdown.setAdapter(dataAdapter);
	}

	private void setStartTaskListener() {
		Button startTask = (Button) rootView.findViewById(R.id.startTask);
		startTask.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Spinner taskDropdown = (Spinner) rootView
						.findViewById(R.id.taskSelector);
				String taskName = taskDropdown.getSelectedItem().toString();
				String taskJsCode = MainActivity.SdTaskProvider
						.GetTaskJsCode(taskName);

				MainActivity.TaskHandler.scheduleTaskExecution(taskName,
						taskJsCode, new String[0]);
			}

		});
	}

	private void setOpenTaskResultListener() {
		Button openTaskResult = (Button) rootView
				.findViewById(R.id.openTaskResult);
		openTaskResult.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Spinner completedTasksDropdown = (Spinner) rootView
						.findViewById(R.id.selectCompletedTask);
				String completedTaskEntry = completedTasksDropdown.getSelectedItem()
						.toString();
				
				LocalTaskInfo task = _completedLocalTasks.get(completedTaskEntry);
				
				AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
				alertDialog.setTitle(completedTaskEntry);
				alertDialog.setMessage(task.getResult());

				alertDialog.show();
			}

		});
	}

}
