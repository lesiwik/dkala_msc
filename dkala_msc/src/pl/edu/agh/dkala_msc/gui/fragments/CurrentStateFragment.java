package pl.edu.agh.dkala_msc.gui.fragments;

import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class CurrentStateFragment extends Fragment {
	/**
	 * The fragment argument representing the section number for this
	 * fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";
	View rootView;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static CurrentStateFragment newInstance(int sectionNumber) {
		CurrentStateFragment fragment = new CurrentStateFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public void setNeighbourCountInformation(String rootIp,
			int neighborCount) {
		TextView currentCloud = (TextView) rootView
				.findViewById(R.id.currentCloud);
		TextView rootIpText = (TextView) rootView.findViewById(R.id.rootIp);
		TextView neighborCountText = (TextView) rootView
				.findViewById(R.id.neighborsCount);
		if (neighborCount == 0) {
			currentCloud.setText("Not a part of any cloud.");
			rootIpText.setText("");
			neighborCountText.setText("");
		} else {
			currentCloud.setText("In Cloud!");
			rootIpText.setText("Root ip: " + rootIp);
			neighborCountText.setText("Neighbors count: " + neighborCount);
		}
		rootView.invalidate();
	}

	public void setCurrentRunningTask(String taskName, String taskRootIp,
			String taskId) {
		TextView currentTask = (TextView) (TextView) rootView
				.findViewById(R.id.currentTask);
		TextView taskNameText = (TextView) (TextView) rootView
				.findViewById(R.id.taskName);
		TextView taskRootIpText = (TextView) (TextView) rootView
				.findViewById(R.id.taskRootIp);
		TextView taskIdText = (TextView) (TextView) rootView
				.findViewById(R.id.taskId);
		TextView taskIdPlaceholderText = (TextView) (TextView) rootView
				.findViewById(R.id.taskIdPlaceholder);
		if (taskName == null) {
			currentTask.setText("Not running any task.");
			taskNameText.setText("");
			taskRootIpText.setText("");
			taskIdText.setText("");
			taskIdPlaceholderText.setText("");
		} else {
			currentTask.setText("Executing task!");
			taskNameText.setText("Task name: " + taskName);
			taskRootIpText.setText("Task root ip: " + taskRootIp);
			taskIdText.setText("Task id: ");
			taskIdPlaceholderText.setText(taskId);
		}
		
		rootView.invalidate();
	}

	public void setErrorInformation(String errorMessage) {
		TextView errorInformationPlaceholder = (TextView) rootView
				.findViewById(R.id.exceptionPlaceholder);
		
		errorInformationPlaceholder.setText(errorMessage);
		rootView.invalidate();
	}

	public CurrentStateFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_current_state,
				container, false);

		setNeighbourCountInformation(null, 0);
		setCurrentRunningTask(null, null, null);
		setErrorInformation(null);
		
		MainActivity.Infrastructure.setNeighbourCountInformation();
		MainActivity.TaskHandler.setCurrentRunningTask();

		return rootView;
	}
}