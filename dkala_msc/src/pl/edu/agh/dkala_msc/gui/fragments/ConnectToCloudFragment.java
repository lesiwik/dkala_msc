package pl.edu.agh.dkala_msc.gui.fragments;

import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class ConnectToCloudFragment extends Fragment {
	/**
	 * The fragment argument representing the section number for this
	 * fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";
	private View rootView;
	
	public void setNeighbourCountInformation(String rootIp, int neighborsCount) {
		EditText cloudIp = (EditText) rootView.findViewById(R.id.cloudIp);
		Button connectButton = (Button) rootView.findViewById(R.id.connectButton);
		Button disconnectButton = (Button) rootView.findViewById(R.id.disconnectButton);
		
		if (neighborsCount == 0) {
			cloudIp.setEnabled(true);
			connectButton.setEnabled(true);
			disconnectButton.setEnabled(false);
		} else {
			cloudIp.setEnabled(false);
			connectButton.setEnabled(false);
			disconnectButton.setEnabled(true);
		}
	}

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static ConnectToCloudFragment newInstance(int sectionNumber) {
		ConnectToCloudFragment fragment = new ConnectToCloudFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public ConnectToCloudFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_connect_to_cloud,
				container, false);

		final Button connect = (Button) rootView
				.findViewById(R.id.connectButton);
		connect.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				EditText ipAddress = (EditText) rootView.findViewById(R.id.cloudIp);
				final String address = ipAddress.getText().toString();
				(new Thread() {
					public void run() {
						MainActivity.Infrastructure.connectCurrentDeviceToCloud(address);
					}
				}).start();
			}
		});

		final Button disconnect = (Button) rootView
				.findViewById(R.id.disconnectButton);
		disconnect.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				MainActivity.Infrastructure.reset();
			}
		});
		
		MainActivity.Infrastructure.setNeighbourCountInformation();			
		return rootView;
	}
}
