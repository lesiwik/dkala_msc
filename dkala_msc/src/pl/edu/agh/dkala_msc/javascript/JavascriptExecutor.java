package pl.edu.agh.dkala_msc.javascript;

import android.webkit.JavascriptInterface;

import com.google.gson.Gson;

import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.device.DeviceInformation;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.internal.requests.DeviceMessageRequest;
import pl.edu.agh.dkala_msc.infrastructure.DeviceSensorManager;
import pl.edu.agh.dkala_msc.infrastructure.TaskInfo;

public class JavascriptExecutor {

	private Gson gson;
	
	private int _deviceNumber;
	private TaskInfo _taskInfo;
	private MainActivity _mainActivity;
	private NetworkThread _networkThread;
	private DeviceInformation _currentDevice;
	private DeviceInformation[] _orderedDevices;
	private DeviceSensorManager _deviceSensorManager;

	public JavascriptExecutor(TaskInfo taskInfo,
			DeviceSensorManager sensorManager, MainActivity mainActivity,
			NetworkThread networkThread) {
		gson = new Gson();
		_taskInfo = taskInfo;
		_mainActivity = mainActivity;
		_networkThread = networkThread;

		_currentDevice = taskInfo.getCurrentDevice();
		_orderedDevices = taskInfo.getOrderedDevices();
		_deviceSensorManager = sensorManager;
		_deviceNumber = calculateDeviceNumber(_orderedDevices, _currentDevice);
	}

	static private int calculateDeviceNumber(DeviceInformation[] orderedDevices, DeviceInformation currentDevice) {
		for (int deviceNumber = 0; deviceNumber < orderedDevices.length; deviceNumber++) {
			DeviceInformation deviceInformation = orderedDevices[deviceNumber];
			if (deviceInformation.IPAddress.equals(currentDevice.IPAddress)) {
				return deviceNumber;
			}
		}
		
		return -1;
	}
	
	@JavascriptInterface
	public void sendToDevice(final String json, final int device) {
		DeviceMessageRequest deviceMessageRequest = new DeviceMessageRequest();
		deviceMessageRequest.Message = json;
		deviceMessageRequest.Sender = _deviceNumber;
		deviceMessageRequest.TaskId = _taskInfo.getTaskId();

		DeviceInformation receiver = _orderedDevices[device];

		String url = "http://" + receiver.IPAddress + ":8080/messages";
		
		_networkThread.ScheduleRequest(url, deviceMessageRequest, "post");
	}
	
	@JavascriptInterface
	public String receiveFromDevice(int device) {
		String message = _taskInfo.dequeueMessage(device);
		return message;
	}
	
	@JavascriptInterface
	public String readSensor(String sensorType) {
		Object read = _deviceSensorManager.readSensor(sensorType);
		String jsonRead = gson.toJson(read);
		return jsonRead;
	}
	
	@JavascriptInterface
	public int getDeviceNumber() {
		return _deviceNumber;
	}
	
	@JavascriptInterface
	public int getDevicesCount() {
		return _orderedDevices.length;
	}
	
	@JavascriptInterface
	public String getTaskId() {
		return _taskInfo.getTaskId();
	}
	
	@JavascriptInterface
	public String sendDataToServer(final String url, final String data) {
		_networkThread.ScheduleRequest(url, data, "post");		
		return null;
	}

	@JavascriptInterface
	public void finalizeTask(String json) {
		_taskInfo.setResult(json);
		_deviceSensorManager.tearDownSensors();
		_mainActivity.runOnUiThread(new Runnable(){
			public void run() {
				_mainActivity.setCurrentRunningTask(null, null, null);	
				MainActivity.TaskHandler.updateCompletedLocalTasksList();
			}
		});
	}

}
