package pl.edu.agh.dkala_msc.infrastructure;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import pl.edu.agh.dkala_msc.device.DeviceInformation;

public class TaskInfo {
		
	public static final String TASK_CREATED = "created";
	public static final String TASK_READY = "ready";
	public static final String TASK_COMPLETED = "completed";

	private Map<Integer, LinkedList<String>> _messages; 
	
	private String rootTaskId;

	private String taskName;
	
	private String jsCode;
	
	private String status;
	
	private String result;
	
	private String[] requiredSensors;
	
	private Set<String> unreadyNeighbors;
	
	protected List<DeviceInformation> remoteParticipants;
	
	protected DeviceInformation currentDevice;
	
	protected DeviceInformation[] orderedDevices;
	
	public TaskInfo(String rootTaskId, String taskName, String jsCode, Set<String> neighbors, String[] sensors) {
		this.unreadyNeighbors = neighbors;
		this.jsCode = jsCode;
		this.taskName = taskName;
		this.rootTaskId = rootTaskId;
		this.requiredSensors = sensors;
		this.remoteParticipants = new LinkedList<DeviceInformation>();
		
		if (neighbors.size() == 0) {
			this.status = TASK_READY;
		} else {
			this.status = TASK_CREATED;
		}
	}
	
	public String getJsCode() {
		return jsCode;
	}
	
	public String getTaskName() {
		return taskName;
	}	
	
	public String getTaskId() {
		return rootTaskId;
	}
	
	public String getStatus() {
		return status;
	}
	
	public String getResult() {
		return result;
	}
	
	public String[] getRequiredSensors() {
		return requiredSensors;
	}
	
	public DeviceInformation[] getAllParticipants() {
		DeviceInformation[] participantsTable;
		if (currentDevice != null) {
			participantsTable = remoteParticipants.toArray(
				new DeviceInformation[remoteParticipants.size() + 1]
			);
			participantsTable[remoteParticipants.size()] = currentDevice;
		return participantsTable;
		} else {
			participantsTable = remoteParticipants.toArray(
				new DeviceInformation[remoteParticipants.size()]
			);
		}
		
		return participantsTable;
	}
	
	public void setResult (String result) {
		this.result = result;
		this.status = TASK_COMPLETED;
	}
	
	public void setCurrentDevice(DeviceInformation currentDevice) {
		if (currentDevice == null) return;
		this.currentDevice = currentDevice;
	}
	
	public DeviceInformation getCurrentDevice() {
		return currentDevice;
	}
	
	public void addParticipants(String neighborIp, DeviceInformation[] participants) {
		if (!unreadyNeighbors.contains(neighborIp)) {
			return;
		}
		
		unreadyNeighbors.remove(neighborIp);	
		for (DeviceInformation participant : participants)
			this.remoteParticipants.add(participant);
		
		if (unreadyNeighbors.size() == 0) {
			this.status = TASK_READY;
		}
	}
	
	public DeviceInformation[] getOrderedDevices() {
		return orderedDevices;
	}
	
	public void enqueueMessage(String message, int sender) {
		InitializeMessages();		
		LinkedList<String> queue = _messages.get(sender);
		queue.push(message);
	}
	
	public String dequeueMessage(int sender) {
		InitializeMessages();		
		LinkedList<String> queue = _messages.get(sender);
		if (queue.size() != 0) {
			String message = queue.pop();
			return message;
		} else {
			return null;
		}
	}
	
	private void InitializeMessages() {
		if (_messages != null) {
			return;
		}
		
		_messages = new TreeMap<Integer, LinkedList<String>>();
		for (int deviceNumber = 0; deviceNumber < orderedDevices.length; deviceNumber++) {
			_messages.put(deviceNumber, new LinkedList<String>());
		}
	}
}
