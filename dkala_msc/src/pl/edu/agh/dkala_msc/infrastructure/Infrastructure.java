package pl.edu.agh.dkala_msc.infrastructure;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.device.DeviceInformation;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.internal.requests.ConnectDeviceRequest;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;

public class Infrastructure {

	public static final int MAX_CHILDREN = 5;

	private List<DeviceInformation> _children = new LinkedList<DeviceInformation>();
	private Map<String, Integer> _childrenCount = new TreeMap<String, Integer>();
	private DeviceInformation _currentDevice;
	private DeviceInformation _rootDevice = null;
	private NetworkThread _networkThread;
	private MainActivity _context;
	
	private int _allChildrenCount = 0;

	public void connectCurrentDeviceToCloud(String address) {
		ConnectDeviceRequest connectDevice = new ConnectDeviceRequest();
		connectDevice.GoToRoot = true;
		connectDevice.DeviceInformation = getCurrentDevice();

		String uri = "http://" + address + ":8080/connect";
		_networkThread.ScheduleRequest(uri, connectDevice, "post");
	}

	public void setNeighbourCountInformation() {
		final int neighborCount = _children.size() + (_rootDevice == null ? 0 : 1);
		_context.runOnUiThread(new Runnable() {
			public void run() {
				try {
				_context.setNeighbourCountInformation(
						_rootDevice == null ? "no direct root"
								: _rootDevice.IPAddress, neighborCount);
				} catch (Exception e) {
					_context.setErrorInformation(e.getLocalizedMessage());
				}
			}
		});
	}

	public void reset() {
		_children = new LinkedList<DeviceInformation>();
		_childrenCount = new TreeMap<String, Integer>();
		_allChildrenCount = 0;
		_rootDevice = null;
		
		setNeighbourCountInformation();
		//System.gc();
	}
	
	public void setRootDevice(DeviceInformation deviceInformation) {
		_rootDevice = deviceInformation;
		setNeighbourCountInformation();
	}
	
	public void updateChildrenCount(String deviceIp, int childCount) {
		int prevChildCount = 0;
		if (_childrenCount.containsKey(deviceIp)) {
			prevChildCount = _childrenCount.get(deviceIp);
		}
		
		_allChildrenCount += childCount - prevChildCount;
		_childrenCount.put(deviceIp, childCount);
	}

	public boolean addChildDevice(DeviceInformation deviceInformation) {
		if (_children.size() >= MAX_CHILDREN) {
			return false; 
		}

		_allChildrenCount++;
		_children.add(deviceInformation);
		_childrenCount.put(deviceInformation.IPAddress, 0);
		
		setNeighbourCountInformation();
		return true;
	}
	
	public boolean canAddChildDevice() {
		return _children.size() < MAX_CHILDREN;
	}
	
	public DeviceInformation[] getAllNeighbors() {		
		if (_rootDevice == null) {
			return _children.toArray(new DeviceInformation[_children.size()]);
		}
		
		DeviceInformation[] neighbors = new DeviceInformation[_children.size() + 1];
		_children.toArray(neighbors);
		neighbors[_children.size()] = _rootDevice;
		
		return neighbors;
	}
	
	public DeviceInformation getNextChildDeviceForConnection() {
		DeviceInformation bestChild = null;
		int lowestChildCount = 0;
		
		for (DeviceInformation child : _children) {
			if (bestChild == null) {
				bestChild = child;
				lowestChildCount = _childrenCount.get(child.IPAddress);
				
				continue;
			}
			
			int currentChildCount = _childrenCount.get(child.IPAddress);
			if (currentChildCount < lowestChildCount) {
				bestChild = child;
				lowestChildCount = currentChildCount;
			}
		}
		
		return bestChild;
	}
	
	public int getAllChildrenCount() {
		return _allChildrenCount;
	}
	
	public List<DeviceInformation> getChildrenDevices() {
		return _children;
	}

	public DeviceInformation getRootDevice() {
		return _rootDevice;
	}
	
	public DeviceInformation getCurrentDevice() {
		return _currentDevice;
	}
	
	public Infrastructure(MainActivity context, NetworkThread networkThread) {
		_context = context;
		_networkThread = networkThread;
		_currentDevice = createCurrentDeviceInformation();
	}
	
	private DeviceInformation createCurrentDeviceInformation() {
		DeviceInformation currentDevice = new DeviceInformation();
		
		try {
			currentDevice.IPAddress = getCurrentDeviceIpAddress();
			currentDevice.Model = getDeviceName();
		} catch (SocketException e) {
			e.printStackTrace();
		}
		
		return currentDevice;
	}
	
	private String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}


	private String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}

	private String getCurrentDeviceIpAddress() throws SocketException {
		Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
		while (en.hasMoreElements()) {
            NetworkInterface intf = en.nextElement();
            Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses();
            while (enumIpAddr.hasMoreElements()) {
                InetAddress inetAddress = enumIpAddr.nextElement();
                NetworkInterface netInterface = NetworkInterface.getByInetAddress(inetAddress);
                String interfaceName = netInterface.getName();
                if (!inetAddress.isLoopbackAddress()) {
                    String ip = inetAddress.getHostAddress().toString();
                    if ((interfaceName.equals("eth0") || interfaceName.equals("wlan0")) && ip.contains(".")) {
                    	return ip;
                    }
                }
            }
        }
		return null;
	}

//	private String getCurrentDeviceIpAddress() {
//		WifiManager wifiManager = (WifiManager) _context.getSystemService(Context.WIFI_SERVICE);
//		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
//		int ip = wifiInfo.getIpAddress();
//		String ipAddress = Formatter.formatIpAddress(ip);
//		
//		return ipAddress;
//	}

}
