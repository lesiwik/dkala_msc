package pl.edu.agh.dkala_msc.infrastructure;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.device.DeviceInformation;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.internal.requests.DeviceMessageRequest;
import pl.edu.agh.dkala_msc.httpd.internal.requests.RemoteTaskReadyRequest;
import pl.edu.agh.dkala_msc.httpd.internal.requests.ScheduleTaskExecutionRequest;
import pl.edu.agh.dkala_msc.httpd.internal.requests.StartTaskRequest;
import pl.edu.agh.dkala_msc.javascript.JavascriptExecutor;
import android.webkit.WebView;

public class TaskHandler {

	private Map<String, LocalTaskInfo> _localTasksInformation = new TreeMap<String, LocalTaskInfo>();
	private Map<String, RemoteTaskInfo> _remoteTasksInformation = new TreeMap<String, RemoteTaskInfo>();
	private TaskInfo _currentTask;

	private DeviceSensorManager _deviceSensorManager;
	private Infrastructure _infrastructure;
	private NetworkThread _networkThread;
	private MainActivity _mainActivity;
	private WebView _webView;

	public TaskHandler(DeviceSensorManager deviceSensorManager,
			Infrastructure infrastructure, NetworkThread networkThread,
			MainActivity mainActivity, WebView webView) {
		_deviceSensorManager = deviceSensorManager;
		_infrastructure = infrastructure;
		_networkThread = networkThread;
		_mainActivity = mainActivity;
		_webView = webView;
	}

	public void reset() {
		_localTasksInformation = new TreeMap<String, LocalTaskInfo>();
		_remoteTasksInformation = new TreeMap<String, RemoteTaskInfo>();
		_currentTask = null;
		
		_deviceSensorManager.tearDownSensors();
		setCurrentRunningTask();
	}

	public String scheduleTaskExecution(String taskName, String taskJsCode,
			String[] requiredSensors) {
		String taskId = saveLocalTask(taskName, taskJsCode, requiredSensors);
		final ScheduleTaskExecutionRequest scheduleTaskRequest = createScheduleTaskRequest(
				taskId, taskName, taskJsCode, requiredSensors);

		DeviceInformation[] neighbors = _infrastructure.getAllNeighbors();
		for (DeviceInformation neighbor : neighbors) {
			propagateTaskRequest(scheduleTaskRequest, neighbor);
		}

		return taskId;
	}

	private String saveLocalTask(String taskName, String taskJsCode,
			String[] requiredSensors) {
		DeviceInformation currentDevice = _infrastructure.getCurrentDevice();
		Set<String> neighbors = getNeighborsIp(null);
		String rootTaskId = createTaskId();

		LocalTaskInfo localTaskInfo = new LocalTaskInfo(rootTaskId, taskName,
				taskJsCode, neighbors, requiredSensors);
		localTaskInfo.setCurrentDevice(currentDevice);

		_localTasksInformation.put(rootTaskId, localTaskInfo);

		if (localTaskInfo.getStatus().equals(TaskInfo.TASK_READY)) {
			onLocalTaskReady(localTaskInfo);
		}

		return rootTaskId;
	}

	private ScheduleTaskExecutionRequest createScheduleTaskRequest(
			String rootTaskId, String taskName, String taskJsCode,
			String[] requiredSensors) {
		ScheduleTaskExecutionRequest scheduleTaskRequest = new ScheduleTaskExecutionRequest();

		scheduleTaskRequest.RootDevice = _infrastructure.getCurrentDevice();
		scheduleTaskRequest.TaskId = rootTaskId;
		scheduleTaskRequest.TaskName = taskName;
		scheduleTaskRequest.TaskJsCode = taskJsCode;
		scheduleTaskRequest.RequiredSensors = requiredSensors;

		return scheduleTaskRequest;
	}

	private void propagateTaskRequest(
			ScheduleTaskExecutionRequest scheduleTaskRequest,
			DeviceInformation scheduleDirection) {
		String address = scheduleDirection.IPAddress;
		String uri = "http://" + address + ":8080/tasks";
		_networkThread.ScheduleRequest(uri, scheduleTaskRequest, "post");
	}

	private String createTaskId() {
		String taskId = UUID.randomUUID().toString();
		return taskId;
	}
	
	public void saveRemoteTask(ScheduleTaskExecutionRequest taskExecutionRequest) {
		DeviceInformation currentDevice = _infrastructure.getCurrentDevice();
		String rootDeviceIp = taskExecutionRequest.RootDevice.IPAddress;
		String taskId = taskExecutionRequest.TaskId;
		String taskName = taskExecutionRequest.TaskName;
		String jsCode = taskExecutionRequest.TaskJsCode;
		String[] sensors = taskExecutionRequest.RequiredSensors;
		Set<String> neighbors = getNeighborsIp(rootDeviceIp);
		
		RemoteTaskInfo remoteTaskInfo = new RemoteTaskInfo(
			taskId, taskName, jsCode, neighbors, sensors, rootDeviceIp
		);
		if (_deviceSensorManager.areAllSensorsSupported(taskExecutionRequest.RequiredSensors)) {
			remoteTaskInfo.setCurrentDevice(currentDevice);
		}

		_remoteTasksInformation.put(taskId, remoteTaskInfo);
		
		if (remoteTaskInfo.getStatus().equals(TaskInfo.TASK_READY)) {
			onRemoteTaskReady(remoteTaskInfo);
		}
	}

	private Set<String> getNeighborsIp (String ipToExclude) {
		HashSet<String> neighbors = new HashSet<String>();
		for (DeviceInformation neighbor : _infrastructure.getAllNeighbors()) {
			if (neighbor.IPAddress.equals(ipToExclude)) continue;
			neighbors.add(neighbor.IPAddress);
		}
		
		return neighbors;
	}
	
	public void updateTaskRequest(RemoteTaskReadyRequest remoteTaskReadyRequest) {
		String taskId = remoteTaskReadyRequest.TaskId;
		String neighborIp = remoteTaskReadyRequest.SenderIp;
		DeviceInformation[] participants = remoteTaskReadyRequest.ReadyDevices;
		
		if (_remoteTasksInformation.containsKey(taskId)) {
			RemoteTaskInfo remoteTaskInfo = _remoteTasksInformation.get(taskId);
			remoteTaskInfo.addParticipants(neighborIp, participants);
			
			if (remoteTaskInfo.getStatus().equals(TaskInfo.TASK_READY)) {
				onRemoteTaskReady(remoteTaskInfo);
			}
		} else {
			LocalTaskInfo localTaskInfo = _localTasksInformation.get(taskId);
			localTaskInfo.addParticipants(neighborIp, participants);
			
			if (localTaskInfo.getStatus().equals(TaskInfo.TASK_READY)) {
				onLocalTaskReady(localTaskInfo);
			}
		}
	}
	
	private void onLocalTaskReady(final LocalTaskInfo localTaskInfo) {
		localTaskInfo.orderDevices();
		propagateTaskStart(localTaskInfo);
		runTask(localTaskInfo);
	}
	
	private void onRemoteTaskReady(final RemoteTaskInfo remoteTaskInfo) {
		RemoteTaskReadyRequest remoteTaskReadyRequest = new RemoteTaskReadyRequest();
		remoteTaskReadyRequest.TaskId = remoteTaskInfo.getTaskId();
		remoteTaskReadyRequest.ReadyDevices = remoteTaskInfo.getAllParticipants();
		remoteTaskReadyRequest.SenderIp = _infrastructure.getCurrentDevice().IPAddress;
		
		String uri = "http://" + remoteTaskInfo.getRootDeviceIp() + ":8080/tasks";
		_networkThread.ScheduleRequest(uri, remoteTaskReadyRequest, "put");
	}
	
	public TaskInfo getTaskInfo(String taskId) {
		TaskInfo taskInfo;
		if (_localTasksInformation.containsKey(taskId)) {
			taskInfo = _localTasksInformation.get(taskId);
		} else {
			taskInfo = _remoteTasksInformation.get(taskId);
		}
		return taskInfo;
	}

	public void startTask(StartTaskRequest startTaskRequest) {
		String taskId = startTaskRequest.TaskId;
		DeviceInformation[] orderedDevices = startTaskRequest.OrderedDevices;
		
		RemoteTaskInfo remoteTaskInfo = _remoteTasksInformation.get(taskId);
		remoteTaskInfo.setOrderedDevices(orderedDevices);

		propagateTaskStart(remoteTaskInfo);
		runTask(remoteTaskInfo);
	}
	
	private void propagateTaskStart(TaskInfo taskInfo) {
		DeviceInformation[] devicesInformation = taskInfo.getOrderedDevices();
		String currentDeviceIp = _infrastructure.getCurrentDevice().IPAddress;
		int currentDeviceIndex = 0;
		for (int index = 0; index < devicesInformation.length; index++) {
			if (devicesInformation[index].IPAddress.equals(currentDeviceIp)) {
				currentDeviceIndex = index;
				break;
			}
		}
		
		int firstNeighborIndex = currentDeviceIndex * 2 + 1;
		int secondNeighborIndex = currentDeviceIndex * 2 + 2;
		
		StartTaskRequest startTaskRequest = new StartTaskRequest();
		startTaskRequest.OrderedDevices = taskInfo.getOrderedDevices();
		startTaskRequest.TaskId = taskInfo.getTaskId();
		
		if (firstNeighborIndex < devicesInformation.length) {
			String url = "http://" + devicesInformation[firstNeighborIndex].IPAddress + ":8080/runningTasks";
			_networkThread.ScheduleRequest(url, startTaskRequest, "post");
		}
		
		if (secondNeighborIndex < devicesInformation.length) {
			String url = "http://" + devicesInformation[secondNeighborIndex].IPAddress + ":8080/runningTasks";
			_networkThread.ScheduleRequest(url, startTaskRequest, "post");
		}
	}
	
	private void runTask(TaskInfo taskInfo) {
		_currentTask = taskInfo;
		
		final String summary = 
				"<html><head><script type=\"text/javascript\">" +
					"PlatformTools = (function () {" +
					"	var senderCallbackMapping = {};" +
					"	var peekLoop = function() {" +
					"		Object.keys(senderCallbackMapping).forEach(function(key) {" +
					"			var sender = Number(key);" +
					"			var callback = senderCallbackMapping[sender];" +
					"		    var message = Platform.receiveFromDevice(sender);" +
					"		    if (message) {" +
					"			    callback(JSON.parse(message));" +
					"				delete senderCallbackMapping[sender];" +
					"		    }" +
					"		});" +
					"	    setTimeout(peekLoop, 25);" +
					"	};" +
					"	setTimeout(peekLoop, 25);" +
					"" +
					"	return {" +
					"		onReceive: function(sender, callback) {" +
					"			senderCallbackMapping[sender] = callback;" +
					"		}," +
					"" +
					"		sendTo: function(message, receiver) {" +
					"			Platform.sendToDevice(JSON.stringify(message), receiver);" +
					"		}," +
					"" +
					"		readSensor: function(sensorType) {" +
					"			var read = Platform.readSensor(sensorType);" +
					"			return JSON.parse(read);" +
					"		}," +
					"" +
					"		sendDataToServer: function(uri, data) {" +
					"			var jsonData = JSON.stringify(data);" +
					"			Platform.sendDataToServer(uri, jsonData);" +
					"		}," +
					"" +
					"		finalize: function(result) {" +
					"			Platform.finalizeTask(JSON.stringify(result));" +
					"		}" +
					"	};" +
					"}());" +
					"" 
					+ taskInfo.getJsCode() +
			    "</script></head><body></body></html>";
		
		final TaskInfo currentTaskLocal = _currentTask;
		Runnable startTaskThread = new Thread() {
			public void run() {
				if (currentTaskLocal == null) {
					return;
				}
				_deviceSensorManager.setUpSensors(currentTaskLocal);
				JavascriptExecutor javascriptInterface = new JavascriptExecutor(
						currentTaskLocal, _deviceSensorManager, _mainActivity,
						_networkThread);
				_webView.addJavascriptInterface(javascriptInterface, "Platform");
				_webView.loadData(summary, "text/html", null);
			}
		};
		_mainActivity.runOnUiThread(startTaskThread);
		
		setCurrentRunningTask();
	}

	public void saveMessage(DeviceMessageRequest deviceMessageRequest) {
		TaskInfo taskInfo;
		String taskId = deviceMessageRequest.TaskId;
		if (_localTasksInformation.containsKey(taskId)) {
			taskInfo = _localTasksInformation.get(taskId);
		} else if (_remoteTasksInformation.containsKey(taskId)) {
			taskInfo = _remoteTasksInformation.get(taskId);
		} else {
			return;
		}
		
		String message = deviceMessageRequest.Message;
		int sender = deviceMessageRequest.Sender;
		taskInfo.enqueueMessage(message, sender);
	}
	
	public void setCurrentRunningTask() {
		final TaskInfo currentTaskLocal = _currentTask;
		_mainActivity.runOnUiThread(new Runnable() {
			public void run() {
				try {
					String taskName = null;
					String taskRootIp = null;
					String taskId = null;
					
					if (currentTaskLocal != null && currentTaskLocal.getStatus() == TaskInfo.TASK_READY) {
						taskName = currentTaskLocal.getTaskName();
						taskRootIp = currentTaskLocal.getAllParticipants()[0].IPAddress;
						taskId = currentTaskLocal.getTaskId();
					}

					_mainActivity.setCurrentRunningTask(taskName, taskRootIp, taskId);
				} catch (Exception e) {
					_mainActivity.setErrorInformation(e.getLocalizedMessage());
				}
			}
		});
	}

	public void updateCompletedLocalTasksList() {
		final Map<String, LocalTaskInfo> completedLocalTasks = new HashMap<String, LocalTaskInfo>();
		Iterator<Entry<String, LocalTaskInfo>> it = _localTasksInformation
				.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, LocalTaskInfo> pairs = it.next();

			String taskId = pairs.getKey();
			LocalTaskInfo task = pairs.getValue();

			if (task.getStatus() == TaskInfo.TASK_COMPLETED) {
				completedLocalTasks.put(taskId, task);
			}
		}

		_mainActivity.runOnUiThread(new Runnable() {
			public void run() {
				_mainActivity.updateCompletedLocalTasksList(completedLocalTasks);
			}
		});
	}
	
}
