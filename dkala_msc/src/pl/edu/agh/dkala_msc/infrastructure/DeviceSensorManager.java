package pl.edu.agh.dkala_msc.infrastructure;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import pl.edu.agh.dkala_msc.infrastructure.sensors.ISensorManager;

public class DeviceSensorManager {
	
	private Map<String, ISensorManager> _sensorManagers = new TreeMap<String, ISensorManager>();
	
	public DeviceSensorManager (ISensorManager[] sensorManagers) {
		if (sensorManagers == null) {
			return;
		}
		
		for (ISensorManager sensorManager : sensorManagers) {
			_sensorManagers.put(sensorManager.getSensorName(), sensorManager);
		}
	}

	public boolean areAllSensorsSupported (String[] requiredSensors) {
		if (requiredSensors == null) {
			return true;
		}
	
		for (String sensor : requiredSensors) {
			if (!_sensorManagers.containsKey(sensor)) {
				return false;
			}
		}
		
		return true;
	}

	public Object readSensor(String sensorType) {
		if (!_sensorManagers.containsKey(sensorType)) return null;
		
		Object read = _sensorManagers.get(sensorType).readSensor();
		return read;
	}
	
	public void setUpSensors(TaskInfo currentTask) {
		String[] requiredSensors = currentTask.getRequiredSensors();
		if (requiredSensors == null) return;
		for (String sensor : requiredSensors) {
			if (!_sensorManagers.containsKey(sensor)) continue;
			_sensorManagers.get(sensor).setUp();
		}
	}
	
	public void tearDownSensors() {
		for (Entry<String, ISensorManager> entry : _sensorManagers.entrySet()) {
			entry.getValue().tearDown();
		}
	}
	
}
