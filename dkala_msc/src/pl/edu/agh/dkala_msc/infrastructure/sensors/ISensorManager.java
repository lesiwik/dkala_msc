package pl.edu.agh.dkala_msc.infrastructure.sensors;

public interface ISensorManager {

	String getSensorName();
	
	Object readSensor();
	
	void setUp();
	
	void tearDown();
	
}
