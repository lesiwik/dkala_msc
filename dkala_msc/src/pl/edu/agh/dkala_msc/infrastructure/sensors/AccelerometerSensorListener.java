package pl.edu.agh.dkala_msc.infrastructure.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

public class AccelerometerSensorListener implements SensorEventListener {
	
	private float lastReading[];

	
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
	    if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) return;
	    lastReading = event.values;
	}

	public float[] getLastReading() {
		return lastReading;
	}
	
}
