package pl.edu.agh.dkala_msc.infrastructure.sensors;

import android.hardware.Sensor;
import android.hardware.SensorManager;

public class AccelerometerSensorManager implements ISensorManager {

	private SensorManager _sensorManager;
	
	private boolean _isListenerRegistered;
	private AccelerometerSensorListener _sensor;
	
	public AccelerometerSensorManager(SensorManager sensorManager) {
		_sensorManager = sensorManager;
		
		_sensor = new AccelerometerSensorListener();
	}
	
	@Override
	public String getSensorName() {
		return "accelerometer";
	}

	@Override
	public Object readSensor() {
		if (!_isListenerRegistered) return null;
		
		Object read = _sensor.getLastReading();
		return read;
	}

	@Override
	public void setUp() {
		Sensor accelerometer = _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		int delayType = SensorManager.SENSOR_DELAY_UI;
	    _sensorManager.registerListener(_sensor, accelerometer, delayType);
	    
	    _isListenerRegistered = true;
	}

	@Override
	public void tearDown() {
		if (!_isListenerRegistered) return;
		
		_sensorManager.unregisterListener(_sensor);
		_isListenerRegistered = false;
	}

}
