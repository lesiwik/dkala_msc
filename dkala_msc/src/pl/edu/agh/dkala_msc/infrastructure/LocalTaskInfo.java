package pl.edu.agh.dkala_msc.infrastructure;

import java.util.Set;

import pl.edu.agh.dkala_msc.device.DeviceInformation;

public class LocalTaskInfo extends TaskInfo {

	public LocalTaskInfo(String rootTaskId, String taskName, String jsCode, Set<String> neighbors, String[] requiredSensors) {
		super(rootTaskId, taskName, jsCode, neighbors, requiredSensors);
	}
	
	public void orderDevices() {
		DeviceInformation[] orderedDevices = new DeviceInformation[remoteParticipants.size() + 1];
		orderedDevices[0] = currentDevice;
		int deviceNumber = 1;
		for (DeviceInformation deviceInformation : remoteParticipants) {
			orderedDevices[deviceNumber++] = deviceInformation;
		}
		
		this.orderedDevices = orderedDevices;
	}

}
