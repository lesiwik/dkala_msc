package pl.edu.agh.dkala_msc.infrastructure;

import java.util.Set;

import pl.edu.agh.dkala_msc.device.DeviceInformation;

public class RemoteTaskInfo extends TaskInfo {

	private String rootDeviceIp;
	
	public RemoteTaskInfo(String id, String taskName, String jsCode, Set<String> neighbors, String[] sensors, String rootDeviceIp) {
		super(id, taskName, jsCode, neighbors, sensors);
		
		this.rootDeviceIp = rootDeviceIp;  
	}
	
	public String getRootDeviceIp() {
		return rootDeviceIp;
	}

	public void setOrderedDevices(DeviceInformation[] orderedDevices) {
		this.orderedDevices = orderedDevices;
	}

}
