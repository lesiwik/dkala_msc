package pl.edu.agh.dkala_msc;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import pl.edu.agh.dkala_msc.gui.SdTaskProvider;
import pl.edu.agh.dkala_msc.gui.fragments.ConnectToCloudFragment;
import pl.edu.agh.dkala_msc.gui.fragments.CurrentStateFragment;
import pl.edu.agh.dkala_msc.gui.fragments.TaskHandlingFragment;
import pl.edu.agh.dkala_msc.httpd.HttpServer;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.infrastructure.DeviceSensorManager;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.LocalTaskInfo;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;
import pl.edu.agh.dkala_msc.infrastructure.sensors.AccelerometerSensorManager;
import pl.edu.agh.dkala_msc.infrastructure.sensors.ISensorManager;
import pl.edu.agh.dkala_msc.infrastructure.sensors.LightSensorManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

public class MainActivity extends ActionBarActivity {

	public static Infrastructure Infrastructure;
	public static TaskHandler TaskHandler;
	public static SdTaskProvider SdTaskProvider;
	public static MainActivity MainActivity;

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	private boolean _isInitialized = false;

	public void setNeighbourCountInformation(String rootIp, int neighborCount) {
		if (mSectionsPagerAdapter.CurrentState != null) {
			mSectionsPagerAdapter.CurrentState.setNeighbourCountInformation(
					rootIp, neighborCount);
		}
		if (mSectionsPagerAdapter.ConnectToCloud != null) {
			mSectionsPagerAdapter.ConnectToCloud.setNeighbourCountInformation(
					rootIp, neighborCount);
		}
	}

	public void setCurrentRunningTask(String taskName, String taskRootIp,
			String taskId) {
		if (mSectionsPagerAdapter.CurrentState != null) {
			mSectionsPagerAdapter.CurrentState.setCurrentRunningTask(taskName,
					taskRootIp, taskId);
		}
		if (mSectionsPagerAdapter.TaskHandling != null) {
			mSectionsPagerAdapter.TaskHandling.setCurrentRunningTask(taskName,
					taskRootIp, taskId);
		}
	}

	public void setErrorInformation(String errorMessage) {
		if (mSectionsPagerAdapter.CurrentState != null) {
			mSectionsPagerAdapter.CurrentState
					.setErrorInformation(errorMessage);
		}
	}

	public void updateCompletedLocalTasksList(
			Map<String, LocalTaskInfo> completedLocalTasks) {
		if (mSectionsPagerAdapter.TaskHandling != null) {
			mSectionsPagerAdapter.TaskHandling
					.updateCompletedLocalTasksList(completedLocalTasks);
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		MainActivity = this;

		if (_isInitialized)
			return;
		_isInitialized = true;

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		WebView webView = startWebApplication();
		NetworkThread networkThread = startNetworkThread();
		Infrastructure infrastructure = startInfrastructure(networkThread);
		DeviceSensorManager deviceSensorManager = startDeviceSensorManager();
		TaskHandler taskHandler = startTaskHandler(deviceSensorManager,
				infrastructure, networkThread, webView);
		startHttpdServer(infrastructure, networkThread, taskHandler, webView);

		Infrastructure = infrastructure;
		TaskHandler = taskHandler;
		SdTaskProvider = new SdTaskProvider();
		SdTaskProvider.Initialize();

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
	}

	private Infrastructure startInfrastructure(NetworkThread networkThread) {
		Infrastructure infrastructure = new Infrastructure(this, networkThread);
		return infrastructure;
	}

	private DeviceSensorManager startDeviceSensorManager() {
		List<ISensorManager> sensorManagers = new LinkedList<ISensorManager>();
		SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

		Sensor accelerometerSensor = sensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		if (accelerometerSensor != null) {
			sensorManagers.add(new AccelerometerSensorManager(sensorManager));
		}

		Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		if (lightSensor != null) {
			sensorManagers.add(new LightSensorManager(lightSensor));
		}

		ISensorManager[] supportedSensors = sensorManagers
				.toArray(new ISensorManager[sensorManagers.size()]);
		DeviceSensorManager deviceSensorManager = new DeviceSensorManager(
				supportedSensors);
		return deviceSensorManager;
	}

	private NetworkThread startNetworkThread() {
		NetworkThread networkThread = new NetworkThread();
		networkThread.start();
		return networkThread;
	}

	private TaskHandler startTaskHandler(
			DeviceSensorManager deviceSensorManager,
			Infrastructure infrastructure, NetworkThread networkThread,
			WebView webView) {
		TaskHandler taskHandler = new TaskHandler(deviceSensorManager,
				infrastructure, networkThread, this, webView);
		return taskHandler;
	}

	@SuppressLint("SetJavaScriptEnabled")
	private WebView startWebApplication() {
		WebView webView = new WebView(this);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebChromeClient(new WebChromeClient() {
			public boolean onConsoleMessage(ConsoleMessage cm) {
				final String errorMessage = cm.message() + cm.lineNumber();
				MainActivity.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						MainActivity.setErrorInformation(errorMessage);
					}

				});
				return true;
			}
		});

		return webView;
	}

	private void startHttpdServer(Infrastructure infrastructure, NetworkThread networkThread, 
								  TaskHandler taskHandler, WebView webView) {
		HttpServer httpServer = new HttpServer(infrastructure, networkThread, this, taskHandler, webView);

		try {
			httpServer.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public CurrentStateFragment CurrentState;
		public ConnectToCloudFragment ConnectToCloud;
		public TaskHandlingFragment TaskHandling;

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {

			switch (position) {
			case 0:
				CurrentState = CurrentStateFragment.newInstance(0);
				return CurrentState;
			case 1:
				ConnectToCloud = ConnectToCloudFragment.newInstance(1);
				return ConnectToCloud;
			case 2:
				TaskHandling = TaskHandlingFragment.newInstance(2);
				return TaskHandling;
			}
			return null;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}

}
