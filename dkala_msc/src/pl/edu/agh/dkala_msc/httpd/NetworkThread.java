package pl.edu.agh.dkala_msc.httpd;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.springframework.web.client.RestTemplate;

import pl.edu.agh.dkala_msc.MainActivity;

public class NetworkThread extends Thread {

	private ConcurrentLinkedQueue<Request> requestQueue = new ConcurrentLinkedQueue<Request>();

	@Override
	public void run() {
		while (true) {
			Request request;
			synchronized(requestQueue){
				while (requestQueue.isEmpty()) {
					try {
						requestQueue.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				request = requestQueue.remove();
			}
			for (int tryNumber = 0; tryNumber < 3; tryNumber++) {
				try {
					try {
						if (request.method.toLowerCase().equals("post")){
							new RestTemplate().postForLocation(request.url, request.data);	
						} else if (request.method.toLowerCase().equals("put")) {
							new RestTemplate().put(request.url, request.data);
						}
						break;
					} catch (final Exception e) {
						MainActivity.MainActivity.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								MainActivity.MainActivity.setErrorInformation(e
										.getMessage());
							}

						});
					}
				} catch (Exception e) {
				}
			}
		}
	}

	public void ScheduleRequest(String url, Object message, String method) {
		Request request = new Request();
		request.method = method;
		request.data = message;
		request.url = url;
		
		Schedule(request);
	}
	
	public void Send(Request request) {
		Schedule(request);
	}
	
	private void Schedule(Request request) {
		synchronized(requestQueue){
			requestQueue.add(request);
			requestQueue.notify();
		}
	}

}
