package pl.edu.agh.dkala_msc.httpd;


public interface IHttpRequestHandler {

	boolean canHandleRequest(RequestInformation requestInformation);
	
	String handleRequest (RequestInformation requestInformation, String requestBody);
	
}
