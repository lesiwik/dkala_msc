package pl.edu.agh.dkala_msc.httpd.internal.handlers;

import android.webkit.WebView;

import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.device.DeviceInformation;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.internal.requests.ConnectDeviceRequest;
import pl.edu.agh.dkala_msc.httpd.internal.requests.SaveDeviceConnectionRequest;
import pl.edu.agh.dkala_msc.httpd.internal.requests.UpdateChildrenCountRequest;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;

public class ConnectAnotherDeviceToCloud extends HttpRequestHandlerBase {

	public ConnectAnotherDeviceToCloud(Infrastructure infrastructure, NetworkThread networkThread, 
									   MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.equals("connect");
		boolean isMethodOk = requestInformation.Method.equals("POST");
		
		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation, String requestBody) {
		final ConnectDeviceRequest connectDevice = gson.fromJson(requestBody, ConnectDeviceRequest.class);

		if (connectDevice.GoToRoot && infrastructure.getRootDevice() != null) {
			delegateToRoot(connectDevice);
		} else if (infrastructure.canAddChildDevice()){
			connectLocally(connectDevice);
		} else {
			delegateToChild(connectDevice);
		}
		
		return null;
	}
	
	private void delegateToRoot(ConnectDeviceRequest connectDevice) {
		String address = infrastructure.getRootDevice().IPAddress;
		String url = "http://" + address + ":8080/connect";
		
		networkThread.ScheduleRequest(url, connectDevice, "post");
	}
	
	private void connectLocally(ConnectDeviceRequest connectDevice) {
		infrastructure.addChildDevice(connectDevice.DeviceInformation);
		sendConnectionEstablishedRequest(connectDevice);
		updateAllChildrenCounter();
	}
	
	private void sendConnectionEstablishedRequest(ConnectDeviceRequest connectDevice) {
		SaveDeviceConnectionRequest request = new SaveDeviceConnectionRequest();
		request.DeviceInformation = infrastructure.getCurrentDevice();
		
		String address = connectDevice.DeviceInformation.IPAddress;
		String url = "http://" + address + ":8080/connection";
		
		networkThread.ScheduleRequest(url, request, "post");
	}
	
	private void updateAllChildrenCounter() {
		DeviceInformation rootDevice = infrastructure.getRootDevice();
		if (rootDevice == null) return;
		
		UpdateChildrenCountRequest request = new UpdateChildrenCountRequest();
		request.DeviceIp = infrastructure.getCurrentDevice().IPAddress;
		request.AllChildrenCount = infrastructure.getAllChildrenCount();

		String url = "http://" + rootDevice.IPAddress + ":8080/children/count";
		networkThread.ScheduleRequest(url, request, "put");
	}
	
	private void delegateToChild(ConnectDeviceRequest connectDevice) {
		connectDevice.GoToRoot = false;
		DeviceInformation childForDelegation = infrastructure.getNextChildDeviceForConnection();

		String address = childForDelegation.IPAddress;
		String url = "http://" + address + ":8080/connect";
		
		networkThread.ScheduleRequest(url, connectDevice, "post");
	}

}
