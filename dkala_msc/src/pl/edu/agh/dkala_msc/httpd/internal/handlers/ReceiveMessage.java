package pl.edu.agh.dkala_msc.httpd.internal.handlers;

import android.webkit.WebView;
import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.internal.requests.DeviceMessageRequest;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;

public class ReceiveMessage extends HttpRequestHandlerBase {

	public ReceiveMessage(Infrastructure infrastructure, NetworkThread networkThread, 
		   		  		  MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.equals("messages");
		boolean isMethodOk = requestInformation.Method.equals("POST");

		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation, String requestBody) {
		DeviceMessageRequest deviceMessageRequest = gson.fromJson(requestBody, DeviceMessageRequest.class);
		taskHandler.saveMessage(deviceMessageRequest);
		
		return null;
	}

}
