package pl.edu.agh.dkala_msc.httpd.internal.requests;

import pl.edu.agh.dkala_msc.device.DeviceInformation;

public class RemoteTaskReadyRequest {

	public String SenderIp;
	
	public String TaskId;
	
	public DeviceInformation ReadyDevices[];
	
}
