package pl.edu.agh.dkala_msc.httpd.internal.handlers;

import android.webkit.WebView;
import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.internal.requests.SaveDeviceConnectionRequest;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;

public class SuccessfullyConnectedToCloud extends HttpRequestHandlerBase {

	public SuccessfullyConnectedToCloud(Infrastructure infrastructure, NetworkThread networkThread, 
	   		  							MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.equals("connection");
		boolean isMethodOk = requestInformation.Method.equals("POST");
		
		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation, String requestBody) {
		SaveDeviceConnectionRequest saveDeviceConnectionRequest = gson.fromJson(requestBody, SaveDeviceConnectionRequest.class);
		infrastructure.setRootDevice(saveDeviceConnectionRequest.DeviceInformation);
		
		return null;
	}

}
