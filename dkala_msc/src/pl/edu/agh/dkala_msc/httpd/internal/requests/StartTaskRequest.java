package pl.edu.agh.dkala_msc.httpd.internal.requests;

import pl.edu.agh.dkala_msc.device.DeviceInformation;

public class StartTaskRequest {

	public String TaskId;
	
	public DeviceInformation[] OrderedDevices;
	
}
