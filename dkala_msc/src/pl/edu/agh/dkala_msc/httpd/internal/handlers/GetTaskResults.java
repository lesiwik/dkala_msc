package pl.edu.agh.dkala_msc.httpd.internal.handlers;

import android.webkit.WebView;
import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.internal.responses.TaskResultResponse;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;
import pl.edu.agh.dkala_msc.infrastructure.TaskInfo;

public class GetTaskResults extends HttpRequestHandlerBase {

	public GetTaskResults(Infrastructure infrastructure, NetworkThread networkThread, 
				   		  MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}
	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.startsWith("tasks");
		boolean isMethodOk = requestInformation.Method.equals("GET");
		
		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation, String requestBody) {
		String taskId = extractTaskId(requestInformation);
		TaskInfo taskInfo = taskHandler.getTaskInfo(taskId);
		
		TaskResultResponse taskResultResponse = new TaskResultResponse();
		taskResultResponse.Status = taskInfo.getStatus();
		taskResultResponse.Result = taskInfo.getResult();
		
		String jsonResponse = gson.toJson(taskResultResponse);
		return jsonResponse;
	}
	
	private String extractTaskId(RequestInformation requestInformation) {
		String taskId = requestInformation.Path.split("/")[1];
		return taskId;
	}

}
