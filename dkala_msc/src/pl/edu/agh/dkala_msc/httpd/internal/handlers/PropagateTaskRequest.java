package pl.edu.agh.dkala_msc.httpd.internal.handlers;

import android.webkit.WebView;
import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.device.DeviceInformation;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.internal.requests.ScheduleTaskExecutionRequest;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;

public class PropagateTaskRequest extends HttpRequestHandlerBase {

	public PropagateTaskRequest(Infrastructure infrastructure, NetworkThread networkThread, 
		   		  				MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.equals("tasks");
		boolean isMethodOk = requestInformation.Method.equals("POST");

		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation, String requestBody) {
		ScheduleTaskExecutionRequest scheduleTaskRequest = gson.fromJson(requestBody, ScheduleTaskExecutionRequest.class);
		
		taskHandler.saveRemoteTask(scheduleTaskRequest);
		final ScheduleTaskExecutionRequest newScheduleTaskRequest = createScheduleTaskRequest(scheduleTaskRequest);
		final String localRootIp = scheduleTaskRequest.RootDevice.IPAddress;

		DeviceInformation[] neighbors = infrastructure.getAllNeighbors();
		for (DeviceInformation neighbor : neighbors) {
			if (neighbor.IPAddress.equals(localRootIp)) continue;			
			propagateTaskRequest(newScheduleTaskRequest, neighbor);
		}
		
		return null;
	}
	
	private ScheduleTaskExecutionRequest createScheduleTaskRequest(
				ScheduleTaskExecutionRequest oldScheduleTaskRequest) {
		ScheduleTaskExecutionRequest scheduleTaskRequest = new ScheduleTaskExecutionRequest();
		
		scheduleTaskRequest.RootDevice = infrastructure.getCurrentDevice();
		scheduleTaskRequest.TaskId = oldScheduleTaskRequest.TaskId;
		scheduleTaskRequest.TaskName = oldScheduleTaskRequest.TaskName;
		scheduleTaskRequest.TaskJsCode = oldScheduleTaskRequest.TaskJsCode;
		scheduleTaskRequest.RequiredSensors = oldScheduleTaskRequest.RequiredSensors;
		
		return scheduleTaskRequest;
	}
	
	
	private void propagateTaskRequest(ScheduleTaskExecutionRequest scheduleTaskRequest, 
									  DeviceInformation scheduleDirection) {		
		String address = scheduleDirection.IPAddress;
		String url = "http://" + address + ":8080/tasks";
		
		networkThread.ScheduleRequest(url, scheduleTaskRequest, "post");
	}

}
