package pl.edu.agh.dkala_msc.httpd.internal.handlers;

import android.webkit.WebView;
import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.internal.requests.RemoteTaskReadyRequest;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;

public class PropagateOnRemoteTaskReady extends HttpRequestHandlerBase {

	public PropagateOnRemoteTaskReady(Infrastructure infrastructure, NetworkThread networkThread, 
	   		  						  MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.startsWith("tasks");
		boolean isMethodOk = requestInformation.Method.equals("PUT");
		
		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation, String requestBody) {
		RemoteTaskReadyRequest remoteTaskReadyRequest = gson.fromJson(requestBody, RemoteTaskReadyRequest.class);
		taskHandler.updateTaskRequest(remoteTaskReadyRequest);
				
		return null;
	}

}
