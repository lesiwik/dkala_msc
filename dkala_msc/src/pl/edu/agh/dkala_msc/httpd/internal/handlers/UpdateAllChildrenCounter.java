package pl.edu.agh.dkala_msc.httpd.internal.handlers;

import android.webkit.WebView;
import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.internal.requests.UpdateChildrenCountRequest;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;

public class UpdateAllChildrenCounter extends HttpRequestHandlerBase {

	public UpdateAllChildrenCounter(Infrastructure infrastructure, NetworkThread networkThread, 
	   		  					    MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.equals("children/count");
		boolean isMethodOk = requestInformation.Method.equals("PUT");
		
		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation, String requestBody) {
		UpdateChildrenCountRequest updateRequest = gson.fromJson(requestBody, UpdateChildrenCountRequest.class);
		infrastructure.updateChildrenCount(updateRequest.DeviceIp, updateRequest.AllChildrenCount);
		
		UpdateChildrenCountRequest furtherUpdateRequest = new UpdateChildrenCountRequest();
		furtherUpdateRequest.AllChildrenCount = infrastructure.getAllChildrenCount();
		furtherUpdateRequest.DeviceIp = infrastructure.getCurrentDevice().IPAddress;
		
		String address = infrastructure.getRootDevice().IPAddress;
		String uri = "http://" + address + ":8080/children/count";
		networkThread.ScheduleRequest(uri, furtherUpdateRequest, "put");

		return null;
	}

}
