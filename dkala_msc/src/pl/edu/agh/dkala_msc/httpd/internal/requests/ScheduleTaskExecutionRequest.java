package pl.edu.agh.dkala_msc.httpd.internal.requests;

import pl.edu.agh.dkala_msc.device.DeviceInformation;

public class ScheduleTaskExecutionRequest {

	public String TaskJsCode;
	
	public String[] RequiredSensors;
	
	public String TaskId;
	
	public String TaskName;
	
	public DeviceInformation RootDevice;
	
}
