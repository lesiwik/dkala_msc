package pl.edu.agh.dkala_msc.httpd;

import java.util.Map;

public class RequestInformation {
	
	public String Path;
	
	public String Method;
	
	public Map<String, String> Query;
	
}
