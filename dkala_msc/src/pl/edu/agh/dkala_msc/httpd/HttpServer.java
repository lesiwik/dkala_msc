package pl.edu.agh.dkala_msc.httpd;

import java.io.IOException;
import java.io.InputStream;
import android.webkit.WebView;
import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.httpd.internal.handlers.ConnectAnotherDeviceToCloud;
import pl.edu.agh.dkala_msc.httpd.internal.handlers.GetTaskResults;
import pl.edu.agh.dkala_msc.httpd.internal.handlers.PropagateOnRemoteTaskReady;
import pl.edu.agh.dkala_msc.httpd.internal.handlers.PropagateTaskRequest;
import pl.edu.agh.dkala_msc.httpd.internal.handlers.ReceiveMessage;
import pl.edu.agh.dkala_msc.httpd.internal.handlers.StartTask;
import pl.edu.agh.dkala_msc.httpd.internal.handlers.SuccessfullyConnectedToCloud;
import pl.edu.agh.dkala_msc.httpd.internal.handlers.UpdateAllChildrenCounter;
import pl.edu.agh.dkala_msc.httpd.remote.handlers.ConnectCurrentDeviceToCloud;
import pl.edu.agh.dkala_msc.httpd.remote.handlers.GetAllChildrenCounter;
import pl.edu.agh.dkala_msc.httpd.remote.handlers.GetCurrentDeviceInformation;
import pl.edu.agh.dkala_msc.httpd.remote.handlers.GetCurrentDeviceRoot;
import pl.edu.agh.dkala_msc.httpd.remote.handlers.GetCurrentDevicesChildren;
import pl.edu.agh.dkala_msc.httpd.remote.handlers.ResetDeviceCommand;
import pl.edu.agh.dkala_msc.httpd.remote.handlers.StartTaskExecution;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;
import fi.iki.elonen.NanoHTTPD;

public class HttpServer extends NanoHTTPD{

	private MainActivity _mainActivity;
	private IHttpRequestHandler[] _requestHandlers;
	
	public HttpServer(Infrastructure infrastructure, NetworkThread networkThread, 
					  MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(8080);
		
		_mainActivity = mainActivity;

		_requestHandlers = new IHttpRequestHandler[] {
			// internal:
			new ConnectAnotherDeviceToCloud(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new SuccessfullyConnectedToCloud(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new GetAllChildrenCounter(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new GetTaskResults(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new PropagateTaskRequest(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new PropagateOnRemoteTaskReady(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new StartTask(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new ReceiveMessage(infrastructure, networkThread, mainActivity, taskHandler, webView),
				
			// remote:
			new ConnectCurrentDeviceToCloud(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new GetCurrentDeviceInformation(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new GetCurrentDevicesChildren(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new ResetDeviceCommand(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new GetCurrentDeviceRoot(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new UpdateAllChildrenCounter(infrastructure, networkThread, mainActivity, taskHandler, webView),
			new StartTaskExecution(infrastructure, networkThread, mainActivity, taskHandler, webView)
		};
	}

	@Override
	public Response serve(IHTTPSession httpSession) {
		String html;
		try {
			RequestInformation requestInformation = extractRequestInformation(httpSession);
			IHttpRequestHandler requestHandler = selectRequestHandler(requestInformation);
			
			if (requestHandler != null) {
				String requestBody;
				requestBody = extractHttpBody(httpSession);
				html = requestHandler.handleRequest(requestInformation, requestBody);
				return new NanoHTTPD.Response(Response.Status.OK, MIME_HTML, html);
			} else {
				html = "<html><head></head><body>" +
						"    <h1>Resource cannot be found</h1>" +
						"</body></html>";
				return new NanoHTTPD.Response(Response.Status.NOT_FOUND, MIME_HTML, html);
			}
		} catch (Exception e) {
			final String message = e.getMessage();
			_mainActivity.runOnUiThread(new Runnable() {
				public void run() {
					_mainActivity.setErrorInformation(message);
				}
			});
			html = "<html><head></head><body>" +
					"    <h1>Internal error has occured</h1>" +
					"</body></html>";
			return new NanoHTTPD.Response(Response.Status.INTERNAL_ERROR, MIME_HTML, html);
		}
	}
	
	private RequestInformation extractRequestInformation(IHTTPSession httpSession) {
		RequestInformation requestInformation = new RequestInformation();
		requestInformation.Method = httpSession.getMethod().name();
		requestInformation.Path = httpSession.getUri();
		if (requestInformation.Path.startsWith("/"))
			requestInformation.Path = requestInformation.Path.substring(1);
		requestInformation.Query = httpSession.getParms();

		return requestInformation;
	}
	
	private IHttpRequestHandler selectRequestHandler(RequestInformation requestInformation) {
		for (IHttpRequestHandler requestHandler : _requestHandlers) {
			if (!requestHandler.canHandleRequest(requestInformation)) {
				continue;
			}
			
			return requestHandler;
		}
		
		return null;
	}
	
	private String extractHttpBody(IHTTPSession httpSession) throws Exception {
		String contentLengthHeader = httpSession.getHeaders().get("content-length");
		if (contentLengthHeader == null) {
			return "";
		}
		int contentLength = Integer.parseInt(contentLengthHeader);

		byte[] buffer = new byte[contentLength];
		if (contentLength != fillBufferCompletely(httpSession.getInputStream(), buffer)) {
			throw new Exception("Unsufficent http body");
		}
		String httpBody = new String(buffer, "UTF-8");
		
		return httpBody;
	}
	
	private int fillBufferCompletely(InputStream is, byte[] bytes) throws IOException {
	    int size = bytes.length;
	    int offset = 0;
	    while (offset < size) {
	        int read = is.read(bytes, offset, size - offset);
	        if (read == -1) {
	            if ( offset == 0 ) {
	                return -1;
	            } else {
	                return offset;
	            }
	        } else {
	            offset += read;
	        }
	    }

	    return size;
	}

}
