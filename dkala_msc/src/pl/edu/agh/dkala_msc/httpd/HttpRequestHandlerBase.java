package pl.edu.agh.dkala_msc.httpd;

import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;
import android.os.Handler;
import android.webkit.WebView;
import com.google.gson.Gson;

public abstract class HttpRequestHandlerBase implements IHttpRequestHandler {

	private MainActivity _mainActivity;
	
	protected Infrastructure infrastructure;
	protected NetworkThread networkThread;
	protected TaskHandler taskHandler;
	protected WebView webView;
	protected Gson gson;
	
	protected void inMainThread(Runnable runnable) {
		Handler mainHandler = new Handler(_mainActivity.getMainLooper());
		mainHandler.post(runnable);
	}

	public HttpRequestHandlerBase(Infrastructure infrastructure,
								  NetworkThread networkThread,
								  MainActivity mainActivity,
								  TaskHandler taskHandler,
								  WebView webView){
		this.infrastructure = infrastructure;
		this.networkThread = networkThread;
		this.taskHandler = taskHandler;
		this.webView = webView;
		
		_mainActivity = mainActivity;
		
		gson = new Gson();
	}
}
