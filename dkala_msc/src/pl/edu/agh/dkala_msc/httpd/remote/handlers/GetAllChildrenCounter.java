package pl.edu.agh.dkala_msc.httpd.remote.handlers;

import android.webkit.WebView;
import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.remote.responses.AllChildrenCountResponse;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;

public class GetAllChildrenCounter extends HttpRequestHandlerBase {

	public GetAllChildrenCounter(Infrastructure infrastructure, NetworkThread networkThread, 
		   		  				 MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.equals("commands/device/allChildrenCount");
		boolean isMethodOk = requestInformation.Method.equals("GET");
		
		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation, String requestBody) {
		AllChildrenCountResponse response = new AllChildrenCountResponse();
		response.AllChildrenCount = infrastructure.getAllChildrenCount();
		
		String json = gson.toJson(response);
		return json;
	}

}
