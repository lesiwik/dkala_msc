package pl.edu.agh.dkala_msc.httpd.remote.handlers;

import android.webkit.WebView;
import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.remote.responses.CurrentDeviceInformationResponse;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;

public class GetCurrentDeviceInformation extends HttpRequestHandlerBase {

	public GetCurrentDeviceInformation(Infrastructure infrastructure, NetworkThread networkThread, 
		   		  					   MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.equals("commands/device/information");
		boolean isMethodOk = requestInformation.Method.equals("GET");
		
		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation, String requestBody) {
		CurrentDeviceInformationResponse response = new CurrentDeviceInformationResponse();
		response.CurrentDeviceInformation = infrastructure.getCurrentDevice();
		
		String jsonResponse = gson.toJson(response);
		return jsonResponse;
	}

}
