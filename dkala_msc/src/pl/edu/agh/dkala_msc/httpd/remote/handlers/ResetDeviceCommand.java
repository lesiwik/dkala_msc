package pl.edu.agh.dkala_msc.httpd.remote.handlers;

import android.webkit.WebView;

import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;

public class ResetDeviceCommand extends HttpRequestHandlerBase {

	public ResetDeviceCommand(Infrastructure infrastructure, NetworkThread networkThread, 
							  MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.equals("commands/device/reset");
		boolean isMethodOk = requestInformation.Method.equals("POST");
		
		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation, String requestBody) {
		infrastructure.reset();
		taskHandler.reset();
		
		final String summary = "<html><head></head><body></body></html>";
				
		inMainThread(new Runnable(){
			public void run() {
				webView.loadData(summary, "text/html", null);
			}
		});
		
		return null;
	}

}
