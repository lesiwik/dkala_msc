package pl.edu.agh.dkala_msc.httpd.remote.handlers;

import java.util.List;

import android.webkit.WebView;

import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.device.DeviceInformation;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.remote.responses.CurrentDeviceChildrenResponse;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;

public class GetCurrentDevicesChildren extends HttpRequestHandlerBase {

	public GetCurrentDevicesChildren(Infrastructure infrastructure, NetworkThread networkThread, 
		   		  					 MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
	super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.equals("commands/device/children");
		boolean isMethodOk = requestInformation.Method.equals("GET");
		
		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation, String requestBody) {
		CurrentDeviceChildrenResponse response = new CurrentDeviceChildrenResponse();
		List<DeviceInformation> childrenList = infrastructure.getChildrenDevices();
		response.ChildrenDevices = childrenList.toArray(new DeviceInformation[childrenList.size()]);
		
		String jsonResponse = gson.toJson(response);
		return jsonResponse;
	}

}
