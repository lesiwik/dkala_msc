package pl.edu.agh.dkala_msc.httpd.remote.handlers;

import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.remote.responses.CurrentDeviceRootResponse;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;
import android.webkit.WebView;

public class GetCurrentDeviceRoot extends HttpRequestHandlerBase {

	public GetCurrentDeviceRoot(Infrastructure infrastructure, NetworkThread networkThread, 
		   		  				MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.equals("commands/device/root");
		boolean isMethodOk = requestInformation.Method.equals("GET");
		
		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation,
			String requestBody) {
		CurrentDeviceRootResponse response = new CurrentDeviceRootResponse();
		response.RootDevice = infrastructure.getRootDevice();
		String json = gson.toJson(response);
		
		return json;
	}

}
