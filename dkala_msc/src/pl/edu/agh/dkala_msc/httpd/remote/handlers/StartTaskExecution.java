package pl.edu.agh.dkala_msc.httpd.remote.handlers;

import pl.edu.agh.dkala_msc.MainActivity;
import pl.edu.agh.dkala_msc.httpd.HttpRequestHandlerBase;
import pl.edu.agh.dkala_msc.httpd.NetworkThread;
import pl.edu.agh.dkala_msc.httpd.RequestInformation;
import pl.edu.agh.dkala_msc.httpd.remote.requests.StartTaskExecutionRequest;
import pl.edu.agh.dkala_msc.httpd.remote.responses.TaskExecutionScheduledResponse;
import pl.edu.agh.dkala_msc.infrastructure.Infrastructure;
import pl.edu.agh.dkala_msc.infrastructure.TaskHandler;
import android.webkit.WebView;

public class StartTaskExecution extends HttpRequestHandlerBase {

	public StartTaskExecution(Infrastructure infrastructure, NetworkThread networkThread, 
	   		  				  MainActivity mainActivity, TaskHandler taskHandler, WebView webView) {
		super(infrastructure, networkThread, mainActivity, taskHandler, webView);
	}

	@Override
	public boolean canHandleRequest(RequestInformation requestInformation) {
		boolean isPathOk = requestInformation.Path.equals("commands/tasks");
		boolean isMethodOk = requestInformation.Method.equals("POST");

		return isPathOk && isMethodOk;
	}

	@Override
	public String handleRequest(RequestInformation requestInformation,
			String requestBody) {
		StartTaskExecutionRequest startTaskRequest = gson.fromJson(requestBody,
				StartTaskExecutionRequest.class);

		String taskId = taskHandler.scheduleTaskExecution(
				startTaskRequest.TaskName, startTaskRequest.TaskJsCode,
				startTaskRequest.RequiredSensors);

		TaskExecutionScheduledResponse taskExecutionScheduledResponse = new TaskExecutionScheduledResponse();
		taskExecutionScheduledResponse.TaskId = taskId;
		String jsonResponse = gson.toJson(taskExecutionScheduledResponse);

		return jsonResponse;
	}

}
