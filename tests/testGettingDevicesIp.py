import sys
sys.path.insert(0, '../tools')

import time
import json
import unittest
import controller

devices = [device.strip() for device in open('../tools/devices.config')]

class TestGettingDevicesIp(unittest.TestCase):

    def setUp(self):
		self.controller = controller.Controller()
		self.controller.init('../tools/devices.config')
		self.controller.resetAllDevices()

    def test_devices_ip(self):
		for i in range(0, 10):
			deviceInformation = self.controller.getDeviceInformation(i)			
			self.assertEqual(deviceInformation["CurrentDeviceInformation"]["IPAddress"], devices[i])

if __name__ == '__main__':
    unittest.main()