/// <reference path="../tasks/mandelbrodt.js" />
/// <reference path="jasmine.js" />
/// <reference path="sinon-1.9.0.js" />

var Platform, PlatformTools;

Platform = {
	getDeviceNumber: function() {
	
	},
	
	getDevicesCount: function() {
	
	}
};

PlatformTools = {
	finalize: function() {
	
	},
	
	onReceive: function() {
	
	},
	
	sendTo: function() {
	
	}
};

describe("mandelbrodt.js", function () {
	var getDeviceNumberStub, getDevicesCountStub, onReceiveStub,
		finalizeSpy, sendToSpy;
		
	beforeEach(function() {
		finalizeSpy = sinon.spy(PlatformTools, "finalize");
		sendToSpy = sinon.spy(PlatformTools, "sendTo");
	});
	
	afterEach(function() {
		if (getDeviceNumberStub && getDeviceNumberStub.restore) {
			getDeviceNumberStub.restore();
			getDeviceNumberStub = null;
		}
		if (getDevicesCountStub && getDevicesCountStub.restore) {
			getDevicesCountStub.restore();
			getDevicesCountStub = null;
		}
		if (onReceiveStub && onReceiveStub.restore) {
			onReceiveStub.restore();
			onReceiveStub = null;
		}
		if (finalizeSpy && finalizeSpy.restore) {
			finalizeSpy.restore();
			finalizeSpy = null;
		}
		if (sendToSpy && sendToSpy.restore) {
			sendToSpy.restore();
			sendToSpy = null;
		}
	});

    it("should compile", function () {
        task();
    });
	
	describe ("root in two devices configuration", function() {
		
		beforeEach(function() {
			// arrange
			getDeviceNumberStub = sinon.stub(Platform, "getDeviceNumber", function() {
				return 0;
			});
			
			getDevicesCountStub = sinon.stub(Platform, "getDevicesCount", function() {
				return 2;
			});
			
			onReceiveStub = sinon.stub(PlatformTools, "onReceive", function(sender, callback) {
				callback(['remote results']);
			});
		});
	
		it("should finalize calculation", function() {
			// act
			task();
			
			// assert
			expect(finalizeSpy.calledOnce).toBe(true);
		});
	
	});
	
	describe ("slave in two devices configuration", function() {
		
		beforeEach(function() {
			// arrange
			getDeviceNumberStub = sinon.stub(Platform, "getDeviceNumber", function() {
				return 1;
			});
			
			getDevicesCountStub = sinon.stub(Platform, "getDevicesCount", function() {
				return 2;
			});
			
		});
		
		it("should send results to root", function() {
			// act
			task();
			
			// assert
			expect(sendToSpy.calledOnce).toBe(true);
		});
		
	});
	
});