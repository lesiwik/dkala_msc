import sys
sys.path.insert(0, '../tools')

import json, time, unittest, controller

devices = [device.strip() for device in open('../tools/devices.config')]

class TestAccessingEnvironmentInfo(unittest.TestCase):

	def setUp(self):
		self.controller = controller.Controller()
		self.controller.init('../tools/devices.config')
		self.controller.resetAllDevices()
		
		for i in range(0, 9):
			self.controller.connect(i + 1, i)
			time.sleep(0.5)
	
	@classmethod
	def tearDownClass(cls):
		c = controller.Controller()
		c.init('../tools/devices.config')
		c.resetAllDevices()
			
	def testGettingDeviceNumberOnRoot(self):
		# arrange		
		basicJsTask = 'Platform.finalizeTask(Platform.getDeviceNumber());'
		taskRequest = { 'TaskJsCode': basicJsTask }
		
		# act
		taskId = self.controller.scheduleTaskRequest(3, taskRequest)['TaskId']
		
		# assert
		time.sleep(0.5)
		taskResult = self.controller.getTaskResult(3, taskId)
		self.assertEqual(taskResult['Result'], "0")
			
	def testGettingDeviceCountOnRoot(self):
		# arrange		
		basicJsTask = 'Platform.finalizeTask(Platform.getDevicesCount());'
		taskRequest = { 'TaskJsCode': basicJsTask }
		
		# act
		taskId = self.controller.scheduleTaskRequest(4, taskRequest)['TaskId']
		
		# assert
		time.sleep(0.5)
		taskResult = self.controller.getTaskResult(4, taskId)
		self.assertEqual(taskResult['Result'], "10")
		
	def testCollectingDeviceNumbersFromCloud(self):
		# arrange		
		basicJsTask =  """
		var deviceNumber = Platform.getDeviceNumber();
		var devicesCount = Platform.getDevicesCount();
		var deviceNumbersArray = [];
		
		if (deviceNumber === 0) {
			for (var i = 1; i < devicesCount; i++) {
				while(true) {
					var response = Platform.receiveFromDevice(i);
					if (!response) {
						continue;
					}
					
					deviceNumbersArray[i] = parseInt(response);
					break;
				}
			}
			deviceNumbersArray[0] = deviceNumber;
		} else {
			Platform.sendToDevice(deviceNumber, 0);
		}
			
		Platform.finalizeTask(JSON.stringify(deviceNumbersArray));
		"""
		
		taskRequest = { 'TaskJsCode': basicJsTask }
		
		# act
		taskId = self.controller.scheduleTaskRequest(5, taskRequest)['TaskId']
		
		# assert
		time.sleep(0.5)
		taskResult = self.controller.getTaskResult(5, taskId)
		self.assertEqual(json.loads(taskResult['Result']), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

if __name__ == '__main__':
	unittest.main()