import sys
sys.path.insert(0, '../tools')

import time
import json
import unittest
import controller

devices = [device.strip() for device in open('../tools/devices.config')]

class TestFormingCloud(unittest.TestCase):

	def setUp(self):
		self.controller = controller.Controller()
		self.controller.init('../tools/devices.config')
		self.controller.resetAllDevices()
	
	@classmethod
	def tearDownClass(cls):
		c = controller.Controller()
		c.init('../tools/devices.config')
		c.resetAllDevices()

	def test_simple_connection(self):
		# act
		self.controller.connect(0, 1)
		
		# assert
		time.sleep(1)
		childIp = self.controller.getConnectedDevices(1)['ChildrenDevices'][0]['IPAddress']
		rootIp = self.controller.getDeviceRoot(0)['RootDevice']['IPAddress']
		allChildrenCount = self.controller.getAllChildrenCount(1)['AllChildrenCount'];
		
		self.assertEqual(childIp, devices[0])
		self.assertEqual(rootIp, devices[1])
		self.assertEqual(allChildrenCount, 1)

	def test_root_connection_delegation(self):
		# act
		self.controller.connect(1, 2)
		time.sleep(1)
		self.controller.connect(0, 1)
		
		# assert
		time.sleep(1)
		
		rootChildren = self.controller.getConnectedDevices(2)['ChildrenDevices'];
		
		firstChildIp = rootChildren[0]['IPAddress']
		secondChildIp = rootChildren[1]['IPAddress']
		
		firstChildRootIp = self.controller.getDeviceRoot(1)['RootDevice']['IPAddress']
		secondChildRootIp = self.controller.getDeviceRoot(1)['RootDevice']['IPAddress']
		
		allChildrenCount = self.controller.getAllChildrenCount(2)['AllChildrenCount'];
		
		self.assertEqual(firstChildIp, devices[1])
		self.assertEqual(secondChildIp, devices[0])
		self.assertEqual(firstChildRootIp, devices[2])
		self.assertEqual(secondChildRootIp, devices[2])
		self.assertEqual(allChildrenCount, 2)

	def test_child_connection_delegation(self):
		# act
		for i in range(0, 9):
			self.controller.connect(i + 1, i)
			time.sleep(0.5)
		
		for i in range(1, 5):
			commonRootIp = self.controller.getDeviceRoot(i)['RootDevice']['IPAddress']
			childIp = self.controller.getConnectedDevices(i)['ChildrenDevices'][0]['IPAddress']
			rootIp = self.controller.getDeviceRoot(i + 5)['RootDevice']['IPAddress']
			allChildrenCount = self.controller.getAllChildrenCount(i)['AllChildrenCount'];
			
			self.assertEqual(commonRootIp, devices[0])
			self.assertEqual(childIp, devices[i + 5])
			self.assertEqual(rootIp, devices[i])
			self.assertEqual(allChildrenCount, 1)
			
		allChildrenCount = self.controller.getAllChildrenCount(0)['AllChildrenCount'];
		self.assertEqual(allChildrenCount, 9)


if __name__ == '__main__':
	unittest.main()