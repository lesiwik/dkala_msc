import sys
sys.path.insert(0, '../tools')

import json, time, unittest, controller

devices = [device.strip() for device in open('../tools/devices.config')]

class TestJsLibInfo(unittest.TestCase):

    def setUp(self):
        self.controller = controller.Controller()
        self.controller.init('../tools/devices.config')
        self.controller.resetAllDevices()
        
    def testSendReceiveAndFinalize(self):
        # arrange                
        for i in range(0, 9):
            self.controller.connect(i + 1, i)
            time.sleep(0.1)
            
        jsTask =  """
        var deviceNumber = Platform.getDeviceNumber();
        
        if (deviceNumber === 0) {
            PlatformTools.onReceive(1, function(message) {
                PlatformTools.finalize(message);
            });
        } else {
            PlatformTools.sendTo("oh yeah!", 0);
            PlatformTools.finalize();
        }            
        """
        
        taskRequest = { 'TaskJsCode': jsTask }
        
        # act
        taskId = self.controller.scheduleTaskRequest(5, taskRequest)['TaskId']
        
        # assert
        time.sleep(1)
        taskResult = self.controller.getTaskResult(5, taskId)
        self.assertEqual(json.loads(taskResult['Result']), "oh yeah!");
        
    def testSendReceiveAndFinalizeOnArrayOfObjects(self):
        # arrange        
        self.controller.connect(5, 6)
        time.sleep(1)
        
        jsTask =  """
        var deviceNumber = Platform.getDeviceNumber();
        
        if (deviceNumber === 0) {
            PlatformTools.onReceive(1, function(message) {
                var localArray = [{x: 2, y:2}];
                localArray.push.apply(localArray, message);
                PlatformTools.finalize(localArray);
            });
        } else {
            PlatformTools.sendTo([{
                x: 0,
                y: 0
            }, {
                x: 1,
                y: 1
            }], 0);
            PlatformTools.finalize();
        }            
        """
        
        taskRequest = { 'TaskJsCode': jsTask }
        
        # act
        taskId = self.controller.scheduleTaskRequest(5, taskRequest)['TaskId']
        
        # assert
        time.sleep(1)
        taskResult = self.controller.getTaskResult(5, taskId)
        self.assertEqual(taskResult['Result'], u'[{"x":2,"y":2},{"x":0,"y":0},{"x":1,"y":1}]');
        
    def testGettingDataFromAccelerometer(self):
        # arrange
        jsTask = """
        var measurements = [];
        for (var i = 0; i < 100; i++) {
            setTimeout(function() {
                var accelerometerRead = PlatformTools.readSensor('accelerometer');
                measurements.push(accelerometerRead);
            }, i * 10);
        }
        
        setTimeout(function() {
            PlatformTools.finalize(measurements);
        }, 1100);
        """
        
        taskRequest = { 'TaskJsCode': jsTask, 'RequiredSensors': [ 'accelerometer' ] }
        
        # act
        taskId = self.controller.scheduleTaskRequest(10, taskRequest)['TaskId']
        
        # assert
        taskResult = self.controller.waitForTaskResult(10, taskId)
        self.assertEqual(len(taskResult), 100);
        
    def testGettingDataFromAccelerometerWithoutAskingPermission(self):
        # arrange
        jsTask = """
        var measurements = [];
        for (var i = 0; i < 100; i++) {
            setTimeout(function() {
                var accelerometerRead = PlatformTools.readSensor('accelerometer');
                measurements.push(accelerometerRead);
            }, i * 10);
        }
        
        setTimeout(function() {
            PlatformTools.finalize(measurements);
        }, 1100);
        """
        
        taskRequest = { 'TaskJsCode': jsTask, 'RequiredSensors': [ ] }
        
        # act
        taskId = self.controller.scheduleTaskRequest(10, taskRequest)['TaskId']
        
        # assert
        time.sleep(1)
        taskResult = self.controller.getTaskResult(10, taskId)
        response = json.loads(taskResult['Result'])
        for i in range(0, 100):
            self.assertEqual(response[i], None)
        
    def testFilteringDevicesWithAccessToAccelerometer(self):
        # arrange                
        for i in range(0, 10):
            self.controller.connect(i + 1, i)
            time.sleep(0.1)
            
        jsTask = """
        var devicesCount = Platform.getDevicesCount();
        PlatformTools.finalize(devicesCount);
        """
        
        taskRequest = { 'TaskJsCode': jsTask, 'RequiredSensors': [ 'accelerometer' ] }
        
        # act
        taskId = self.controller.scheduleTaskRequest(10, taskRequest)['TaskId']
        
        # assert
        time.sleep(2)
        taskResult = self.controller.getTaskResult(10, taskId)
        self.assertEqual(taskResult['Result'], '11');
        
    def testFilteringDevicesWithAccessToLightAndAccelerometer(self):
        # arrange                
        for i in range(0, 10):
            self.controller.connect(i + 1, i)
            time.sleep(0.1)
            
        jsTask = """
        var devicesCount = Platform.getDevicesCount();
        PlatformTools.finalize(devicesCount);
        """
        
        taskRequest = { 'TaskJsCode': jsTask, 'RequiredSensors': [ 'light', 'accelerometer' ] }
        
        # act
        taskId = self.controller.scheduleTaskRequest(10, taskRequest)['TaskId']
        
        # assert
        time.sleep(2)
        taskResult = self.controller.getTaskResult(10, taskId)
        self.assertEqual(taskResult['Result'], '1');
        
    def testVirtualTopology(self):
        # arrange                
        for i in range(0, 9):
            self.controller.connect(i + 1, i)
            time.sleep(0.1)
            
        jsTask =  """
        var devicesCount = Platform.getDevicesCount();
        var deviceNumber = Platform.getDeviceNumber();   
        var rootNumber = (function getRootNumber() {        
            if (deviceNumber === 0) return;        
            return Math.floor((deviceNumber - 1) / 2);        
        })();        
        var leftChildNumber = (function getLeftChildNumber() {        
            var number = deviceNumber * 2 + 1;        
            if (number < devicesCount) {        
                return number;        
            }        
        })();        
        var rightChildNumber = (function getRightChildNumber() {        
            var number = deviceNumber * 2 + 2;        
            if (number < devicesCount) {        
                return number;        
            }        
        })();
        function onReceiveCallback(message, sender) {
            if ((rootNumber || rootNumber === 0) && (rootNumber !== sender)) {
                PlatformTools.sendTo(message, rootNumber);
            }
            if (leftChildNumber && (leftChildNumber !== sender)) {
                PlatformTools.sendTo(message, leftChildNumber);
            }
            if (rightChildNumber && (rightChildNumber !== sender)) {
                PlatformTools.sendTo(message, rightChildNumber);
            }
            PlatformTools.finalize(message);
        }
        if (rootNumber || rootNumber === 0) {
            PlatformTools.onReceive(rootNumber, function(message) {
                onReceiveCallback(message, rootNumber);
            });
        }
        if (leftChildNumber) {
            PlatformTools.onReceive(leftChildNumber, function(message) {
                onReceiveCallback(message, leftChildNumber);
            });
        }
        if (rightChildNumber) {
            PlatformTools.onReceive(rightChildNumber, function(message) {
                onReceiveCallback(message, rightChildNumber);
            });
        }     
        
        if (deviceNumber === 4) {
            onReceiveCallback("message", 4);
        }
        """
        
        taskRequest = { 'TaskJsCode': jsTask }
        
        # act
        taskId = self.controller.scheduleTaskRequest(5, taskRequest)['TaskId']
        
        # assert
        for i in range(0, 10):
            result = self.controller.waitForTaskResult(i, taskId)
            self.assertEqual(result, "message")
            self.assertEqual(result, "message")
        
if __name__ == '__main__':
    unittest.main()