import sys
sys.path.insert(0, '../tools')

import time
import unittest
import controller

devices = [device.strip() for device in open('../tools/devices.config')]

class TestFormingCloud(unittest.TestCase):

	def setUp(self):
		self.controller = controller.Controller()
		self.controller.init('../tools/devices.config')
		self.controller.resetAllDevices()
	
	@classmethod
	def tearDownClass(cls):
		c = controller.Controller()
		c.init('../tools/devices.config')
		c.resetAllDevices()

	def testAssigningDifferentIdToNewTasks(self):
		# arrange
		taskRequest = { 'TaskJsCode': '' }
		
		# act
		firstResponse = self.controller.scheduleTaskRequest(0, taskRequest)
		secondResponse = self.controller.scheduleTaskRequest(0, taskRequest)
		
		# assert
		self.assertNotEqual(firstResponse['TaskId'], secondResponse['TaskId'])

	def testExecutingVeryBasicTask(self):
		# arrange
		basicJsTask = 'Platform.finalizeTask(2 + 2);'
		taskRequest = { 'TaskJsCode': basicJsTask }
		
		# act
		taskId = self.controller.scheduleTaskRequest(0, taskRequest)['TaskId']
		
		# assert
		time.sleep(0.1)		
		taskResult = self.controller.getTaskResult(0, taskId)
		self.assertEqual(taskResult['Result'], "4")

	def testExecutingBasicTasksOneByOne(self):
		# arrange
		basicJsTask = 'Platform.finalizeTask(2 * 3);'
		taskRequest = { 'TaskJsCode': basicJsTask }
		
		# act
		self.controller.scheduleTaskRequest(0, taskRequest)
		taskId = self.controller.scheduleTaskRequest(0, taskRequest)['TaskId']
		
		# assert
		time.sleep(0.1)
		taskResult = self.controller.getTaskResult(0, taskId)
		self.assertEqual(taskResult['Result'], "6")
		
	def testExecutingBasicTaskOnManyDevices(self):
		# arrange
		for i in range(0, 9):
			self.controller.connect(i + 1, i)
			time.sleep(0.1)
		
		basicJsTask = 'Platform.finalizeTask("asdf" + "vcxz");'
		taskRequest = { 'TaskJsCode': basicJsTask }
		
		# act
		taskId = self.controller.scheduleTaskRequest(0, taskRequest)['TaskId']
		
		# assert
		time.sleep(0.5)
		taskResult = self.controller.getTaskResult(0, taskId)
		self.assertEqual(taskResult['Result'], "asdfvcxz")
		
	def testMakingSampleCommunicationBetweenDevices(self):
		# arrange
		self.controller.connect(1, 2)
		time.sleep(0.5)
		basicJsTask =  """
		var message = "predefined";
		var deviceNumber = Platform.getDeviceNumber();
		if (deviceNumber === 0) {
			while(true) {
				message = Platform.receiveFromDevice(1);
				if (message) {
					break;
				}
			}
		} else {
			Platform.sendToDevice("oh yeah", 0);
		}
			
		Platform.finalizeTask(message);
		"""
		taskRequest = { 'TaskJsCode': basicJsTask }
		
		# act
		taskId = self.controller.scheduleTaskRequest(2, taskRequest)['TaskId']
		
		# assert
		time.sleep(1)
		taskResult = self.controller.getTaskResult(2, taskId)
		self.assertEqual(taskResult['Result'], "oh yeah")

if __name__ == '__main__':
	unittest.main()