\chapter{Weryfikacja eksperymentalna}
\label{cha:rozdzial8}

Eksperymenty przeprowadzone w~celach weryfikacji przydatności produktu \textit{Mobile Cloud Platform} do prowadzenia obliczeń równoległych obejmują dwa odrębne obszary. Pierwszy z~nich dotyczy zmierzenia stopnia, w~jakim platforma \textit{Mobile Cloud Platform} spełnia założenia wyspecyfikowane w~rozdziale piątym. Weryfikacja eksperymentalna polega w~tym przypadku na sprawdzeniu, w~jakim stopniu zostały spełnione zdefiniowane wymagania oraz czy założenia odnośnie samozarządzalności i~wieloplatformowości chmury mobilnej są spełnione. Drugi obszar to testy wydajności obliczeń rozproszonych prowadzonych na przykładowych chmurach mobilnych z podziałem na trzy rodzaje algorytmów równoległych. Zasadniczym celem tych testów jest wstępna weryfikacja, postawionej w~niniejszej pracy, tezy o~możliwości prowadzenia efektywnych obliczeń rozproszonych na urządzeniach mobilnych. Na koniec dokonano próby oszacowania wydajności urządzeń mobilncyh w~chmurze obliczeniowej wspieranej przez \textit{Mobile Cloud Platform} i~porównano ją do największych superkomputerów i~systemów gridowych świata.

\section{Weryfikacja poprawności założeń}

Zrealizowanie zdefiniowanych wymagań funkcjonalnych, przedstawionych w~postaci akcji \textbf{Administratora chmury} oraz akcji \textbf{Użytkownika końcowego}, została zweryfikowana w~poprzednim rozdziale, w~postaci fragmentów podręcznika użytkowania. Sposób weryfikacji pozostałych wymagań funkcjonalnych, przedstawionych w~postaci akcji \textbf{Prowadzącego badanie}, przedstawiono poniżej\footnote{Numeracja zastosowana w~niniejszej liście odpowiada bezpośrednio numeracji wymagań funkcjonalnych.}:
\begin{itemize}
    \item[11] \textit{Możliwość redagowania kodu źródłowego zadania obliczeniowego w~oparciu o~dobrze zdefiniowane API} - wymaganie zostało spełnione poprzez wyspecyfikowanie API programistycznego, załączonego w~dodatku~B. 
    \item[12] \textit{Możliwość pobrania wyników zadania z chmury obliczeniowej} - wymaganie zostało zweryfikowane w~ramach testów integracyjnych opisanych w~rozdziale szóstym oraz podczas tworzenia podręcznika użytkownika zawartego w~poprzednim rozdziale.
    \item[13] \textit{Możliwość filtrowania urządzeń, na których ma zostać wykonane zadanie obliczeniowe} - wymaganie zostało zrealizowane jedynie połowicznie. Obecnie nie ma możliwości ustalenia filtrów z~poziomu interfejsu użytkownika, a~jedynie z~poziomu API REST'owego wystawianego przez aplikację mobilną. Spełnienie wymagania zostało zweryfikowane poprzez jeden z~automatycznych testów integracyjnych.
\end{itemize}

Weryfikacja założenia o samozarządzalności sprowadza się do uruchomienia testów integracyjnych. W~ramach jednego ze scenariuszy, zawartych w~obszarze testowym nazwanym \textit{\mbox{testFormingCloud.py}}, konstruowana jest chmura obliczeniowa z~wykorzystaniem urządzeń mobilnych z~uruchomioną aplikacją \textit{Android Clinet}, a~następnie sprawdzana jest jej poprawność.

Możliwość spełnienia założenia o~wieloplatformowości została częściowo zaadresowana bezpośrednio przy definicji założenia, poprzez wypisanie technologii, które umożliwiają dostarczenie takiego rozwiązania na następujące systemy operacyjne: Android, Windows Phone oraz iOS. Implementacja aplikacji mobilnej w~oparciu o~technologie wyszczególnione dla systemu operacyjnego Android, została zweryfikowana za pośrednictwem testów integracyjnych. Dalsze weryfikacje polegają na dostarczeniu aplikacji mobilnej na pozostałe systemy operacyjne, co wiąże się z~nakładem przekraczającym dostępny czas na zrealizowanie niniejszej pracy. Jednakże wiedząc, że wybrane technologie można z~powodzeniem wykorzystać przy implementacji rozwiązania na system operacyjny Android oraz~wiedząc że są one wspierane przez pozostałe systemy operacyjne, można wnioskować, że dostarczenie aplikacji mobilnej na systemy operacyjne Windows Phone oraz iOS jest osiągalne.

\section{Testy wydajności obliczeń}

Nawiązując do rozdziału trzeciego, w~którym omówione zostały podstawowe informacje na temat algorytmów równoległych, testy wydajności zostały przeprowadzone na trzech zadaniach obliczeniowych z~różną ziarnistością. Pierwsze zadanie obliczeniowe polega na wyznaczeniu zbioru Mandelbrota na danym przedziale liczb urojonych. Zadanie to wykazuje potencjalnie małą ziarnistość - stosunek czasu przeznaczonego na obliczenia równoległe do czasu spędzonego na prowadzeniu komunikacji jest niewielki. Drugie zadanie obliczeniowe, wyznaczanie liczby PI metodą Monte Carlo to zadanie, które wykazuje dużą ziarnistość, ponieważ komunikacja występująca jedynie przy agregacji wyników jest rzadka, a~ilość przesyłanych danych niewielka. Ostatnie zadanie obliczeniowe, rozwiązywanie TSP za pomocą algorytmu genetycznego, nie wymaga komunikacji pomiędzy instancjami wykonywanego algorytmu, a~korzyść ze zwiększania rozmiarów chmury wynika z~uwarunkowań heurystycznych.

\subsection{Metodologia prowadzenia testów}

Miary wydajności zastosowane do prowadzonych testów to przyśpieszenie i~efektywność\footnote{Obie miary zostały szczegółowo omówione w rozdziale drugim.}. Zostały one wybrane, ponieważ doskonale obrazują wydajność zarówno algorytmu równoległego, jak i~środowiska, na którym został on uruchomiony. W~celu zobrazowania wydajności \textit{Mobile Cloud Platform} w~postaci wykresów przyśpieszenia i~efektywności, przeprowadzono pomiary czasu wykonywania danego algorytmu w~zależności od ilości urządzeń wchodzących w~skład tworzonej chmury obliczeniowej oraz rozmiaru rozwiązywanego problemu.

\subsection{Rodzaje zadań testowych}

Pierwsze zadanie obliczeniowe, nazywane dalej Mandelbrot.js, polega na wyznaczeniu zbioru Mandelbrota. Jest to podzbiór płaszczyzny zespolonej, której brzeg jest fraktalem (zob.~rysunek~\ref{fig:mandelbrot}). Zbiór $M$ tworzą te punkty $p \in \mathbb{C}$, dla których ciąg opisany równaniem rekurencyjnym: 
\begin{center}
    $\begin{cases} z_0 = 0 \\ z_{n+1} = z_n^2 + p \end{cases}$
\end{center}
nie dąży do nieskończoności, lub równoznacznie:

\begin{center}
    $M = \{p \in \mathbb{C} : \forall_{n \in \mathbb{N}}|z_n| < 2\}$.
\end{center}

W~ramach zadania obliczeniowego Mandelbrot.js zbiór Mandelbrota jest wyznaczany na przedziale $[-1.85; 0.65]$ dla liczb rzeczywistych i $[-1.25; 1.25]$ dla liczb urojonych. Przedstawienie go na płaszczyźnie liczb urojonych (oś pionowa to liczby urojone, a~pozioma rzeczywiste) polega na zobrazowaniu liczby $p$, skojarzonej z~odpowiadającą jej liczbą zespoloną $z_n$,  w~postaci natężenia pewnego koloru (zob.~rysunek~\ref{fig:mandelbrot}).

\begin{figure}[!h]
	\centering
		\includegraphics[width=8cm]{mandelbrodt}
	\caption{Zbiór Mandelbrota wygenerowany przez zadanie Mandelbrot.js, narysowany przez dedykowany serwer danych, dostarczony razem z \textit{Mobile Cloud Platform}.}
	\label{fig:mandelbrot}
\end{figure}

Zrównoleglenie w~zadaniu Mandelbrot.js polega na podziale przedziału liczb rzeczywistych na równe części. Każda instacja zadania obliczeniowego wylicza swój fragment i~przesyła go na serwer danych. Ilość punktów dla których ciąg $z_n$ jest wyznaczany wynosi 810~000, a~rozmiar zagregowanego wyniku wynosi ok.~2.1MB.

Drugie zadanie obliczeniowe polega na wyznaczeniu liczby Pi metodą Monte Carlo. Zasada działania algorytmu wygląda następująco:

\begin{enumerate}
    \item Każda instacja zadania obliczeniowego losuje ustaloną ilość razy punkt na płaszczyźnie, który zawiera się w~następującym kwadracie: $XY(x, y) : x \in [0; 1] \land y \in [0; 1]$.
    \item Każda instancja algorytmu zlicza ilość punktów, które zostały wylosowane wewnątrz koła wpisanego w~kwadrat~$XY$.
    \item Wszystkie urządzenia wchodzące w skład zadania obliczeniowego formują binarną topologię wirtualną, taką jak przykładowa topologia drzewiasta opisana w~rozdziale drugim (zob.~rysunek~\ref{fig:binary}).
    \item Wyniki są agregowane przy przesyłaniu ich od liści do korzenia drzewa.
    \item Po zagregowaniu wyników, korzeń drzewa zawiera właściwe rozwiązanie zadania.
\end{enumerate}

W~niniejszym zadaniu obliczeniowym przewidywany czas przeznaczony na komunikację w~stosunku do czasu przeznaczonego na prowadzenie obliczeń jest niewielki, głównie ze względu na ilość przesyłanych danych (w~komunikacie przesyłanym pomiędzy urządzeniami zawsze znajduje się tylko jedna liczba - ilość punktów wewnątrz koła wpisanego w~kwadrat~$XY$). Większa ziarnistość niż w~poprzednim zadaniu obliczeniowym nie wynika z~ilości wystąpień komunikacji pomiędzy urządzeniami, a~ze znacznie mniejszej ilości przesyłanych danych.

Ostatnie z~zadań, wykorzystanych do testów wydajnościowych, to algorytm genetyczny, wspomniany w~poprzednim rozdziale, rozwiązujący TSP. Sposób działania algorytmów genetycznych przypomina zjawisko ewolucji biologicznej. Środowisko, na którym operują algorytmy genetyczne oparte jest o~populację osobników (w~przypadku zastosowania algorytmu do rozwiązania TSP osobnik reprezentuje jedną z możliwych ścieżek). Każdy z~osobników ma przypisany pewien zbiór informacji stanowiący jego genotyp. Genotyp składa się z chromosomów, w~których zakodowany jest zbiór cech, które podlegają ocenie funkcji przystosowania modelującej środowisko. Przebieg algorytmu genetycznego rozpoczyna się od wylosowania początkowej populacji osobników. Następnie przez ustaloną ilość iteracji, lub do otrzymania satysfakcjonującego wyniku, następuje tworzenie nowych osobników przez operację krzyżowania, mutacji części osobników oraz selekcji, na podstawie której tworzona jest nowa populacja~\cite{GeneticAlgorithms}. W~dostarczonej implementacji tsp.js proces krzyżowania został oparty o~tzw. sekwencyjne konstruktywne krzyżowanie~\cite{CrossoverSCX}. Mutacja, jako operacja prosta z~natury, polega na ewentualnej zmianie kolejności czterech następujących po sobie punktów w~danej ścieżce.

Proces zrównoleglania zadania tsp.js oparty jest o~tzw. embarrassing parallelism (ang.), co oznacza że zrównoleglenie algorytmu jest trywialne z~racji braku zależności pomiędzy instancjami zadnia obliczeniowego~\cite{Ian_Foster}. Potencjalny zysk wydajnościowy związany z~uruchomieniem kilku instancji algorytmu genetycznego, o~dobrze dobranych parametrach, powinien wykazywać superliniowe właściwości~\cite{PGA}. Jest to związane z~oparciem mechanizmu przeszukiwania wyników o~dobry generator liczb losowych.

Zestaw danych testowych został pobrany ze strony internetowej Uniwersytetu Waterloo\footnote{http://www.math.uwaterloo.ca/tsp/vlsi/index.html}. Przykład wykorzystany we wszystkich testach wydajnościowych to XQF131\footnote{http://www.math.uwaterloo.ca/tsp/vlsi/index.html\#XQF131} (zestaw 131 punktów na płaszczyźnie z~metryką eukliesową).

\subsection{Środowiska testowe}

Weryfikacja eksperymentalna została przeprowadzona na dwóch odrębnych środowiskach uruchomieniowych. Pierwsze z~nich, nazywane dalej \textit{rzeczywistym}, składa się z~dwunastu urządzeń mobilnych połączonych w~jedną sięć lokalną sieć bezprzewodową. (zob.~rysunek~\ref{fig:devices_list}).

\begin{figure}[!h]
	\centering
		\includegraphics[width=11cm]{devices_list}
	\caption{Lista urządzeń mobilnych wchodzących w~skład pierwszego środowiska uruchomieniowego, wygenerowana przez \textit{Mobile Cloud Control Center}.}
	\label{fig:devices_list}
\end{figure}

Drugie z~wykorzystanych środowisk uruchomieniowych, nazywane dalej \textit{wirtualnym}, zostało oparte o dwadzieścia komputerów z~procesorami Intel Q6600 Core 2~Quad, na których zostały uruchomione maszyny wirtualne z~Androidem x84. Na każdym kompuetrze uruchomiono trzy maszyny wirtualne, aby umożliwić płynne działanie każdego z~wątków\footnote{Uruchomienie czterech maszyn wirtualnych skutkowało zbyt częstym wywłaszczaniem procesów, aby można było uznać pomiary przeprowadzone na takim środowisku za wiarygodne.}. Komputery były połączone w~lokalną sieć przewodową.

\subsection{Wyniki testów wydajnościowych}

Przyspieszenie i~efektywność wykonania pierwszego z~zadań obliczeniowych, wyznaczania zbioru Mandelbrota (Mandelbrot.js), zostało zmierzone w~następującej konfiguracji: dwanaście urządzeń mobilnych, przy czym chmura była stopniowo zwiększana o~jedno urządzenie po każdym wykonaniu pomiarów. Zadanie obliczeniowe zostało zaimplementowane w~sposób uniemożliwiający manipulowanie rozmiarem problemu, który jest ustalony i~wynosi 810~000 punktów (zob.~rysunek~\ref{fig:mandelbrot_actual_speedup}).

\begin{figure}[!h]
	\centering
		\includegraphics[width=12cm]{mandelbrot_actual_speedup}
	\caption{Wykres przyspieszenia wykonania zadania obliczeniowego Mandelbrot.js, dla rozmiaru problemu wynoszącego 810~000 punktów, w~zależności od ilości urządzeń mobilnych wykonujących zadanie obliczeniowe.}
	\label{fig:mandelbrot_actual_speedup}
\end{figure}

Wykres, obrazujący zmierzone przyspieszenie, pozwolił zaobserwować bardzo ciekawą zależność. Początkowe zwiększanie ilości urządzeń daje bardzo duże zyski wydajnościowe, większe niż wskazuje na to ilość urządzeń wchodzących w~skład chmury obliczeniowej (zob.~rysunek~\ref{fig:mandelbrot_actual_speedup}). Charakterystyka sposobu zrównoleglenia obliczeń w~zadaniu Mandelbrot.js wyklucza możliwość superliniowego przyspieszenia wykonania zadania, przy założeniu że narzut związany z~komunikacją skaluje się liniowo. Oznacza to, że podczas gdy występowało superliniowe przyspieszenie, rozmiar wysyłanego komunikatu przekraczał dopuszczalne wartości dla urządzenia mobilnego (wiadomość z pełnym rozwiązaniem dla 810~000 punktów zajmuje 2.1MB). 

Aby wyznaczyć rozmiar komunikatu, który nie powoduje nadmiernych strat wydajnościowych przedstawiono wykres efektywności dla tej części pomiarów, w~której superliniowość zanikła (zob.~rysunek~\ref{fig:mandelbrot_actual_effectiveness}).

\begin{figure}[!h]
	\centering
		\includegraphics[width=11cm]{mandelbrot_actual_effectiveness_adjusted}
	\caption{Wykres efektywności wykonania zadania obliczeniowego Mandelbrot.js, dla rozmiaru problemu wynoszącego 810~000 punktów, w~zależności od zawężonej ilości (od~5 do~12) urządzeń mobilnych wykonujących zadanie obliczeniowe.}
	\label{fig:mandelbrot_actual_effectiveness}
\end{figure}
 
Począwszy od pięciu urządzeń mobilnych, wysyłających komunikat o rozmiarze ok.~0.4~MB, zmierzona efektywność jest zgodna ze spodziewaną dla zadania o~tego typu sposobie zrównoleglenia obliczeń. Oznacza to, że szacowany maksymalny rozmiar przesyłanego komunikatu podczas wykonywania zadania obliczeniowego na \textit{Mobile Cloud Platform} wynosi ok.~0.4~MB (dokładna wartość jest zależna od modelu urządzenia mobilnego).

Przyspieszenie i efektywność wykonania drugiego zadania, tj. wyznaczania przybliżonej wartości liczby Pi metodą Monte Carlo (pi.js), zostały zmierzone na podobnej konfiguracji co zadanie Mandelbrot.js. Jedyną różnicą w niniejszej konfiguracji uruchomieniowej jest sposób zwiększania chmury, która przyjmowała kolejno rozmiary 6~i~12 urządzeń. Natura zadania pi.js pozwoliła na uzależnienie pomiarów od ilości punktów, które mają zostać wylosowane w~celu wyznaczenia liczby Pi. Pomiary zostały przeprowadzone dla 300~000, 3~000~000 i 30~000~000 punktów (zob.~rysunek~\ref{fig:pi_actual_speedup}). 

\begin{figure}[!h]
	\centering
		\includegraphics[width=11cm]{pi_actual_speedup}
	\caption{Wykres przyspieszenia wykonania zadania obliczeniowego pi.js, dla rozmiaru problemu wynoszącego kolejno 300~000, 3~000~000 i 30~000~000 punktów, w~zależności od ilości urządzeń mobilnych wykonujących zadanie obliczeniowe.}
	\label{fig:pi_actual_speedup}
\end{figure}

Komunikacja prowadzona w~ramach zadania pi.js oparta została o~wirtualną topologię w~postaci drzewa binarnego (zob.~rysunek~\ref{fig:binary}). Wiedząc, że komunikacja polega na propagowaniu informacji w~górę drzewa, sumaryczna ilość komunikatów wysłana pomiędzy urządzeniami jest równa liczbie urządzeń wchodzących w~skład chmury obliczeniowej. Długość najdłuższej sekwencji wykonania pełnej komunikacji w~górę drzewa równa się wysokości drzewa binarnego.

Gdy problem jest stosunkowo niewielkich rozmiarów (ok.~100~000 punktów) dodawanie kolejnych urządzeń mobilnych nie ma sensu - można zauważyć spadek efektywności wykonania zadania obliczeniowego do 18\% przy podwojeniu ilości początkowych urządzeń z~sześciu do dwunastu (zob.~rysunek~\ref{fig:pi_actual_effectiveness}). 

\begin{figure}[!h]
	\centering
		\includegraphics[width=11cm]{pi_actual_effectiveness}
	\caption{Wykres efektywności zadania obliczeniowego pi.js, dla rozmiaru problemu wynoszącego kolejno 300~000, 3~000~000 i 30~000~000 punktów, w~zależności od ilości urządzeń mobilnych wykonujących zadanie obliczeniowe.}
	\label{fig:pi_actual_effectiveness}
\end{figure}

Rozmiar problemu, który pozwala całkowicie zaniedbać narzut komunikacyjny (zmierzona efektywność wynosi ok.~100\%) można szacować na ok.~2~500~000 punktów na jedno urządzenie\footnote{Wartość została oszacowana poprzez podzielenie ilości punktów dla których zmierzona efektywność utrzymywała się na poziomie 100\% przez ilość urządzeń dla której została zaobserwowana.}. Nawiązując do prawa Gustafsona, omówionego w~drugim rozdziale, można twierdzić że problemy o~podobnej ziarnistości do pi.js można zrównoleglać prawie idealnie, w~chmurze obliczeniowej o~dowolnych rozmiarach opartej o~\textit{Mobile Cloud Platform}, przy odpowiednim zwiększaniu rozmiaru problemu obliczeniowego.

Ostatnie zadanie obliczeniowe, rozwiązywanie TSP za pomocą algorytmu genetycznego (tsp.js), zostało przetestowane w~dwóch konfiguracjach środowiska uruchomieniowego. Na \textit{rzeczywistym} środowisku uchomieniowym rozmiar populacji został ustawiony na 300 osobników, rozwiązywany problem składał się ze 131 wierzchołków, a~rozwiązanie uznawane za satysfakcjonujące to ścieżka o~długości~605~(o~7.3\% dłuższa od rozwiązania optymalnego). Rozmiar chmury obliczeniowej przyjmował kolejno~4, 8~i~12 urządzeń mobilnych (zob.~rysunek~\ref{fig:tsp_actual_effectiveness}).

\begin{figure}[!h]
	\centering
		\includegraphics[width=11cm]{tsp_actual_effectiveness}
	\caption{Wykres efektywności wykonania zadania obliczeniowego tsp.js, wykonanego na \textit{rzeczywistym} środowisku uruchomieniowym, dla problemu znalezienia ścieżki o~długości nie większej niż 605, w~zależności od ilości urządzeń mobilnych wykonujących zadanie obliczeniowe.}
	\label{fig:tsp_actual_effectiveness}
\end{figure}

Przeprowadzone pomiary efektywności zadania obliczeniowego (tsp.js) wykazują superliniowość. Polega ona na utrzymującym się powyżej 100\%, konsekwentnym wzroście efektywności wykonania zadania w~zależności od ilości urządzeń.

Testy przyspieszenia i~efektywności wykonania zadania rozwiązującego TSP na środowisku \textit{wirtualnym} zostały przeprowadzone w~następującej konfiguracji: rozmiar populacji został ustawiony na 500 osobników, rozwiązywany problem składał się ze 131 wierzchołków (taki sam problem został wykorzystany do testów na środowisku \textit{rzeczywistym}), a~rozwiązanie uznawane za satysfakcjonujące to ścieżka o~długości~595~(o~5.5\% odbiegająca od rozwiązania optymalnego) i~590~(rozwiązanie o~4.6\% oddalone od optymalnego). Chmura obliczeniowa przyjmowała kolejno rozmiar 15, 30, 45 i 60 wirtualizowanych urządzeń mobilnych.

Słabe przyspieszenie w~wykonywaniu algorytmu tsp.js, dla problemu o~długości satysfakcjonującej ścieżki wynoszącej 595 oznacza, że stosunek ilości urządzeń mobilnych wchodzących w~skład chmury obliczeniowej, do rozmiaru problemu jest zbyt duży. W~tym przypadku trudność zadania obliczeniowego rozwiązywanego algorytmem genetycznym okazała się zbyt pesymistycznie dobrana w~stosunku do potencjału wydajnościowego chmury obliczeniowej (zob.~rysunek~\ref{fig:tsp_virtual_speedup}).

\begin{figure}[!h]
	\centering
		\includegraphics[width=11.5cm]{tsp_virtual_speedup}
	\caption{Wykres przyspieszenia zadania obliczeniowego tsp.js, wykonanego na \textit{wirtualnym} środowisku uruchomieniowym, dla problemów znalezienia ścieżki o~długości nie większej niż 595 i 590, w~zależności od ilości urządzeń mobilnych wykonujących zadanie obliczeniowe.}
	\label{fig:tsp_virtual_speedup}
\end{figure}

Zupełnie inaczej zachowała się chmura obliczeniowa przy rozwiązywaniu trudniejszego problemu, polegającego na znalezieniu ścieżki o~długości nie większej niż 590. Począwszy od chmury obliczeniowej składającej się z 45 maszyn wirtualnych można zaobserwować superliniowość (zob.~rysunek~\ref{fig:tsp_virtual_effectiveness}). 

\begin{figure}[!h]
	\centering
		\includegraphics[width=11.5cm]{tsp_virtual_effectiveness}
	\caption{Wykres efektywności zadania obliczeniowego tsp.js, wykonanego na \textit{wirtualnym} środowisku uruchomieniowym, dla problemów znalezienia ścieżki o~długości nie większej niż 595 i~590, w~zależności od ilości urządzeń mobilnych wykonujących zadanie obliczeniowe.}
	\label{fig:tsp_virtual_effectiveness}
\end{figure}

Na podstawie zależności zaobserwowanych podczas weryfikacji eksperymentalnej można twierdzić, że stosując algorytmy genetyczne na chmurze obliczeniowej opartej o~\textit{Mobile Cloud Platform} i~dobierając odpowiedni rozmiar chmury obliczeniowej do poziomu trudności problemu, można zawsze uzyskać wydajność, której nie udało by się uzyskać z~jednakowego zwiększania wydajności pojedynczej maszyny obliczeniowej (wniosek ten wynika bezpośrednio z~zaobserwowanej superliniowości).

\subsection{Pomiary mocy obliczeniowej}

Aby móc osadzić chmurę obliczeniową, zestawioną za pomocą \textit{Mobile Cloud Platform}, w~realiach prowadzenia obliczeń na wielką skalę, należy dokonać próby oszacowania wydajności wspomnianej chmury obliczeniowej. W~celu wyznaczenia punktu odniesienia, potrzebnego do oszacowania wydajności przykładowego urządzenia mobilnego napisano analogiczne zadanie do pi.js w~języku C++, z~wykorzystaniem protokołu komunikacyjnego MPI. Następnie dla jednakowych rozmiarów problemu, zmierzono czas wykonywania zadania na \textit{rzeczywistym} środowisku uruchomieniowym oraz na jednym wątku komputera~PC, z~procesorem Intel~i7-3370K, o~ częstotliwości wynoszącej~3,50GHz (zob.~rysunek~\ref{fig:performance_comparison}).

\begin{figure}[!h]
	\centering
		\includegraphics[width=11cm]{performance_comparison}
	\caption{Wykres czasu wyznaczania liczby Pi metodą Monte Carlo na \textit{rzeczywistym} środowisku uruchomieniowym oraz na jednym wątku procesora Intel~i7-3370K~@3,50GHz.}
	\label{fig:performance_comparison}
\end{figure}

Pomiary wydajności dla 100, 200 i 300 milionów punktów wykazują pełne zrównoleglenie zarówno na środowisku \textit{rzeczywistym} jak i~na komputerze PC. Uśredniając te pomiary można wyznaczyć mnożnik wydajności $M$ urządzenia mobilnego w~stosunku do procesora Intel~i7-3370K wykonującego jednowątkowe zadanie:

\begin{center}
$M \approx \frac{1,75 + 3,60 + 5,35}{3} \div\frac{(10,10 + 20,25 + 30,61)\times12}{3} \approx 0.0146$
\end{center}

Za pomocą narzędzia IntelBurnTest v2.54\footnote{IntelBurnTest to narzędzie służące do prowadzenia pomiarów wydajności procesora CPU. Jest dostępne do pobrania pod następującym adresem: http://www.majorgeeks.com/files/details/intelburntest.html.} zmierzono wydajność wykonywania jednego wątku na procesorze Intel~i7-3370K i~wynosi ona 25,47~GFlops. Po pomnożeniu tej wartości przez mnożnik $M$, szacowana wydajność przykładowego urządzenia mobilnego wynosi $\sim$0,37~GFlops\footnote{0,37~GFlops jako moc obliczeniową przykładowego telefonu wykonującego zadanie obliczeniowe za pośrednictwem \textit{Mobile Cloud Platform} należy traktować jedynie jako wartość szacunkową.}.

Nawiązując do rozdziału trzeciego\footnote{W~rozdziale trzecim uwgzlędniono moc obliczeniową największych superkomputerów świata według listy TOP~500.}, aby zrównoważyć moc obliczeniową największego obecnie superkomputera świata, Tianhe-2 o~mocy obliczeniowej wynoszącej 33,9~PFlops, należy wykorzystać ok.~91,6~milionów urządzeń mobilnych. Aby uzyskać jednakową moc obliczeniową, do obecnie deklarowanej dla systemu gridowego Bitcoint network, należy wykorzystać ok.~6,3~bilionów urządzeń mobilnych, co teoretycznie nie jest jeszcze możliwe, biorąc pod uwagę obecną całkowitą ilość urządzeń mobilnych na świecie. Drugi system gridowy na świecie pod względem mocy obliczeniowej (BONIC) jest już znacznie mniej wydajny i~aby dokonać próby zastąpienia go za pomocą \textit{Mobile Cloud Platform} należałoby wykorzystać już tylko ok.~158,4~milionów urządzeń mobilnych.

\section{Podsumowanie}

Platforma obliczeniowa dla urządzeń mobilnych \textit{Mobile Cloud Platform} umożliwia wykonywanie zadań obliczeniowych, przy spełnieniu założeń o~samozarządzalności i~wieloplatformowości. Potencjalnie, \textit{Mobile Cloud Platform}, może pozwolić na budowanie heterogenicznych chmur obliczeniowych, w~których uczestniczyć będą urządzenia mobilne oparte o~systemy operacyjne, pokrywające łącznie prawie~100\% rynku urządzeń mobilnych. Dodatkowo, od momentu wprowadzenia adresacji sieciowej opartej o~IPv6, samozarządzalne chmury obliczeniowe będą mogły być zestawiane z~urządzeń mobilnych łączących się protokołami transmisji danych, co daje możliwość budowania chmur obliczeniowych rozpostartych na rozległych przestrzeniach geograficznych.

Projektowanie algorytmów, które mają być wykonywane na chmurze obliczeniowej wspieranej przez \textit{Mobile Cloud Platform}, polega na unikaniu wysyłania dużych komunikatów (>~0,4MB), oraz zwróceniu uwagi na utrzymanie dużej ziarnistości. Aby można było efektywnie wykorzystać dużą ilość dostępnych urządzeń mobilnych należy dobrze dostosować ilość urządzeń wchodzących w~skład chmury obliczeniowej do rozmiaru rozwiązywanego problemu. 

Biorąc pod uwagę stale rosnącą liczbę urządzeń mobilnych (tylko w~zeszłym roku wprowadzono na rynek miliard nowych urządzeń mobilnych) oraz ich wykładniczo rosnącą moc obliczeniową, można podejrzewać, że w~niedalekiej przyszłości w~oparciu o takie narzędzia jak \textit{Mobile Cloud Platform} będzie możliwe konstruowanie mobilnych chmur obliczeniowych o~wydajności przewyższającej moc obliczeniową obecnych superkomputerów i~systemów gridowych. Dodatkowo należy pamiętać o~zasadniczej zalecie chmury obliczeniowej złożonej z~urządzeń mobilnych, która polega na możliwości pobieraniu wybranych parametrów z~szerokiej gamy sensorów znajdujących się w~urządzeniach mobilnych rozlokowanych po całym świecie.


