\chapter{Konepcja projektu Mobile Cloud Platform}
\label{cha:rozdzial5}
\textit{Mobile Cloud Platform} to platforma przeznaczona na urządzenia mobilne wspierająca wykonywanie obliczeń rozproszonych. Jej głównym zadaniem jest umożliwienie zestawienia chmury obliczeniowej z~urządzeń mobilnych oraz wspieranie zadania wykonywanego na tak zestawionej chmurze. Jednym z~istotnych założeń jest możliwość sięgnięcia do sensorów urządzenia na którym aktualnie realizowana jest konkretna część zadania oraz komunikacji z~pozostałymi urządzeniami wykonującymi to samo zadanie. Po wykonaniu zadania istnieje możliwość skorzystania z~jego wyników oraz uruchomienia kolejnych zadań na zestawionej chmurze obliczeniowej.

Użytkownicy, dla których platforma jest przeznaczona, nazywani dalej \textit{Aktorami}, prezentują się następująco:
\begin{itemize}
    \item \textbf{Administrator chmury} - osoba zarządzająca chmurą, mająca kontrolę nad zestawianymi chmurami obliczeniowymi oraz zadaniami na nich wykonywanymi.
    \item \textbf{Użytkownik końcowy} - osoba mająca bezpośredni kontakt z~urządzeniem mobilnym, uruchamia aplikację i~dostaje informacje o~bieżącym zadaniu w~którym uczestniczy jego aparat. Ponadto wspomniany \textit{Aktor} sprawuje kontrolę nad platformą tylko z~punktu widzenia jednego urządzenia mobilnego. W~dalszych częściach dokumentu urządzenie mobilne, z~którym \textbf{użytkownik końcowy} ma bezpośredni kontakt, będzie nazywane \textit{lokalnym urządzeniem mobilnym}.
    \item \textbf{Prowadzący badanie} - osoba odpowiedzialna za zadanie wykonywane na chmurze obliczeniowej, zarządza przebiegiem zadania, obserwuje wykonanie i~wyniki zadania.
\end{itemize}

\section{Wymagania}

W~podsumowaniach poprzednich rozdziałów konsekwentnie zbierano fragmentaryczne wymagania, które można stawiać narzędziom wspierającym prowadzenie obliczeń równoległych. Wszystkie częściowe wymagania, po zebraniu w całość, można podzielić na:
\begin{itemize}
    \item \textbf{funkcjonalne} (w standardowym rozumieniu) - funkcjonalność, którą tworzona platforma ma realizować,
    \item \textbf{koncepcyjne} - niezwiązane z~funkcjonalnością, dotykają części implementacyjnej z~punktu widzenia sposobu działania.
\end{itemize}

\subsection{Wymagania funkcjonalne}

Aby szczegółowe wymagania mogły być dobrze zrozumiane, przedstawiono je w~postaci akcji, jakie mogą wykonywać \textit{Aktorzy} zdefiniowani we wstępie do podrozdziału.

Wymagania funkcjonalne z~punktu widzenia \textbf{Administratora chmury}:
\begin{enumerate}
    \item Wykrycie urządzeń gotowych do prowadzenia obliczeń w~sieci lokalnej.
    \item Zbudowanie chmury obliczeniowej ze znanych urządzeń mobilnych\footnote{Znane urządzenie mobilne to takie, które jest dostępne w~osiągalnej sieci.}.
    \item Uruchomienie zadania na chmurze obliczeniowej.
    \item Umożliwienie pobrania wyników ukończonego zadania, które wcześniej zostało uruchomione na wybranej chmurze obliczeniowej.
    \item Umożliwienie monitorowania stanu chmury pod względem rozmiaru\footnote{Rozmiar chmury obliczeniowej definiowany jest przez ilośc urządzeń mobilnych, z~których się składa.}, kształtu\footnote{Kształt chmury obliczeniowej obrazowany jest w~postaci grafu połączeń pomiędzy urządzeniami, z~których się składa.}, urządzeń, z~których składa się chmura obliczeniowa oraz aktualnie wykonywanych zadań.
    \item Deaktywacja chmury.
\end{enumerate}

Patrząc oczami \textbf{Użytkownika końcowego}, \textit{Mobile Cloud Platform} powinna umożliwiać funkcjonalności wypisane poniżej:
\begin{enumerate}[resume]
    \item Dołączenie urządzenia do chmury.
    \item Uruchomienie zadania na urządzeniu połączonym do chmury\footnote{Wymaganie funkcjonalne nr~8 różni się od wymagania funkcjonalnego nr~3 punktem widzenia. W wymaganiu~7. aktor uruchamia zadania na urządzeniu mobilnym, a w wymaganiu~3. aktor uruchamia zadanie na chmurze.}.
    \item Umożliwienie monitorowania informacji o~zadaniu wykonywanym przez \textit{lokalne urządzenie mobilne}.
    \item Przedstawienie informacji o~chmurze, w~skład której wchodzi \textit{lokalne urządzenie mobilne}.
\end{enumerate}

Wymagania funkcjonalne, w~postaci zbioru akcji \textit{Aktora} \textbf{Prowadzącego badanie}, dotyczą redagowania kodu źródłowego zadania obliczeniowego oraz sposobu korzystania z~zestawionej chmury obliczeniowej. Zostały one zdefiniowane następująco:

\begin{enumerate}[resume]
    \item Możliwość redagowania zadania obliczeniowego w~oparciu o~dobrze zdefiniowane API\footnote{API (skr. od ang. Application Programming Interface) - ściśle określony zestaw reguł, w~jaki programy komunikują się między sobą.}, umożliwiające: komunikację pomiędzy urządzeniami; komunikację urządzeń ze zdalnymi serwerami; pobieranie wartości wybranych parametrów z~sensorów urządzenia mobilnego, na którym zadanie jest wykonywane.
    \item Możliwość pobrania wyników zadania z~chmury obliczeniowej.
    \item Możliwość ustalenia filtru na urządzenia wewnątrz chmury obliczeniowej, na podstawie którego zostaną wybrane urządzenia, na których zadanie obliczeniowe ma zostać wykonane. Przykładowe opcje filtrowania: współrzędne geograficzne; występowanie określonych sensorów.
\end{enumerate}

\subsection{Wymagania koncepcyjne}

Potrzeba wyspecyfikowania tego rodzaju wymagań została częściowo opisana w podsumowaniu do rozdziału trzeciego. Adresuje ona m.in. jeden z~atrybutów systemu gridowego: używanie standardowych protokołów i~interfejsów ogólnego zastosowania~\cite{Grid}.

Wymagania koncepcyjne zostały zdefiniowane następująco:
\begin{enumerate}
    \item Protokoły komunikacyjne wykorzystane w~ramach \textit{Mobile Cloud Platform} muszą być publiczne. Specyfikacja protokołów komunikacyjnych ma być przedstawiona w~sposób umożliwiający ich zastosowanie przez zewnętrzne komponenty\footnote{Specyfikacja protokołów komunikacyjnych została zamieszczona w~rozdziale 6.3~-~Specyfikacja protokołów komunikacyjnych.}.
    \item Urządzenia wchodzące w~skład chmury mają udostępniać API, pozwalające na zdalną kontrolę w~zakresie: uruchamiania zadania, zbierania wyników, dołączania urządzenia do chmury, odłączania urządzenia od chmury.
    \item Specyfikacja API, wykorzystywanego przez protokoły komunikacyjne oraz zdalny dostęp do urządzeń, ma być kompleksowa i~dostarczona wraz z produktem \textit{Mobile Cloud Platform}\footnote{Specyfikacja API wykorzystywanego przez protokoły komunikacyjne oraz zdalny dostęp do urządzeń mobilnych została umieszczona w dodatku A.}.
\end{enumerate}

\section{Założenia}

W~niniejszym podrozdziale przedstawione zostały założenia, na bazie których \textit{Mobile Cloud Platform} jest zbudowana. Opisują one decyzje projektowe dotyczące zasady działania i~kierunków rozwoju obranych podczas prac projektowych. Zostały one podzielone na dwie główne kategorie:
\begin{itemize}
    \item samozarządzalność (komunikacja peer-to-peer) - możliwość zestawienia chmury obliczeniowej bez konieczności utrzymywania zewnętrznych serwerów,
    \item wieloplatformowość - możliwość uruchomienia \textit{Mobile Cloud Platform} na urządzeniach mobilnych z~heterogenicznymi systemami operacyjnymi, tj.~Android, iOS, Windows Phone, etc.
\end{itemize}

\subsection{Samozarządzalność chmury obliczeniowej}

\textit{Mobile Cloud Platform} powinna udostępniać pełną funkcjonalność bez konieczności korzystania z~żadnych usług oferowanych przez urządzenia niewchodzące w~skład chmury obliczeniowej. Oznacza to pełną samozarządzalność platformy w obrębie urządzeń mobilnych, na których chmura obliczeniowa jest zestawiana.

Korzyści z przyjęcia takiego założenia są następujące:
\begin{itemize}
    \item brak konieczności utrzymywania serwerów zarządzających mobilnymi chmurami obliczeniowymi,
    \item brak konieczności zapewnienia dostępu do internetu urządzeniom mobilnym formującym chmurę obliczeniową.
\end{itemize}

\begin{figure}[h!]
	\centering
		\includegraphics[width=\linewidth]{cloud_tree}
	\caption{Hierarchiczna struktura samozarządzalnej mobilnej chmury obliczeniowej.}
	\label{fig:cloud_tree}
\end{figure}

Idea stojąca za działaniem samozarządzalnej chmury obliczeniowej polega na wprowadzeniu możliwości wysyłania bezpośrednich komunikatów pomiędzy urządzeniami. Przyjęto, że budowana chmura obliczeniowa przyjmuje kształt drzewiastej struktury hierarchicznej, w~której każdy korzeń ma najwyżej pięcioro dzieci (zob. rysunek ~\ref{fig:cloud_tree}). Ogólna zasada działania zastosowanych protokołów komunikacyjnych prezentuje się następująco:

\begin{enumerate}
    \item Dołączenie urządzenia do chmury obliczeniowej - urządzenie chcące dołączyć do chmury wysyła odpowiedni komunikat do jednego z~urządzeń wchodzących w~skład chmury obliczeniowej; następnie zgłoszenie jest delegowane do korzenia całej chmury obliczeniowej. Decyduje on, do którego z~poddrzew nowe urządzenie ma być dołączone jako liść; decyzja ta jest podejmowana uwzględniając ilość urządzeń znajdujących się w~każdym poddrzewie; po dołączeniu nowego urządzenia do wybranego poddrzewa, chmura wysyła komunikat do nowo podłączonego urządzenia z~informacją o~jego bezpośrednim korzeniu,
    \item Uruchomienie zadania na chmurze obliczeniowej - urządzenie, które chce uruchomić zadanie na chmurze obliczeniowej, w~której skład wchodzi, rozsyła komunikat o~nowym zadaniu do wszystkich swoich sąsiadów; komunikat jest propagowany po całym drzewie chmury obliczeniowej; urządzenie, które nie może dalej propagować komunikatu odsyła z~powrotem komunikat o~tym, czy chce wziąć udział w~zadaniu obliczeniowym biorąc pod uwagę zdefiniowane warunki stawiane urządzeniom mobilnym; komunikat składający się z~listy identyfikatorów urządzeń wchodzących w~skład nowego zadania obliczeniowego jest propagowany do urządzenia, które zapoczątkowało uruchomienie zadania; po zebraniu listy urządzeń chętnych do wykonania nowego zadania informacja zbiorcza o~wszystkich identyfikatorach urządzeń wchodzących w~skład zadania jest propagowana po drzewie wirtualnym utworzonym tylko z~urządzeń, które będą wykonywały dane zadanie obliczeniowe.
\end{enumerate}

Podstawową konsekwencją w~przyjęciu wyżej opisanej koncepcji samozarządzalności jest wymaganie technologiczne, które polega na umożliwieniu bezpośredniej komunikacji pomiędzy urządzeniami mobilnymi. Przy obecnie stosowanej infrastrukturze sieciowej Internetu opartej o~adresację typu IPv4 nie ma możliwości przesyłania komunikatów bezpośrednio do urządzeń mobilnych. Jest to spowodowane prywatną adresacją urządzeń oddzielonych protokołem NAT\footnote{NAT (skr. od ang. Network Address Translation) - technika przesyłania ruchu sieciowego poprzez router, która wiąże się ze zmianą źródłowych lub docelowych adresów IP.} od sieci publicznej. Problem ten jest rozwiązywany poprzez wprowadzenie adresacji IPv6\footnote{IPv6 (skr. od ang. Internet Protocol version 6) – protokół komunikacyjny, który jest następcą protokołu IPv4.}, w której NAT nie jest potrzebny, a~każdy interfejs ma publiczny adres IP~\cite{RFC4193}.

Adresacja sieciowa w~wersji IPv6 podlega konsekwentnemu wdrażaniu przez różnych dostawców internetowych. Firma T-Mobile w~Stanach Zjednoczonych ustanowiła adresację IPv6 jako domyślną konfigurację wszystkich nowych urządzeń z~zainstalowanym systemem operacyjnym Android w~wersji~4.3, lub wyższej~\cite{Android_IPv6}. Od grudnia 2013 lista telefonów skonfigurowanych domyślnie na IPv6 wygląda następująco~\cite{IPv6_devices}:

\begin{itemize}
    \item Samsung Galaxy Note 3,
    \item Galaxy Light,
    \item MetroPCS Samsung Mega
    \item Google Nexus 5.
\end{itemize}

Wszystkie powyższe informacje pozwalają twierdzić, że w~niedalekiej przyszłości będzie można tworzyć samozarządzalne chmury mobilne oparte o~telefony z~adresacją IPv6, komunikujące się za pośrednictwem Internetu. W~ramach niniejszej pracy magisterskiej wykonano testy tworzenia chmur obliczeniowych na telefonach z~adresacją IPv4, komunikujących się za pośrednictwem sieci lokalnej.

\subsection{Wieloplatformowość produktu}

Wieloplatformowość (z~ang. cross platform) to atrybut nadawany oprogramowaniu, które jest zaimplementowane i~gotowe do pracy na wielu systemach operacyjnych~\cite{CrossPlatform}. W~odniesieniu do produktu \textit{Mobile Cloud Platform} wieloplatformowość oznacza możliwość zestawienia mobilnej chmury obliczeniowej i~prowadzenia obliczeń na urządzeniach mobilnych pod kontrolą różnych systemów operacyjnych, tj.~Android, iOS czy Windows Phone, bez względu na konfigurację sprzętową.

Aby zauważyć korzyści wynikające z~przyjęcia wymagania o~wieloplatformowości do projektu \textit{Mobile Cloud Platform} należy zwrócić uwagę na dane statystyczne zawarte w~rozdziale czwartym. Aktualnie największy udział w~rynku urządzeń mobilnych ma system Android (ok.~80\%). Z~kolei urządzenia pracujące pod kontrolą systemu iOS oraz Windows Phone mają aktualnie około~20\% udziału w~rynku. Oznacza to, że przy dostarczeniu implementacji \textit{Mobile Cloud Platform} na wszystkie wyżej wymienione systemy operacyjne można zaadresować niemal~100\% urządzeń dostępnych na rynku. Koncepcja uwzględnienia wieloplatformowości w~produkcie jest adresowana poprzez oparcie komunikacji pomiędzy urządzeniami wchodzącymi w~skład chmury obliczeniowej o~standardowe protokoły komunikacyjne. Dodatkowo, wymaganie o~wieloplatformowości produktu jest wspierane wyborem języka JavaScript, jako język w~którym napisany jest kod zadania obliczeniowego. Język JavaScirpt jest obsługiwany przez wszystkie systemy operacyjne dedykowane na urządzenia mobilne. Biorąc pod uwagę aktualne trendy, JavaScript prawdopodobnie będzie coraz częściej wykorzystywany do zdalnego uruchamiania kodu na urządzeniach mobilnych.

Wybierając HTTP\footnote{HTTP (skr. od ang. Hypertext Transfer Protocol) – protokół aplikacji przeznaczony dla rozproszonych systemów opartych o~hipermedia.} jako protokół komunikacyjny warstwy aplikacji można dostarczyć implementacji na wszystkie wymagane systemy operacyjne. Technologie, służące kolejno do hostowania serwera HTTP oraz wysyłania zapytań w~języku HTTP, przedstawione z~podziałem na systemy operacyjne, prezentują się następująco:
\begin{itemize}
    \item \textbf{Android} - otwarta biblioteka o~nazwie NanoHttpd~\cite{NanoHttpd}; klasa RestTemplate z~pakietu org.springframework.web.client~\cite{RestTemplate},
    \item \textbf{iOS} - otwarta biblioteka o~nazwie CocoaHTTPServer~\cite{CocoaHTTPServer}, klasa NSURLConnection~\cite{NSURLConnection},
    \item \textbf{Windows Phone} - implementacja własnego serwera HTTP na bazie klasy StreamSocketListener znajdującej się w~następującej nazw: Windows.Networking.Sockets~\cite{WindowsPhoneHttpServer}; klasa HttpWebRequest z~zestawu narzędzi sieciowych dostępnych w~pakiecie System.Net~\cite{WindowsPhoneWebRequests}.
\end{itemize} 

Narzędzia wspierające wykonywanie kodu napisanego w~języku JavaScript na urządzeniu mobilnym, przedstawione z~podziałem na systemy operacyjne, zebrano poniżej:
\begin{itemize}
    \item \textbf{Android} - klasa o~nazwie WebView z~pakietu android.webkit~\cite{WebView},
    \item \textbf{iOS} - framework o~nazwie JavaScriptCore~\cite{JavaScriptCore},
    \item \textbf{Windows Phone} - biblioteka do kontroli JavaScript'u~\cite{WindowsPhoneJavaScirpt}.
\end{itemize} 

Z~uwagi na ograniczony czas na implementację \textit{Mobile Cloud Platform}, w~ramach niniejszej pracy wybrano system operacyjny Android w~wersji~2.3.3 (i~wyższe) jako ten, na który zostanie dostarczona implementacja aplikacji mobilnej w postaci jednego z komponentów \textit{Mobile Cloud Platform}. Wyboru systemu operacyjnego Android dla aplikacji mobilnej dokonano ze względu na największy udział na rynku urządzeń mobilnych.

\section{Komponenty Mobile Cloud Platform}

Biorąc pod uwagę zdefiniowane wymagania oraz przyjęte założenia w~toku prac projektowych wyróżniono dwa zasadnicze komponenty \textit{Mobile Cloud Platform}. Pierwszy z~nich to aplikacja mobilna dla systemu Android, która została nazwana \textit{Android Client}. Kolejny komponent to serwis internetowy, który pozwala: sprawować centralną kontrolę nad zestawianymi chmurami obliczeniowymi, uruchamiać zadania obliczeniowe i~monitorować przebieg prowadzonych obliczeń. W~dalszych częściach pracy serwis ten jest nazywany \textit{Mobile Cloud Control Center}.

\subsection{Android Client}

Aplikacja mobilna jest najważniejszym elementem produktu \textit{Mobile Cloud Platform} pod względem wspierania funkcjonalności związanych z~zestawianiem chmury mobilnej i~wykonywaniem zadań obliczeniowych. Dodatkowo, w~ramach wymagań przedstawionych w~postaci akcji aktora nazwanego \textbf{Użytkownikiem końcowym}, aplikacja mobilna ma udostępniać prosty, graficzny interfejs użytkownika.

\subsection{Mobile Cloud Control Center}

\textit{Mobile Cloud Control Center} to serwis internetowy, który umożliwia monitorowanie urządzeń mobilnych z~uruchomioną aplikacją \textit{Android Client}. Został stworzony w~celu wspierania wymagań funkcjonalnych przedstawionych w~postaci akcji \textbf{Administratora chmury}. Główną cechą serwisu jest nastawienie na ułatwienie korzystania z~\textit{Mobile Cloud Platform} poprzez wprowadzenie wysoce funkcjonalnego graficznego interfejsu użytkownika. W~toku prac przyjęto założenie, że \textit{Mobile Cloud Control Center} będzie uruchamiany lokalnie, ze względu na problem publicznej dostępności urządzeń mobilnych z~adresem sieciowym w~wersji~IPv4.

\textit{Mobile Cloud Control Center} jest ściśle powiązany z~aplikacją \textit{Android Client} poprzez korzystanie z~wystawianego przez tą aplikację REST~API. Specyfikacja publicznego API aplikacji \textit{Android Client} została zamieszczona w~dodatku~A.


