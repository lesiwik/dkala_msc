\chapter{Problematyka obliczeń rozproszonych}
\label{cha:rozdzial2}

Obliczenia można podzielić na równoległe i~sekwencyjne. Równoległe, w dużym uproszczeniu, polegają na wykonywaniu kilku instrukcji jednocześnie. Algorytm sekwencyjny można zrównoleglić na różne sposoby i~każdy z nich może dać inny wzrost wydajnościowy (tzw. speed up) przy wykonaniu na wielu procesorach.

\begin{quotation}
Równoległy komputer to zbiór procesorów, które mogą pracować wspólnie, aby rozwiązać problem obliczeniowy~\cite{Ian_Foster}.
\end{quotation}

Powyższa definicja komputera równoległego jest bardzo rozległa. Obejmuje ona zarówno superkomputery składające się z~setek tysięcy procesorów oraz rozległe instalacje połączone siecią. Cechą przewodnią komputera, który można nazwać mianem równoległego, jest możliwość udostępniania pewnego rodzaju zasobu w~ramach wykonywanego zadania.

Skłanianie się ku prowadzeniu równoległych obliczeń, może wynikać z~konsekwentnie malejących możliwości zwiększania mocy obliczeniowej pojedynczych układów scalonych. Obecnie wzrostu wydajności należy szukać raczej poprzez stosowanie odpowiednio zaprojektowanych algorytmów równoległych na środowiskach o~wielu procesorach.

W~niniejszym rozdziale dokonano próby przybliżenia powodów zrównoleglania programów oraz wprowadzenia czytelnika w~świat algorytmów równoległych. Wnioski każdej z~poniższych sekcji mają doprowadzić do wstępnego zarysowania obrazu \textit{Mobile Cloud Platform}.

\section{Uzasadnienie zrównoleglania obliczeń}

Według prawa Moore'a możliwość zawierania liczby tranzystorów w~jednym układzie scalonym zwiększa się w~kolejnych latach w~sposób wykładniczy. Wzrost ten polega na podwojeniu liczby tranzystorów co około 12 miesięcy~\cite{Gordon_Moore_1965_Article}. Od pewnego czasu wzrost mocy obliczeniowej pojedynczego procesora nie jest już wykładniczy, a~zachowanie prawa Moore'a wynika z~umieszczania coraz większej ilości rdzeni w~jednym procesorze. Takie uwarunkowanie ukazuje przewagę algorytmów równoległych nad sekwencyjnymi pod kątem potencjalnego wzrostu mocy obliczeniowej w~przyszłości.

Obliczenia równoległe można stosować do bardzo wielu znanych i~istotnych dziedzin. Najbardziej popularne to~\cite{Parallel_Computing_Research}:
\begin{itemize}
    \item \textbf{kombinatoryka} - wykonywanie prostych operacji na dużych zbiorach danych,
    \item \textbf{przeszukiwanie grafu} - przeglądanie węzłów pod względem danej charakterystyki,
    \item \textbf{automaty skończone} - przechodzenie po połączonym zbiorze stanów, przykładowo wykorzystywane do parsowania gramatyk,
    \item \textbf{uczenie maszynowe} - znajdowanie niebanalnych zależności w~dużych zbiorach danych,
    \item \textbf{oprogramowanie bazodanowe} - operacje na bardzo dużej liczbie rekordów,
    \item \textbf{grafika komputerowa i gry} - renderowanie grafiki i~modelowanie zjawisk fizycznych,
    \item \textbf{Monte Carlo} - prowadzenie symulacji w~oparciu o~generator liczb losowych,
    \item \textbf{branch and bound} - rozwiązywanie problemu poprzez dzielenie go na podproblemy, a~następnie agregowanie wyników
\end{itemize}
i~wiele innych.

\section{Algorytmy równoległe}

Aby dobrze uchwycić istotę algorytmiki równoległej, najlepiej przytoczyć metodologię projektowania takich algorytmów. Obecnie najczęściej stosowany sposób projektowania polega na metodzie PCAM~\cite{Ian_Foster}. Pozwala ona realizować proces kreowania programu w~ramach czterech wyróżniających się etapów: podział, komunikacja, aglomeracja i~mapowanie (partitioning, communication, agglomeration, and mapping).~\cite{Ian_Foster} Skrócony opis tych etapów wygląda następująco:
\begin{itemize}
    \item \textbf{Podział} - dekompozycja zadania na mniejsze elementy, wskazanie możliwości zrównoleglenia obliczeń,
    \item \textbf{Komunikacja} - określenie wymaganej komunikacji w~celu koordynacji wykonania zadania,
    \item \textbf{Aglomeracja} - ewaluacja poprzednich dwóch etapów w~praktycznym środowisku, biorąc pod uwagę dostępne zasoby oraz przewidywane koszty implementacyjne; zadania wydzielone w~ramach podziału (pierwszega etapu metody PCAM) mogą być łączone w~większe zadania w~celu zwiększenia wydajności lub zmniejszenia kosztów implementacyjnych,
    \item \textbf{Mapowanie} - przypisanie każdego zadania odpowiadającemu mu procesorowi.
\end{itemize}

Stosowanie wyżej opisanej metodologii pozwala wyróżnić najważniejsze cechy algorytmu równoległego. Należy do nich stopień zrównoleglenia algorytmu oraz wymagany narzut komunikacyjny. Im większy stopień zrównoleglenia i~im mniejsza konieczność wymiany informacji lub danych pomiędzy procesami tym bardziej wydajnie będzie działał program wykonujący takie zadanie.

\subsection{Pomiary wydajnościowe}

Istnieją dwie miary, które najlepiej obrazują wydajność algorytmu równoległego: przyśpieszenie (tzw. speed up) i~efektywność~\cite{Introduction_to_PC}. Jeśli $T_1$~to średni czas wykonania na jednym procesorze, a~$T_n$~to czas wykonania na $N$~procesorach, to przyśpieszenie $S_n$~oraz efektywność $E_n$~definiuje się jako:
\\~\\
{\LARGE \centerline{$S_n = \frac{T_1}{T_n}$, $E_n = \frac{S_n}{N}$} }

Prawo Amdahl'a traktuje o~maksymalnym możliwym przyśpieszeniu programu sekwencyjnego, który podlega przebudowaniu na program równoległy. Jeśli $P$~to ułamek programu, który można zrównoleglić, a~$(1 - P)$~to, ułamek który musi pozostać sekwencyjny, wówczas maksymalne przyśpieszenie $S_n$,~które może być uzyskane poprzez użycie $N$~procesorów wynosi~\cite{Amdahl}:
\\~\\
{\LARGE \centerline{$S_n = \frac{1}{(1 - P) + \frac{P}{N}}$} }

Oznacza to, że dla każdego programu o~ułamkowej części równoległej istnieje górna granica możliwego przyśpieszenia przy uruchomieniu go na nieskończenie wielkiej ilości procesorów (zob. rysunek~\ref{fig:amdhals_law}).

\begin{figure}[h!]
	\centering
		\includegraphics[width=\linewidth]{amdhals_law}
	\caption{Zobrazowanie prawa Amdahl'a dla różnych wartości ułamka programu podlegającego zrównolegleniu. Wykres utworzony na podstawie:~\cite{Amdahl}.}
	\label{fig:amdhals_law}
\end{figure}

Prawo Gustafson'a stworzone w~odpowiedzi na prawo Amdahl'a bierze dodatkowo pod uwagę chęć zwiększania problemu wraz ze wzrostem ilości dostępnych procesorów (zob. rysunek ~\ref{fig:gustafsons_law}). Wówczas jeśli $Q$~to część programu, której nie można zrównoleglić maksymalne przyśpieszenie można opisać następująco:~\cite{Gustafson}:
\\~\\
{\LARGE \centerline{$S_n = N - Q(N - 1)$} }

\begin{figure}[h!]
	\centering
		\includegraphics[width=\linewidth]{gustafsons_law}
	\caption{Zobrazowanie prawa Gustafsona'a dla różnych wartości ułamka programu nie podlegającego zrównolegleniu. Wykres utworzony na podstawie:~\cite{Gustafson}.}
	\label{fig:gustafsons_law}
\end{figure}

W~obliczeniach równoległych \textbf{ziarnistość} oznacza ilość obliczeń w~stosunku do komunikacji~\cite{Granularity}. Jeśli $T_o$~to czas przeznaczony na obliczenia, a~$T_k$~czas przeznaczony na komunikację to ziarnistość $G$~można opisać w następujący sposób:
\\~\\
{\LARGE \centerline{$G = \frac{T_o}{T_k}$} }

Ziarnistość ma pośredni związek z~przyśpieszeniem. Mała ziarnistość oznacza dużo komunikacji w~stosunku do obliczeń, co ma przełożenie na słabe przyspieszenie algorytmu równoległego. Gdy ziarnistość rośnie, maleją koszty komunikacji i~potencjalne przyspieszenie algorytmu może być wysokie.

\subsection{Wirtualne topologie}

Komunikacja pomiędzy procesami może przyjmować różnego rodzaju formy. Dobrze ustrukturyzowana komunikacja opiera się o tzw. \textbf{wirtualną topologię}. Definiuje ona zależność pomiędzy kolekcją wirtualnych procesorów, a~ich mapowaniem na procesory fizyczne~\cite{Virtual_Topologies}. Może być ona skonfigurowana jako drzewa, grafy lub siatki wszelkiego rodzaju. Zastosowanie takiego mechanizmu pomaga zarówno w~projektowaniu algorytmu równoległego, jak i~uchwyceniu potencjalnych problemów wydajnościowych związanych z~małą ziarnistością.

Przykładem zastosowania wirtualnej topologii może być użycie schematu drzewa binarnego przy rozwiązywaniu problemu sortowania liczb. Niżej opisany algorytm jest zrównoleglonym wariantem sortowania przez scalanie (zob. ~\cite{Cormen}). Zasada działania: 
\begin{enumerate}
    \item korzeń drzewa rozsyła dwie części tabeli do węzłów, które są jego bezpośrednimi dziećmi,
    \item rozsyłanie w dół jest powtarzane rekurencyjnie, aż do osiągnięcia liści w~wirtualnym drzewie,
    \item liście sortują przekazane im fragmenty tabel i~odsyłają je do swojego bezpośredniego korzenia,
    \item po otrzymaniu posortowanych tabel od procesów dzieci, proces ojciec przystępuje do tworzenia nowej tabeli poprzez zastosowanie scalania,
    \item przesyłanie w~górę ze scalaniem jest powtarzane rekurencyjnie aż do osiągnięcia korzenia w~wirtualnym drzewie.
\end{enumerate}

Przy tak zbudowanej wirtualnej topologii każdy wirtualny procesor musi jedynie prowadzić komunikację z~trzema innymi procesorami: ojcem oraz dwoma dziećmi (zob. rysunek ~\ref{fig:binary}). Oznacza to stosunkowo dużą ziarnistość i~potencjalne dobre wartości miar przyśpieszenia i~efektywności.

\begin{figure}[h!]
	\centering
		\includegraphics[width=\linewidth]{binary_tree_virtual_topology}
	\caption{Wirtualna topologia oparta na drzewie binarnym, zbudowana z~7~procesorów.}
	\label{fig:binary}
\end{figure}

\section{Podsumowanie}

Najważniejsze fakty zawarte w~tym rozdziale dotyczą zasad, którymi kieruje się algorytmika równoległa. Te najważniejsze, które będą wykorzystane w~dalszej części pracy, prezentują się następująco:
\begin{enumerate}
    \item obliczenia równoległe polegają na uruchamianiu jednego zadania na wielu procesorach,
    \item algorytmy rozproszone po podziale na maszyny, muszą mieć możliwość prowadzenia komunikacji pomiędzy poszczególnymi procesorami,
    \item maksymalny zysk wydajnościowy, w~postaci przyśpieszenia, posiada górną granicę, która zależy od rozmiaru ułamka programu składającego się na część równoległą,
    \item wirtualne topologie wspomagają proces projektowania algorytmu oraz dają oględne pojęcie o~stopniu ziarnistości, czyli potencjalnych problemach z~wydajnością.
\end{enumerate}

Przekładając infromacje tu zebrane na wymagania funkcjonalne stawiane budowanej \textit{Platformie obliczeniowej dla urządzeń mobilnych}, można je zaprezentować w~następujący sposób - \textit{Mobile Cloud Platform} powinna wspierać:
\begin{enumerate}
    \item uruchamianie algorytmu równoległego na wielu urządzeniach mobilnych,
    \item umożliwienie i~wspieranie przesyłania komunikatów pomiędzy procesami uruchomionymi na osobnych maszynach,
    \item wspieranie możliwości stosowania wirtualnych topologii przy wykonaniu zrównoleglonego zadania.
\end{enumerate}

Wspomniane wcześniej, zgrupowane w podsumowaniu, fakty pozwalają jednocześnie na zgromadzenie wskazówek odnośnie metodologii testowania i~porównywania algorytmów uruchomionych na zbudowanej już \textit{Platformie obliczeniowej}. Aby zobrazować zysk ze zwiększania rozmiarów chmury obliczeniowej w~stosunku do rozmiaru rozwiązywanego problemu, należy:
\begin{enumerate}
    \item przedstawić miarę wydajności algorytmów w~postaci wykresów przyśpieszenia i~efektywności,
    \item zaadresować problem górnej granicy możliwego przyśpieszenia, zwany prawem Amdahl'a wraz z~jego rozwinięciem do prawa Gustafson'a.
\end{enumerate}
