\select@language {polish}
\select@language {polish}
\contentsline {chapter}{\numberline {1}Wprowadzenie}{9}
\contentsline {section}{\numberline {1.1}Cel pracy}{9}
\contentsline {section}{\numberline {1.2}Struktura pracy}{10}
\contentsline {section}{\numberline {1.3}Zakres prac}{10}
\contentsline {chapter}{\numberline {2}Problematyka oblicze\IeC {\'n} rozproszonych}{13}
\contentsline {section}{\numberline {2.1}Uzasadnienie zr\IeC {\'o}wnoleglania oblicze\IeC {\'n}}{13}
\contentsline {section}{\numberline {2.2}Algorytmy r\IeC {\'o}wnoleg\IeC {\l }e}{14}
\contentsline {subsection}{\numberline {2.2.1}Pomiary wydajno\IeC {\'s}ciowe}{15}
\contentsline {subsection}{\numberline {2.2.2}Wirtualne topologie}{17}
\contentsline {section}{\numberline {2.3}Podsumowanie}{18}
\contentsline {chapter}{\numberline {3}Wybrane technologie w~obliczeniach r\IeC {\'o}wnoleg\IeC {\l }ych}{19}
\contentsline {section}{\numberline {3.1}Komputery r\IeC {\'o}wnoleg\IeC {\l }e}{19}
\contentsline {section}{\numberline {3.2}Protoko\IeC {\l }y komunikacyjne}{21}
\contentsline {section}{\numberline {3.3}Podsumowanie}{21}
\contentsline {chapter}{\numberline {4}Trendy w rozwoju urz\IeC {\k a}dze\IeC {\'n} mobilnych}{23}
\contentsline {section}{\numberline {4.1}Moc obliczeniowa}{23}
\contentsline {section}{\numberline {4.2}Rozmiar pami\IeC {\k e}ci}{23}
\contentsline {section}{\numberline {4.3}Powszechno\IeC {\'s}\IeC {\'c} smartfon\IeC {\'o}w}{24}
\contentsline {section}{\numberline {4.4}Podsumowanie}{25}
\contentsline {chapter}{\numberline {5}Konepcja projektu Mobile Cloud Platform}{27}
\contentsline {section}{\numberline {5.1}Wymagania}{27}
\contentsline {subsection}{\numberline {5.1.1}Wymagania funkcjonalne}{28}
\contentsline {subsection}{\numberline {5.1.2}Wymagania koncepcyjne}{29}
\contentsline {section}{\numberline {5.2}Za\IeC {\l }o\IeC {\.z}enia}{29}
\contentsline {subsection}{\numberline {5.2.1}Samozarz\IeC {\k a}dzalno\IeC {\'s}\IeC {\'c} chmury obliczeniowej}{29}
\contentsline {subsection}{\numberline {5.2.2}Wieloplatformowo\IeC {\'s}\IeC {\'c} produktu}{32}
\contentsline {section}{\numberline {5.3}Komponenty Mobile Cloud Platform}{33}
\contentsline {subsection}{\numberline {5.3.1}Android Client}{33}
\contentsline {subsection}{\numberline {5.3.2}Mobile Cloud Control Center}{33}
\contentsline {chapter}{\numberline {6}Wybrane aspekty implementacyjne}{35}
\contentsline {section}{\numberline {6.1}Implementacja komponentu Android Client}{35}
\contentsline {subsection}{\numberline {6.1.1}Wykorzystane technologie}{35}
\contentsline {subsection}{\numberline {6.1.2}Architektura aplikacji}{36}
\contentsline {subsection}{\numberline {6.1.3}Komunikacja pomi\IeC {\k e}dzy urz\IeC {\k a}dzeniami}{36}
\contentsline {subsection}{\numberline {6.1.4}Dost\IeC {\k e}p do sensor\IeC {\'o}w}{38}
\contentsline {subsection}{\numberline {6.1.5}Kontrola jako\IeC {\'s}ci}{38}
\contentsline {section}{\numberline {6.2}Implementacja komponentu Mobile Cloud Control Center}{39}
\contentsline {subsection}{\numberline {6.2.1}Wykorzystane technologie}{40}
\contentsline {subsection}{\numberline {6.2.2}Architektura rozwi\IeC {\k a}zania}{41}
\contentsline {section}{\numberline {6.3}Specyfikacja protoko\IeC {\l }\IeC {\'o}w komunikacyjnych}{42}
\contentsline {subsection}{\numberline {6.3.1}Protoko\IeC {\l }y wewn\IeC {\k e}trzne}{42}
\contentsline {subsection}{\numberline {6.3.2}Protoko\IeC {\l }y zewn\IeC {\k e}trzne}{48}
\contentsline {chapter}{\numberline {7}Wdro\IeC {\.z}enie i wykorzystanie}{49}
\contentsline {section}{\numberline {7.1}Przygotowanie \IeC {\'s}rodowiska}{50}
\contentsline {subsection}{\numberline {7.1.1}Uruchomienie przyk\IeC {\l }adowego serwera danych}{50}
\contentsline {subsection}{\numberline {7.1.2}Uruchomienie Mobile Cloud Control Center}{50}
\contentsline {subsection}{\numberline {7.1.3}Generowanie tre\IeC {\'s}ci przyk\IeC {\l }adowego zadania obliczeniowego}{51}
\contentsline {section}{\numberline {7.2}Wykorzystanie komponentu Android Client}{52}
\contentsline {subsection}{\numberline {7.2.1}Instalacja i uruchomienie Android Client}{53}
\contentsline {subsection}{\numberline {7.2.2}Do\IeC {\l }\IeC {\k a}czanie urz\IeC {\k a}dzenia do mobilnej chmury obliczeniowej}{53}
\contentsline {subsection}{\numberline {7.2.3}Uruchamianie zadania obliczeniowego w chmurze mobilnej}{53}
\contentsline {section}{\numberline {7.3}Wykorzystanie komponentu Mobile Cloud Control Center}{54}
\contentsline {subsection}{\numberline {7.3.1}Dodawanie urz\IeC {\k a}dze\IeC {\'n} mobilnych}{54}
\contentsline {subsection}{\numberline {7.3.2}Budowanie chmury mobilnej ze znanych urz\IeC {\k a}dze\IeC {\'n}}{56}
\contentsline {subsection}{\numberline {7.3.3}Wysy\IeC {\l }anie tre\IeC {\'s}ci zadania obliczeniowego do serwera}{57}
\contentsline {subsection}{\numberline {7.3.4}Uruchomienie zadania obliczeniowego na chmurze}{57}
\contentsline {subsection}{\numberline {7.3.5}Pobranie wyniku zadania obliczeniowego}{58}
\contentsline {subsection}{\numberline {7.3.6}Deaktywacja chmury}{58}
\contentsline {chapter}{\numberline {8}Weryfikacja eksperymentalna}{61}
\contentsline {section}{\numberline {8.1}Weryfikacja poprawno\IeC {\'s}ci za\IeC {\l }o\IeC {\.z}e\IeC {\'n}}{61}
\contentsline {section}{\numberline {8.2}Testy wydajno\IeC {\'s}ci oblicze\IeC {\'n}}{62}
\contentsline {subsection}{\numberline {8.2.1}Metodologia prowadzenia test\IeC {\'o}w}{62}
\contentsline {subsection}{\numberline {8.2.2}Rodzaje zada\IeC {\'n} testowych}{63}
\contentsline {subsection}{\numberline {8.2.3}\IeC {\'S}rodowiska testowe}{65}
\contentsline {subsection}{\numberline {8.2.4}Wyniki test\IeC {\'o}w wydajno\IeC {\'s}ciowych}{66}
\contentsline {subsection}{\numberline {8.2.5}Pomiary mocy obliczeniowej}{71}
\contentsline {section}{\numberline {8.3}Podsumowanie}{72}
\contentsline {chapter}{\numberline {9}Podsumowanie}{73}
\contentsline {section}{\numberline {9.1}Rezultaty prac}{73}
\contentsline {section}{\numberline {9.2}Mo\IeC {\.z}liwo\IeC {\'s}ci rozwoju}{74}
\contentsline {section}{\numberline {9.3}Wnioski}{74}
\contentsline {chapter}{\numberline {A}Specyfikacja REST API}{77}
\contentsline {section}{\numberline {A.1}Protoko\IeC {\l }y wewn\IeC {\k e}trzne}{77}
\contentsline {subsection}{\numberline {A.1.1}Zlecenie do\IeC {\l }\IeC {\k a}czenia urz\IeC {\k a}dzenia do chmury obliczeniowej}{77}
\contentsline {subsection}{\numberline {A.1.2}Propagowanie informacji o~do\IeC {\l }\IeC {\k a}czeniu nowego urz\IeC {\k a}dzenia do chmury mobilnej}{78}
\contentsline {subsection}{\numberline {A.1.3}Propagowanie informacji o~ilo\IeC {\'s}ci urz\IeC {\k a}dze\IeC {\'n} w~ka\IeC {\.z}dym poddrzewie}{78}
\contentsline {subsection}{\numberline {A.1.4}Zlecenie wykonania zadania obliczeniowego na chmurze mobilnej}{78}
\contentsline {subsection}{\numberline {A.1.5}Propagowanie informacji o~nowym zadaniu obliczeniowym}{79}
\contentsline {subsection}{\numberline {A.1.6}Propagowanie informacji o~urz\IeC {\k a}dzeniach, kt\IeC {\'o}re przyj\IeC {\k e}\IeC {\l }y zadanie obliczeniowe}{79}
\contentsline {subsection}{\numberline {A.1.7}Propagowanie informacji o~topologii sieciowej zadania obliczeniowego}{80}
\contentsline {section}{\numberline {A.2}Protoko\IeC {\l }y zewn\IeC {\k e}trzne}{80}
\contentsline {subsection}{\numberline {A.2.1}Pobranie informacji o wybranym urz\IeC {\k a}dzeniu mobilnym}{81}
\contentsline {subsection}{\numberline {A.2.2}Pobranie informacji o korzeniu wybranego urz\IeC {\k a}dzenia mobilnego}{81}
\contentsline {subsection}{\numberline {A.2.3}Pobranie informacji o dzieciach wybranego urz\IeC {\k a}dzenia mobilnego}{81}
\contentsline {subsection}{\numberline {A.2.4}Deaktywacja chmury obliczeniowej}{82}
\contentsline {chapter}{\numberline {B}Narz\IeC {\k e}dzia programistyczne}{83}
